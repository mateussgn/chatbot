<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
class MY_Controller extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedin'))
           redirect('admin/login');
		
		$this->load->model('usuariomodel');
		$this->session->set_userdata('folder', 'admin/');
	

		$obj = $this->usuariomodel->getById($this->session->userdata('loggedin'));

		$data = array(
			'userdata_nome' => $this->session->userdata('loggedname'),
			'userdata_id' => $this->session->userdata('loggedin'),
			'usuCorrente' => $this->session->userdata('loggedin')

		);

		$data['dadosuser'] = $obj;
		
		
		$this->load->vars($data);
	}
}

class MY_Controller_Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->session->set_userdata('folder', 'admin/');
		
				
		$this->load->vars($data);
	}
}


/* -------------------------   CONTROLE OPERADOR -----------------------*/
class MY_Controller_Operador extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedinoperador'))
           redirect('operador/login');
		
		$this->load->model('operadoresmodel');
		$this->session->set_userdata('folder', 'operador/');
	

		$obj = $this->operadoresmodel->getById($this->session->userdata('loggedinoperador'));

		$data = array(
			'userdata_nome' => $this->session->userdata('loggednameoperador'),
			'userdata_id' => $this->session->userdata('loggedinoperador'),
			'usuCorrente' => $this->session->userdata('loggedinoperador')

		);

		$data['dadosuser'] = $obj;
		
		
		$this->load->vars($data);
	}
}

class MY_Controller_Operador_Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->session->set_userdata('folder', 'operador/');
		
				
		$this->load->vars($data);
	}
}



/* -------------------------   CONTROLE MONITOR -----------------------*/
class MY_Controller_Monitor extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedinmonitor'))
           redirect('monitor/login');
		
		$this->load->model('monitoresmodel');
		$this->session->set_userdata('folder', 'monitor/');
	

		$obj = $this->monitoresmodel->getById($this->session->userdata('loggedinmonitor'));

		$data = array(
			'userdata_nome' => $this->session->userdata('loggednamemonitor'),
			'userdata_id' => $this->session->userdata('loggedinmonitor'),
			'usuCorrente' => $this->session->userdata('loggedinmonitor')

		);

		$data['dadosuser'] = $obj;
		
		
		$this->load->vars($data);
	}
}

class MY_Controller_Monitor_Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->session->set_userdata('folder', 'monitor/');
		
				
		$this->load->vars($data);
	}
}


/* -------------------------   CONTROLE CLIENTE -----------------------*/
class MY_Controller_Cliente extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('clientesmodel');
		
		
		$obj = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));


		$data = array(
			'userdata_nome' => $this->session->userdata('loggednamecliente'),
			'userdata_id' => $this->session->userdata('loggedincliente'),
			'usuCorrente' => $this->session->userdata('loggedincliente')

		);

		$data['dadosuser'] = $obj;
		
		$this->session->set_userdata('folder', 'cliente/');
		$this->load->vars($data);
	}
}

class MY_Controller_Login_Cliente extends CI_Controller {
	function __construct(){
		parent::__construct();
	
	    $this->session->set_userdata('folder', 'cliente/');


		$this->load->vars($data);
	}
}

//Rest
class MY_Controller_Rest extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->session->set_userdata('folder', 'rest/');
		
				
		$this->load->vars($data);
	}
}
