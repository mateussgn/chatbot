var urlatual = 'http://localhost/meusprojetos/atendimentodigital/';
//var urlatual = 'http://lembrelembre.com.br/';
$(document).ready(function(){

/* Executa a requisição quando o campo CEP perder o foco */
   $('#cep').blur(function(){
     /* Configura a requisição AJAX */
     var cep = $('#cep').val();
     
      //Consulta o webservice viacep.com.br/
      $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

            //Atualiza os campos com os valores da consulta.
            $("#rua").val(dados.logradouro);
            $("#bairro").val(dados.bairro);
            $("#cidade").val(dados.localidade);
            $("#estado").val(dados.uf);
        
      });

    //  $.getJSON(urlatual+'admin/clientes/ajax_cep/'+cep, function(data){
    //   console.log(data);
    //   if(data.sucesso == 1){
          
    //       $('#rua').val(data.rua);
    //       $('#bairro').val(data.bairro);
    //       $('#cidade').val(data.cidade);
    //       $('#estado').val(data.estado);
          
    //       $('#numero').focus();
    //   }   
    // });     

  });

/*FORMATANDO INPUTS COM MASKINPUT JQUERY*/
$('#fonep').mask("(99) 9999-9999?9");
$('#fonea').mask("(99) 9999-9999?9");
$('#foneo').mask("(99) 9999-9999?9");
$('#cep').mask("99999-999");
$('.hora').mask("99:99");
$('.cepfranquia').mask("99999-999");
$('.fone').mask("(99) 9999-9999?9");
$('.cpf').mask("999.999.999-99");
$('.cnpj').mask("99.999.999/9999-99");
$('.data').mask("99/99/9999");






//INSERÇÃO PARA TIPO DE EVENTO EM FRANQUIA
$("#form-tipo-evento-franquia").submit(function(event){
  var inputcampo = $("input[type=text][name=tipo_eventos]").val();
  $.ajax({
    url : urlatual+'franquia/tipos_eventos/insert_ajax', /* URL que será chamada */ 
          type : 'POST', /* Tipo da requisição */ 
          data: 'campo=' + inputcampo, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
              //BROTHER ATUALIZA O SELECT AQUI PRA MIM... 
              //BLZ CONSEGUIR NÃO HEHE
                
              
             $(".select_tipo_evento").html('');
             $(".select_tipo_evento").append(data);

              $('#add_tipo_evento').modal('hide');
          }     
     });
    event.preventDefault();
});

/*#############################################################*/
/*#############################################################*/
});//FINAL DO ESCOPO $document(function);
/*#############################################################*/
/*#############################################################*/
/*#############################################################*/




//FUNÇÕES FORA DO documento(functioin)
function dataAtualFormatada() {
  var datacompleta = new Date();
  var mesabrev  = datacompleta.getMonth()+"";if(mesabrev.length==1) mesabrev="0" +mesabrev;
  var dataabrev = datacompleta.getDate()+"";if(dataabrev.length==1) dataabrev="0" +dataabrev;
  var anoatual  = datacompleta.getFullYear();
  var horasAtual= datacompleta.getHours()+':'+datacompleta.getMinutes()+':'+datacompleta.getSeconds();
    return dataabrev+"/"+mesabrev+"/"+anoatual+"  "+horasAtual;
}


function apagarmodal(modulo,funcao,titulo,id){
    $('#myModal').modal('show');
    $('#myModal').find('[id="msn-titulo-form"]').html('');
    $('#myModal').find('[id="msn-titulo-form"]').append('Confirmação');
    $('#myModal').find('[id="msn-titulo-texto"]').html('');
    $('#myModal').find('[id="msn-titulo-texto"]').append('Apagar o registro');
    $('#myModal').find('[id="msn-texto"]').html('');
    //$('#myModal').find('[id="msn-texto"]').append('Deseja apagar '+modulo+' '+titulo)+"?";
    $('#myModal').find('[id="msn-texto"]').append('Deseja apagar o registro selecionado ?');

    $('.btn-apagar-sim').click(function(){
        location.href = modulo+"/"+funcao+"/"+id;
    });
}

function confirmarfinanceiromodal(link){
    $('#myModal').modal('show');
    $('#myModal').find('[id="msn-titulo-form"]').html('');
    $('#myModal').find('[id="msn-titulo-form"]').append('Confirmação');
    $('#myModal').find('[id="msn-titulo-texto"]').html('');
    $('#myModal').find('[id="msn-titulo-texto"]').append('Confirmar Financeiro');
    $('#myModal').find('[id="msn-texto"]').html('');
    //$('#myModal').find('[id="msn-texto"]').append('Deseja apagar '+modulo+' '+titulo)+"?";
    $('#myModal').find('[id="msn-texto"]').append('Deseja realmente lançar este evento no financeiro ?');

    $('.btn-apagar-sim').click(function(){
        location.href = link;
    });
}


function apagarmodalfotos(modulo,funcao,titulo,id){
    $('#myModal').modal('show');
    $('#myModal').find('[id="msn-titulo-form"]').html('');
    $('#myModal').find('[id="msn-titulo-form"]').append('Confirmação');
    $('#myModal').find('[id="msn-titulo-texto"]').html('');
    $('#myModal').find('[id="msn-titulo-texto"]').append('Apagar o registro');
    $('#myModal').find('[id="msn-texto"]').html('');
    //$('#myModal').find('[id="msn-texto"]').append('Deseja apagar '+modulo+' '+titulo)+"?";
    $('#myModal').find('[id="msn-texto"]').append('Deseja apagar o registro selecionado ?');

    $('.btn-apagar-sim').click(function(){
        location.href = "../../"+modulo+"/"+funcao+"/"+id;
    });
}

function clonediv(de,para){
    $( de ).clone().appendTo( para );
}

function addblank(formulario,de){
  
  if($(de).prop('checked') == true){
    $(formulario).attr('target','_blank');
  }else{
    $(formulario).attr('target','');
  }
} 

function selfranquia(id,modulo){

   window.location.href = modulo+'/'+id.value;
}


function selAtividade(id,modulo){

   window.location.href = modulo+'/'+id.value;
}



// function selecionaCidade(modulo,div){
//    var estado = ($('.estado').val());
//    var urlgeral = urlatual+modulo+'eventos/selecionaCidade';
//    $.ajax({
//         url : urlgeral,
//         type : 'POST',
//         data: 'estado=' + estado,
        
//         success: function(result){
//             //alert(result);
//             $(div).html(result);
//         }
//    });

// }


