<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('show_alert'))
{
	function show_alert()
	{
		$CI =& get_instance();
		if($CI->session->flashdata('alert_admin')==null) 
			return;
		?>
		<div id="alertBox">
			<?php echo $CI->session->flashdata('alert_admin');?>
		</div>
		<?php
	}
}

if ( ! function_exists('set_alert'))
{
	function set_alert($text = null)
	{
		$CI =& get_instance();
		$CI->session->set_flashdata('alert_admin', $text);
	}
}