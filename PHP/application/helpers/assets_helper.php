<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CSS File
 *
 * Retorna o link do arquivo
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 
if ( ! function_exists('a_css'))
{
	function a_css($file = '')
	{
		return base_url()._ASSETS_PATH_.'/css/'.$file;
	}
}

if ( ! function_exists('a_img'))
{
	function a_img($file = '')
	{
		return base_url()._ASSETS_PATH_.'/img/'.$file;
	}
}

if ( ! function_exists('a_site'))
{
	function a_assets($file = '')
	{
		return base_url()._ASSETS_PATH_.'/'.$file;
	}
}


if ( ! function_exists('a_js'))
{
	function a_js($file = '')
	{
		return base_url()._ASSETS_PATH_.'/js/'.$file;
	}
}


/**
 * Others Files
 *
 * Retorna o link do arquivo
 *
 * @access	public
 * @param	string
 * @return	string
 */	
