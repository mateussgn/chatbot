<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");



function gerar_tumbs_real($t_x,$t_y, $qualidade,$c_original,$c_final){
		#### Raphael Jordany ####
		#### Versão 1.2		 ####
		$thumbnail = imagecreatetruecolor($t_x,$t_y);  
		$original = $c_original;
		$igInfo = getImageSize($c_original) ;   
		switch ($igInfo['mime']) {
		   case 'image/gif':
			   if (imagetypes() & IMG_GIF)  {  $originalimage = imageCreateFromGIF($original) ; } else {   $ermsg = 'GIF formato não suportado<br />'; }
			   break;
		   case 'image/jpeg':
			   if (imagetypes() & IMG_JPG)  {  $originalimage = imageCreateFromJPEG($original) ;  } else {  $ermsg = 'JPEG formato não suportado<br />'; }
			   break;
		   case 'image/png':
			   if (imagetypes() & IMG_PNG)  {  $originalimage = imageCreateFromPNG($original) ; } else {  $ermsg = 'PNG formato não suportado<br />';   }
			   break;
		   case 'image/wbmp':
			   if (imagetypes() & IMG_WBMP)  {  $originalimage = imageCreateFromWBMP($original) ;   } else { $ermsg = 'WBMP formato não suportado<br />'; }
			   break;
		   default:
			   $ermsg = $igInfo['mime'].' formato não suportado <br />';
			   break;
		 };
		$nLargura = $igInfo[0];
		$nAltura = $igInfo[1];
	
		if( ($nLargura > $t_x )and($nAltura >  $t_y) ){
			if($t_x <= $t_y){
				  $nLargura = (int) (($igInfo[0]*$t_y)/$igInfo[1]);
				  $nAltura = $t_y;
			} else {
				  $nLargura = $t_x ;
				  $nAltura = (int) (($igInfo[1]*$t_x)/$igInfo[0]);
				  if($nAltura < $t_y){
					  $nLargura = (int) (($igInfo[0]*$t_y)/$igInfo[1]);
					  $nAltura = $t_y;
				  };
			};
		};
	
		$x_pos =  ($t_x/2)-( $nLargura/2);
		$y_pos =  ($t_y/2)-($nAltura/2);
		imagecopyresampled($thumbnail, $originalimage,  $x_pos, $y_pos,0, 0, $nLargura, $nAltura, $igInfo[0],$igInfo[1]); 
		imagejpeg($thumbnail,$c_final ,$qualidade); 
		imagedestroy($thumbnail); 
		return 'ok';
	}


function retirarAcento($texto) 
{
	$novotexto = strtolower(ereg_replace("[^a-zA-Z0-9_.]", "", strtr($texto, "áàãâéêíõôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_")));
	return $novotexto;
}

function mes($data)
{
	switch ($data) {
		case 1: $mes = 'Janeiro'; break;
		case 2: $mes = 'Fevereiro'; break;
		case 3: $mes = 'Março'; break;
		case 4: $mes = 'Abril'; break;
		case 5: $mes = 'Maio'; break;
		case 6: $mes = 'Junho'; break;
		case 7: $mes = 'Julho'; break;
		case 8: $mes = 'Agosto'; break;
		case 9: $mes = 'Setembro'; break;
		case 10: $mes = 'Outubro'; break;
		case 11: $mes = 'Novembro'; break;
		case 12: $mes = 'Dezembro'; break;
	}
	return $mes;
}

function mes2($data)
{
	switch ($data) {
		case 1: $mes = 'Jan'; break;
		case 2: $mes = 'Fev'; break;
		case 3: $mes = 'Mar'; break;
		case 4: $mes = 'Abr'; break;
		case 5: $mes = 'Mai'; break;
		case 6: $mes = 'Jun'; break;
		case 7: $mes = 'Jul'; break;
		case 8: $mes = 'Ago'; break;
		case 9: $mes = 'Set'; break;
		case 10: $mes = 'Out'; break;
		case 11: $mes = 'Nov'; break;
		case 12: $mes = 'Dez'; break;
	}
	return $mes;
}

function inverterDataHora($data)
{
	$datetime = explode(" ",$data);
	$hora = explode(":", $datetime[1]);

	$data = explode("-", $datetime[0]);

	return $data[2]."/".$data[1]." ".$hora[0].":".$hora[1];
}

function inverterData($data)
{
	$date = explode("-",$data);
	

	return $date[2]."/".$date[1]."/".$date[0];
}

function inverterData2($data)
{
	$date = explode("/",$data);
	

	return $date[2]."-".$date[1]."-".$date[0];
}

function sliderDataInicio($data)
{
	$date = explode("/",$data);
	
	return $date[2]."-".$date[1]."-".$date[0]." 00:01:00";
}
function sliderDataFim($data)
{
	$date = explode("/",$data);
	
	return $date[2]."-".$date[1]."-".$date[0]." 23:59:00";
}

function DataRetirarHora($data)
{
	$datetime = explode(" ",$data);
	
	$date = explode("-",$datetime[0]);

	return $date[2]."/".$date[1]."/".$date[0];
}


function limita_caracteres($texto, $limite, $quebra = true) {
    $tamanho = strlen($texto);
    // Verifica se o tamanho do texto é menor ou igual ao limite
    if ($tamanho <= $limite) {
        $novo_texto = $texto;
    // Se o tamanho do texto for maior que o limite
    } else {
        // Verifica a opção de quebrar o texto
        if ($quebra == true) {
            $novo_texto = trim(substr($texto, 0, $limite)).'...';
        // Se não, corta $texto na última palavra antes do limite
        } else {
            // Localiza o útlimo espaço antes de $limite
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), ' ');
            // Corta o $texto até a posição localizada
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco)).'...';
        }
    }
    // Retorna o valor formatado
    return $novo_texto;
}


/**
* Função para gerar senhas aleatórias
*
* @author    Thiago Belem <contato@thiagobelem.net>
*
* @param integer $tamanho Tamanho da senha a ser gerada
* @param boolean $maiusculas Se terá letras maiúsculas
* @param boolean $numeros Se terá números
* @param boolean $simbolos Se terá símbolos
*
* @return string A senha gerada
*/
function geraSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
{
$lmin = 'abcdefghijklmnopqrstuvwxyz';
$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$num = '1234567890';
$simb = '!@#$%*-';
$retorno = '';
$caracteres = '';
$caracteres .= $lmin;
if ($maiusculas) $caracteres .= $lmai;
if ($numeros) $caracteres .= $num;
if ($simbolos) $caracteres .= $simb;
$len = strlen($caracteres);
for ($n = 1; $n <= $tamanho; $n++) {
$rand = mt_rand(1, $len);
$retorno .= $caracteres[$rand-1];
}
return $retorno;
}