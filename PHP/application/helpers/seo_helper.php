<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Head Title
 *
 * Retorna o title da pagina a ser exibido no HTML
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 
if ( ! function_exists('head_title'))
{
	function head_title($title = null)
	{
		$CI =& get_instance();
		return $CI->seo->title($title);
	}
}

/**
 * Head Keywords
 *
 * Retorna o keywords da pagina a ser exibido nas metas do HTML
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 
if ( ! function_exists('head_keywords'))
{
	function head_keywords($keywords = null)
	{
		$CI =& get_instance();
		return $CI->seo->keywords($keywords);
	}
}


/**
 * Head Description
 *
 * Retorna o descriptions da pagina a ser exibido nas metas do HTML
 *
 * @access	public
 * @param	string
 * @return	string
 */	
 
if ( ! function_exists('head_description'))
{
	function head_description($description = null)
	{
		$CI =& get_instance();
		return $CI->seo->description($description);
	}
}