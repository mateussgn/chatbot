<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller_Login_Cliente
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('clientesmodel');

    }

    public function index()  
    {	
		if ($this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'dashboard');
			
		$this->load->view($this->session->userdata('folder').'login');
    }

    public function esqueceuasenha()  
    {   
        if ($this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'dashboard');
            
        $this->load->view($this->session->userdata('folder').'esqueceuasenha');
    }

    public function mudarsenha($id)  
    {   
        $id = base64_decode($id);
        $data['dados'] = $this->clientesmodel->getById($id);

        $this->load->view($this->session->userdata('folder').'mudarsenha',$data);
        
    }

    public function updatsenha()
    {
       $obj = new stdClass();
       $obj->id      = $_REQUEST['id'];
        
            if($_POST['senha']){
                $senha = isset($_POST['senha'])?$_POST['senha']:null;
                $obj->senha = sha1($senha);
            }
            
            $update = $this->clientesmodel->update($obj);
            
            if($update){
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Senha alterada com sucesso!</p>
                </div>');
            }
        
        redirect($this->session->userdata('folder').$this->router->class); 
    }
	  
    public function logar()
    {   
        //echo sha1($this->input->post('senha'));
        //die();
    	$result = $this->clientesmodel->authenticate($this->input->post('login'), sha1($this->input->post('senha')));
		if(!is_null($result)){
            $this->session->set_userdata('loggedincliente', $result->id);
			$this->session->set_userdata('loggednamecliente', $result->nome);
            $this->session->set_userdata('loggednameusuario', $result->usuario);
            
            //print_r($result);
            //die();
			redirect($this->session->userdata('folder').'dashboard');
		}else{
			 redirect($this->session->userdata('folder').'login');
		}
    }

    function base64url_encode($data) {
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data) {
    return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    public function lembrar_a_senha(){


        $email = $_POST['email'];       

        $registro = $this->clientesmodel->getUsu_porEmail($email);
        if($registro){

        $emailbased = $this->base64url_encode($registro[0]->id);
        $logo = a_img('logo.jpg');

         $html = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Lembre de senha de acesso</title>
        </head>
        
        <body>
        <div align="center">
            <img src="'.$logo.'" style="width: 239px;">
        </div>

<div class="m_2207450784184353027layout m_2207450784184353027one-col m_2207450784184353027fixed-width gmail_msg" style="Margin:0 auto;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px);word-wrap:break-word;word-break:break-word">
        <div class="m_2207450784184353027layout__inner gmail_msg" style="border-collapse:collapse;display:table;width:100%;background-color:#ffffff">
        
          <div class="m_2207450784184353027column gmail_msg" style="text-align:left;color:#787778;font-size:16px;line-height:24px;font-family:Ubuntu,sans-serif;max-width:600px;min-width:320px;width:320px;width:calc(28000% - 167400px)">
        
            <div style="Margin-left:20px;Margin-right:20px;Margin-top:24px" class="gmail_msg">
      <div style="line-height:20px;font-size:1px" class="gmail_msg">&nbsp;</div>
    </div>
        
            <div style="Margin-left:20px;Margin-right:20px" class="gmail_msg">
      <h1 style="Margin-top:0;Margin-bottom:0;font-style:normal;font-weight:normal;color:#565656;font-size:30px;line-height:38px;text-align:center" class="gmail_msg"><strong class="gmail_msg">Olá, você está recebendo seu lembrete de senha!</strong></h1>

      <p style="Margin-top:20px;Margin-bottom:0" class="gmail_msg">&nbsp;<br class="gmail_msg">

          Recadastre sua senha clicando no botão a baixo e siga as instruções.

          </p>

          
    </div>
        
            <div style="Margin-left:20px;Margin-right:20px" class="gmail_msg">
      <div style="line-height:10px;font-size:1px" class="gmail_msg">&nbsp;</div>
    </div>
        
            <div style="Margin-left:20px;Margin-right:20px" class="gmail_msg">
      <div class="m_2207450784184353027btn m_2207450784184353027btn--flat m_2207450784184353027btn--large gmail_msg" style="Margin-bottom:20px;text-align:center">
        <u class="gmail_msg"></u><a style="border-radius:4px;display:inline-block;font-size:14px;font-weight:bold;line-height:24px;padding:12px 24px;text-align:center;text-decoration:none!important;color:#fff;background-color:#80bf2e;font-family:Ubuntu,sans-serif" href="'.site_url('cliente/login/mudarsenha/'.$emailbased).'" class="gmail_msg" target="_blank" data-saferedirecturl="">RECADASTRE SUA SENHA AQUI</a><u class="gmail_msg"></u>
      </div>
    </div>
        
            <div style="Margin-left:20px;Margin-right:20px" class="gmail_msg">
      <div style="line-height:10px;font-size:1px" class="gmail_msg">&nbsp;</div>
    </div>
        
           
    
        
          </div>
        
        </div>
      </div>


        </body>
        </html>
        ';


        //echo $html;
        $this->email->clear(true);
        $this->email->from('contato@lembrelembre.com.br', 'Lembre-Lembre');
        $this->email->to($registro[0]->email);
        //$this->email->cc('contato@juliowebdesign.com');
        //$this->email->bcc('them@their-example.com');

        $this->email->subject('Recuperar Senha - Lembre-Lembre');
        $this->email->message($html);

        $this->email->send();

        
        set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Verifique seu email, você receberá um email para recuperar sua senha!</p>
                </div>');

        }else{
            set_alert('<div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p>Email não encontrado!</p>
                </div>');
        }

        


                
        
        
        redirect($this->session->userdata('folder').$this->router->class); 
    }



    public function logout()
    {
        $this->session->unset_userdata('loggedincliente');
        redirect('login');
    }
}