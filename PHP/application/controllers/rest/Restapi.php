<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Restapi extends CI_Controller {

	function __construct()
	{
	    parent::__construct();
	    $this->load->library('session');
	    $this->load->library('session');
	    $this->load->model('chatlistmodel');  
        $this->load->model('messagesmodel');    
        $this->load->model('departamentosmodel');    
        $this->load->model('operadoresmodel');    
        $this->load->model('menusmodel');    
        $this->load->model('menuspersonalizadosmodel');
        $this->load->model('sendsmodel');   
        $this->load->model('filamodel');   
        $this->load->model('clientesmodel');
        $this->load->model('agendacontatosmodel');
        $this->load->library('bot'); 
        $u = json_decode(file_get_contents('php://input'), true);
	}

    function index()
    {
        error_reporting(E_ALL);
        $this->load->library('bot'); 
        echo "API";
        $cliente = $this->clientesmodel->getById(1);

        //echo utf8_decode('Informe agora o endereÃ§o completo (Rua, NÃºmero, Bloco, Apartamento, Etc, ...)');
        //die();

        //$menu = $this->menusmodel->getListWhere(array('id_cliente'=>1,'id_menu'=>'6'));
        //print_r($menu);
 
        //print_r($cliente);
        //echo $filas = $this->filamodel->getListWhereCountFila('558388123160@c.us');        
        //$retorno = $this->bot->usa_api('558196505010@c.us','Funcionarios II',$cliente->dialogflowclient);

        //echo utf8_decode($retorno);
        
    }


    function last()
    {

        $arrayAll = array();
        $list = $this->sendsmodel->getListWhereLimit(array('status'=>0),10);
        foreach ($list as $key => $rt) {
        
        if($rt->jid<>'@c.us'){
            array_push($arrayAll, $rt);    
        }
        
        //$arraysend = array('id' => $rt->id,'jid' => $rt->jid,'number' => $rt->number,'message' => ($rt->message),'image' => $rt->image,'media_name' => $rt->media_name );
        
        }

        echo  json_encode($arrayAll);

    }


    function update($id)
    {
        //if($this->gettoken($_POST['token']) == 1){

            //$cliente = $this->gettokenDados($_POST['token']);
            
            //$list = $this->sendsmodel->getListWhereLimit(array('id_cliente'=>$cliente->id,'status'=>0),1);
            //$rt = ($list[0]);
            $obj = new stdClass();
            $obj->id                    = $id;
            $obj->status                = 1;
            $this->sendsmodel->update($obj);
        //}else{
            echo json_encode('update com sucesso');
        //}

 
    }

    function consultas()
    {
        //departamentos, chat_list, messages, menu, sends
        if($this->gettoken($_POST['token']) == 1){

            $cliente = $this->gettokenDados($_POST['token']);
            
            $tabela = $_POST['tabela'];

            if($tabela == 'departamentos'){
                $list = $this->departamentosmodel->getListWhere(array('id_cliente'=>$cliente->id));    
                echo json_encode($list);
            }

            if($tabela == 'chat_list'){
                $list = $this->chatlistmodel->getListWhere(array('id_cliente'=>$cliente->id),1);    
                echo json_encode($list);
            }

            if($tabela == 'messages'){
                $list = $this->messagesmodel->getListWhereLast(array('id_cliente'=>$cliente->id));    
                echo json_encode($list);
            }

            if($tabela == 'menu'){
                $list = $this->menusmodel->getListWhere(array('id_cliente'=>$cliente->id,'id_menu'=>$_POST['menu']));
                echo json_encode($list);
            }

            if($tabela == 'fila'){
                $list = $this->filamodel->getListWhereCount(array('id_cliente'=>$cliente->id,'receive'=>$_POST['receive'],'status'=>$_POST['status']));
                echo json_encode($list);
            }

            if($tabela == 'menupersonalizado'){
                $list = $this->menuspersonalizadosmodel->getListWhere(array('id_cliente'=>$cliente->id));
                echo json_encode($list);
            }
            
            
        }else{
            echo json_encode('Contate o administrador!');
        }

 
    }

    function insert()
    {
        $u = json_decode(file_get_contents('php://input'), true);
        if ($u) {
        //departamentos, chat_list, messages, menu, sends
        //if($this->gettoken($u['token']) == 1){

            //$cliente = $this->gettokenDados($u['token']);
            $tabela = $u['tabela'];

            if($tabela == 'chat_list'){

                $dados = $u;
                unset($dados['tabela']);
                unset($dados['token']);

                $obj = new stdClass();
                foreach ($dados as $key => $value) {
                    $obj->$key = $value;
                }
                
                $this->chatlistmodel->insert($obj);    
                echo json_encode(array('send'=>'1'));
            }

            if($tabela == 'messages'){

                $dados = $u;
                unset($dados['tabela']);
                unset($dados['token']);

                $obj = new stdClass();
                foreach ($dados as $key => $value) {
                    if($dados['media_url']<>''){
                        //$ext = substr($dados['media_name'], -3);
                        //$extname = $this->mimetype($ext);
                        //$media_url = strtr( $value, '-_', '+/'); 
                        //$media_url = 'data:'.$extname.';base64,'.$media_url;
                        $media_url = strtr( $value, '-_', '+/'); 
                        
                        $obj->$key = $media_url;    
                    }else{
                        $obj->$key = $value;    
                    }
                }
                
                $this->messagesmodel->insert($obj);    
                echo json_encode(array('send'=>'1'));
            }

            if($tabela == 'fila'){

                $dados = $u;
                unset($dados['tabela']);
                unset($dados['token']);

                $obj = new stdClass();
                foreach ($dados as $key => $value) {
                    $obj->$key = $value;
                }
                
                $this->filamodel->insert($obj);    
                echo json_encode(array('send'=>'1'));
            }

            if($tabela == 'sends'){

                $dados = $u;
                unset($dados['tabela']);
                unset($dados['token']);

                $obj = new stdClass();
                foreach ($dados as $key => $value) {
                    $obj->$key = $value;
                }
                
                $this->sendsmodel->insert($obj);    
                echo json_encode(array('send'=>'1'));
            }
            
            
        // }else{
        //     echo json_encode('Contate o administrador!');
        // }
        }
 
    }

    function salvarcampos($number,$msg){

        // falta 
        // pegar o valor por bairro
        // criar o pedido apos o sim ou confirmo ou se cancelar terminar o atendimento
        // a confirmação ou cancelamento vim do php e não do dialogflow
        // colocar na aba de menus para configurar o texto de confirmação e cancelamento
        // criar menu confirmação pedido, pedido finalizado, pedido cancelado
        // 

        $lastmessage = $this->messagesmodel->getListWhereLimit(array('receive'=>$number),2);
        $lastmessage = $lastmessage[1];
        $arraycampos = array(
            'bairro' => 'Informe por favor o Bairro para entrega',
            'endereco'=>'Informe agora o endereço completo (Rua, Número, Bloco, Apartamento, Etc, ...)',
            'pontoreferencia'=>'Informe um ponto de referencia (Perto de que fica!)',
            'formapagamento'=>'Informe a forma de pagamento ( Dinheiro, Débito ou Crédito)'
        );
        foreach ($arraycampos as $key => $value) {
            if(trim($lastmessage->texto) == trim($value)){
                $obj = new stdClass();
                $obj->receive = $number;
                $obj->$key = $msg;
                $this->chatlistmodel->updatenumber($obj);
                //echo $key;
            }
        }

        //salvando confirmação de pedido
        if(strtolower($msg) == 'sim' or strtolower($msg) == 'confirmo'){

            $cliente = $this->chatlistmodel->getByNumber($number);

            $obj = new stdClass();
            $obj->numpedido = date('YmdGis');
            $obj->number = $number;
            $obj->valor = 65.00;
            $obj->datapedido = date('Y-m-d G:i:s');
            $obj->entregador = 0;
            $this->pedidomodel->insert($obj);

            //retorno de mensagem

            $message = "*Confirmação de Pedido* ".$obj->numpedido."

*Distribuidores de Gás*

Produto no valor de R$%65%
Entrega para o ".$cliente->bairro."
No endereço ".$cliente->endereco."
Próximo a ".$cliente->pontoreferencia."
Forma de Pagamento ".$cliente->formapagamento."

Digite o número *0* para voltar ao *Menu* Inicial";

            $this->insertchatMenu($number,$number,($message));

            return 1;
        }else{
            return 0;
        }
        


    }

    function insertmessage()
    {   
        $this->load->library('bot'); 
        $u = json_decode(file_get_contents('php://input'), true);

        if ($u) {
        $number = ($u['number']);
        $messageid = ($u['messageid']);

        $message = ($u['message']);
        $mimetype = ($u['mimetype']);
        $caption = trim($u['caption']);
        $name = trim($u['name']);

        
        if($mimetype<>'indefinido'){
            $media_url = 'data:' . $mimetype . ';base64,' . $message; 
            if($caption<>''){
                $media_name = $caption;    
            }else{
                $media_name = 'image';
            }
            
            if($caption<>'undefined'){
                $message = $caption;     
            }else{
                $message = '';     
            }
        }else{
            $mimetype = '';
            $media_name = '';
            $media_url = '';
        }

        //verificando se e a primeira mensagem
        $msgsexiste = $this->messagesmodel->getListWhereCountMessageAll($number);
        if($this->chatlistmodel->getListWhereCount(array('receive'=>$u['number'])) <= 0 ){
            $obj = new stdClass();
            $obj->name = $name;
            $obj->receive = $number;
            $obj->id_cliente = 1;

            $contato = new stdClass();
            $contato->nome = $name;
            $contato->number = str_replace('@c.us', '', $number);
            $contato->id_cliente = 1;

            $this->chatlistmodel->insert($obj);
            $this->agendacontatosmodel->insert($contato);
        }

        $obj = new stdClass();

        $obj->receive = $number;
        $obj->from_me = 0;
        $obj->texto = $message;
        $obj->media_url = $media_url;
        $obj->media_type = $mimetype;
        $obj->media_name = $media_name;
        if(isset($messageid)){
            $obj->id_wa = $messageid;    
        }
        $obj->id_cliente = 1;
        $obj->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
        $this->messagesmodel->insert($obj);    

        //salvando campos
        //$salvoupedido = $this->salvarcampos($number,$message);
        

        //criando fila 
        $departamentos = $this->departamentosmodel->getListByComando($message);
        if(isset($departamentos[0]->id)){
            $filaexiste = $this->filamodel->getListWhereCountFilaNovo($number);
            if($filaexiste <= 0){

                $objfila = new stdClass();
                $objfila->id                    = '';
                $objfila->id_atendente          = $this->session->userdata('loggedinoperador');
                $objfila->receive                   = $number;
                $objfila->id_departamento        = $departamentos[0]->id;
                $objfila->datahora                = date('Y-m-d H:i:s');
                $objfila->protocolo                = date('YmdHis');
                $objfila->status                = 0;
                $objfila->id_cliente            = 1;
                
                $this->filamodel->insert($objfila); 
            }
        }

        //criando menupersonalizado
        $cliente = $this->clientesmodel->getById(1);
        if($cliente->menupersonalizado==1){
            $menupersonalizado = $this->menuspersonalizadosmodel->getListByComando($message);
            if(isset($menupersonalizado[0]->id)){
                $menupersonalizadoexiste = 1;
                $menup = $menupersonalizado[0];
                $message = $menup->texto;
                $image = $menup->foto;

                if(strpos($number, '@c.us') == true){
                    $number = $number;
                }else{
                    $number = $number.'@c.us';
                }

                $obj = new stdClass();
                $obj->id                    = '';
                $obj->id_atendente          = 1;
                $obj->jid                   = $number;
                $obj->number                = $number;
                $obj->message               = ($message);
                $obj->image               = $image;
                if($image<>''){
                    $obj->media_name               = $menup->comando;
                }
                $obj->id_cliente            = 1;
                $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
                
                $this->sendsmodel->insert($obj);

                //inserindo mensagem nas conversas
                $objm = new stdClass();
                $objm->receive = $number;
                $objm->from_me = 1;
                $objm->texto = $message;
                $objm->media_url               = ($image);
                if($image<>''){
                    $objm->media_name               = 'image';
                }
                $objm->id_cliente = 1;
                $objm->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
                $this->messagesmodel->insert($objm); 


            }else{
                $menupersonalizadoexiste = 0;
            }
        }else{
            $menupersonalizadoexiste = 0;
        }

        

        //verificando para ligar o wifi
        $codigo = explode('_', $message);
        if ($codigo[0] == '#QueroWifi') {
            $this->agendacontatosmodel->updateCodigo(str_replace('@c.us', '', $number), $codigo[1]);
            $codigoWifi = 'Codigo = '.$codigo[1];
            $this->insertchatMenu($number, $number, $codigoWifi);

            //$url = 'https://wifi.atendente.digital/app/getadswa/'.$codigo[1];
            $url = 'http://192.99.226.28/integracao/integracao/getContato/'.str_replace('@c.us', '', $number);

            $json_file = file_get_contents($url);
            $json_array = json_decode($json_file);
            $json_contato = (object)$json_array[0];
            $json_contato->id = $this->agendacontatosmodel->countContatos() + 1;
            $salvou = $this->agendacontatosmodel->insert($json_contato);

            $salvo = ($salvou == true) ? 'Contato Salvo!!' : 'Erro' ;
            $url = $salvo."\nBasta Clicar no link abaixo para comecar a navegar\nwifi.atendente.digital/app/liberainternet";
            $this->insertchatMenu($number, $number, $url);

        }


        //if verificando se e a primeira mensagem
        if($msgsexiste == 0){
            //montando boas vindas
            $menu = $this->menusmodel->getListWhere(array('id_cliente'=>1,'id_menu'=>'1'));
            $menu = $menu[0];
            $saudacao = $this->saudacao();

            $numbertxt = str_replace('@c.us', '', $number);
            $message = str_replace('%nome%', $numbertxt, $menu->resposta);
            $message = str_replace('%DTN%', $saudacao, $message);
            $message = str_replace('%operador%', $this->session->userdata('loggednameoperador'), $message);
            $message = str_replace('%protocolo%', $fila->protocolo, $message);

            $this->insertchatMenu($number,$number,($message));

        }else{
            //retorno do dialogflow
            $contato = $this->agendacontatosmodel->getUltimoContato(str_replace('@c.us', '', $number));
            if($contato->nome == ''){
                $this->chatlistmodel->updateNome($number, $message);
                $this->agendacontatosmodel->updateNome(str_replace('@c.us', '', $number), $message);
            }
            $filaexiste = $this->filamodel->getListWhereCountFilaAll($number);
            if($filaexiste > 0){
                $filas = $this->filamodel->getListWhereCountFila($number);
                if($filas <= 0 and $u['mimetype'] == 'indefinido' and $menupersonalizadoexiste==0 and $salvoupedido ==0 ){
                    $cliente = $this->clientesmodel->getById(1);
                    $retorno = $this->bot->usa_api($number,$message,$cliente->dialogflowclient);

                    if(trim($retorno)<>'' or trim($retorno)<>'status@broadcast@c.us'){

                        $retorno = ($retorno);
                        $retorno =  str_replace('\r\n' , '
        ' , $retorno);

                        $obj = new stdClass();
                        $obj->id                    = '';
                        $obj->id_atendente          = $this->session->userdata('loggedinoperador');
                        $obj->jid                   = $number;
                        $obj->number                = $number;
                        $obj->message               = $retorno;
                        $obj->id_cliente            = 1;
                        $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
                        $this->sendsmodel->insert($obj);

                        //inserindo mensagem nas conversas
                        $obj = new stdClass();

                        $obj->receive = $number;
                        $obj->from_me = 0;
                        $obj->texto = $retorno;
                        $obj->media_url = '';
                        $obj->media_type = '';
                        $obj->id_cliente = 1;
                        $obj->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
                        $this->messagesmodel->insert($obj); 

                    }
                }
            }else{
                if($u['mimetype'] == 'indefinido' and $menupersonalizadoexiste==0 and $salvoupedido ==0 ){
                    $cliente = $this->clientesmodel->getById(1);
                    $retorno = $this->bot->usa_api($number,$message,$cliente->dialogflowclient);

                    if(trim($retorno)<>'' or trim($retorno)<>'status@broadcast@c.us'){

                        $retorno = ($retorno);
                        $retorno =  str_replace('\r\n' , '
        ' , $retorno);

                        $obj = new stdClass();
                        $obj->id                    = '';
                        $obj->id_atendente          = $this->session->userdata('loggedinoperador');
                        $obj->jid                   = $number;
                        $obj->number                = $number;
                        $obj->message               = $retorno;
                        $obj->id_cliente            = 1;
                        $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
                        $this->sendsmodel->insert($obj);

                        //inserindo mensagem nas conversas
                        $obj = new stdClass();

                        $obj->receive = $number;
                        $obj->from_me = 0;
                        $obj->texto = $retorno;
                        $obj->media_url = '';
                        $obj->media_type = '';
                        $obj->id_cliente = 1;
                        $obj->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
                        $this->messagesmodel->insert($obj); 

                    }
                }
            }
        }
        //echo json_encode(array('send'=>'1'));
        }


 
    }


    function insertmessageimage()
    {   
        $u = json_decode(file_get_contents('php://input'), true);
        if ($u) {
            echo $idwhats = ($u['idwhats']);
            echo $blob = ($u['blob']);
        }

        

 
    }

    function gettoken($token)
    {   
        $result = $this->clientesmodel->authenticateToken($token);
        if(!is_null($result)){
            return 1;
        }else{
            return 0;
        }
    }

    function gettokenDados($token)
    {   
        $result = $this->clientesmodel->authenticateToken($token);
        if(!is_null($result)){
            return $result;
        }else{
            return 0;
        }
    }

    function insertchatMenu($jid,$number,$message){

        if(strpos($number, '@c.us') == true){
            $number = $number;
        }else{
            $number = $number.'@c.us';
        }

        $obj = new stdClass();
        $obj->id                    = '';
        $obj->id_atendente          = 1;
        $obj->jid                   = $number;
        $obj->number                = $number;
        $obj->message               = ($message);
        $obj->id_cliente            = 1;
        $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
        $this->sendsmodel->insert($obj);

        //inserindo mensagem nas conversas
        $objm = new stdClass();
        $objm->receive = $number;
        $objm->from_me = 1;
        $objm->texto = $message;
        $objm->id_cliente = 1;
        $objm->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
        $this->messagesmodel->insert($objm); 

        

    }
    
    function mimetype($mimetype){

        $mimet = array( 
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        return $mimet[$mimetype];
    }

    function saudacao(){
        $hr = date(" H ");
        if($hr >= 12 && $hr<18) {
        $dtn = "Boa tarde";}
        else if ($hr >= 0 && $hr <12 ){
        $dtn = "Bom dia";}
        else {
        $dtn = "Boa noite";}
        return $dtn;
    }

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
