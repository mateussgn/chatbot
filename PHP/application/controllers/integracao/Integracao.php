<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Integracao extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('bairrosmodel');
        $this->load->model('agendacontatosmodel');        
    }

    function getContato($number) {
        $data['list'] = $this->agendacontatosmodel->getListWhere(array('number' => $number));
        foreach ($data['list'] as $key => $value) {
            if($value->bairro<>0) {
                $op = $this->bairrosmodel->getById($value->bairro);
                $value->bairro = $op->nome;
            }
        }
        $json = json_encode($data['list']);
        echo($json);
    }

    function receiveContato($codigo) {
        //$url = "https://wifi.atendente.digital/app/getadswa/".$codigo;
        $url = "http://192.99.226.28/integracao/integracao/getcontato/".$codigo;
        $json_file = file_get_contents($url);
        $json_array = json_decode($json_file);
        $json_contato = (object)$json_array[0];
        $json_contato->id = $this->agendacontatosmodel->countContatos() + 1;
        $salvou = $this->agendacontatosmodel->insert($json_contato);

        if ($salvou == true)
            echo "Contato salvo!!";
        else
            echo "Erro!";

        if (is_null($data['list']))
            return false;
        else
            return true;
    }
}