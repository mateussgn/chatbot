<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Monitores extends MY_Controller_Monitor {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedinmonitor'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('monitoresmodel');
		$this->load->model('departamentosmodel');
		head_title('Monitores');

	}

	function index()
	{
		$this->page();
	}
	

    function ajax_cep($cep){
    	$cep = $cep;
    	$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
		 
		$dados['sucesso'] = (string) $reg->resultado;
		$dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
		$dados['bairro']  = (string) $reg->bairro;
		$dados['cidade']  = (string) $reg->cidade;
		$dados['estado']  = (string) $reg->uf;
		 
		echo json_encode($dados);

    } 
	
	function editar($id)
	{
		head_title('Modificar monitores');
		
		$data['obj'] = $this->monitoresmodel->getById($id);
		$data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form',$data);
	}
	
	function insert()
	{
		$obj = new stdClass();
		$obj->id      				= $_REQUEST['id'];
		$obj->nome    				= ($_REQUEST['nome']!=''?$_REQUEST['nome']:'');
		$obj->mae    				= ($_REQUEST['mae']!=''?$_REQUEST['mae']:'');
		$obj->telefone				= ($_REQUEST['telefone']!=''?$_REQUEST['telefone']:'');
		$obj->complemento	        = ($_REQUEST['complemento']!=''?$_REQUEST['complemento']:'');
		$obj->endereco				= ($_REQUEST['endereco']!=''?$_REQUEST['endereco']:'');
	    $obj->cep 					= ($_REQUEST['cep']!=''?$_REQUEST['cep']:'');
        $obj->cidade 				= ($_REQUEST['cidade']!=''?$_REQUEST['cidade']:'');
        $obj->estado 				= ($_REQUEST['estado']!=''?$_REQUEST['estado']:'');
        $obj->bairro 				= ($_REQUEST['bairro']!=''?$_REQUEST['bairro']:'');
        $obj->numero 				= ($_REQUEST['numero']!=''?$_REQUEST['numero']:'');

		$obj->email					= ($_REQUEST['email']!=''?$_REQUEST['email']:'');
		$obj->cpf	    			= ($_REQUEST['cpf']!=''?$_REQUEST['cpf']:'');
		$obj->sexo  		        = ($_REQUEST['sexo']!=''?$_REQUEST['sexo']:'');
		$arrDataNasc = explode("/", $_REQUEST['dataNascimento']);
		$dataNasc    = $arrDataNasc[2]."-".$arrDataNasc[1]."-".$arrDataNasc[0];
		$obj->dataNascimento        = $dataNasc;
		$obj->usuario            		= $_REQUEST['usuario']; 
		$obj->horainicio  		        = ($_REQUEST['horainicio']!=''?$_REQUEST['horainicio']:'');
		$obj->horafim  		        = ($_REQUEST['horafim']!=''?$_REQUEST['horafim']:'');
		$obj->id_cliente    				= $this->session->userdata('loggedincliente');

		if($_REQUEST['dias']){
			foreach ($_REQUEST['dias'] as $key => $value) {
				$dias .= $value.',';
			}
			$dias = substr($dias, 0,-1);
			$obj->dias = $dias;
		}

		if($_REQUEST['id_departamento']){
			foreach ($_REQUEST['id_departamento'] as $key => $value) {
				$id_departamento .= $value.',';
			}
			$id_departamento = substr($id_departamento, 0,-1);
			$obj->id_departamento = $id_departamento;
		}


		if(is_numeric($obj->id)){
			if($_POST['senha']){
				$senha = isset($_POST['senha'])?$_POST['senha']:null;
				$obj->senha = sha1($senha);
			}

			$update = $this->monitoresmodel->update($obj);
			
			if($update){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Monitor alterado com sucesso!</p>
	            </div>');
			}
		}else{
			$senha = isset($_POST['senha'])?$_POST['senha']:null;
			$obj->senha = sha1($senha);
			$insert = $this->monitoresmodel->insert($obj);
			
			if($insert){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Monitor cadastrado com sucesso!</p>
	            </div>');
				
			}	
		}
		
		redirect($this->router->class);
		
					
	}
	
	function apagar($id)
	{
		$del = $this->monitoresmodel->delById($id);
		if($del){
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Monitor deletado com sucesso!</p>
	            </div>');
		}else{
			set_alert('<div class="alert alert-danger alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados Monitor apresentou erro ao deletar!</p>
	            </div>');
		}	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	
	function page($page = 0)
	{
		
		$this->editar($this->session->userdata('loggedinmonitor'));
		
		
	}
	
	function search($page = 0)
	{
		$titulo = $_GET['nome'];
		
		$cont = $this->monitoresmodel->totalPaginasSearch($titulo);
		$config['base_url'] = base_url().$this->session->userdata('folder').$this->router->class.'/page/';
        $config['total_rows'] = $cont;
	    		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    $data['list'] = $this->monitoresmodel->getListPageSearch($page,$this->pagination->per_page,$titulo);
    	
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
}