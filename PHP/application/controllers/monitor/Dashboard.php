<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller_Monitor {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedinmonitor'))
            redirect($this->session->userdata('folder').'login');

		
		$this->load->library('session');
		$this->load->model('chatlistmodel');  
        $this->load->model('messagesmodel');    
        $this->load->model('departamentosmodel');    
        $this->load->model('operadoresmodel');    
        $this->load->model('monitoresmodel');    
        $this->load->model('menusmodel');    
        $this->load->model('sendsmodel');   
        $this->load->model('filamodel');   
        $this->load->model('clientesmodel');
        $this->load->model('pedidomodel');
        $this->load->model('entregadoresmodel');
        $this->load->model('bairrosmodel');
        $this->load->model('interacoesmodel');
        $this->load->model('agendacontatosmodel');
	}

	function index()
	{
		$data['aguardando'] = $this->filamodel->getListWhereDeps(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'status'=>'0'),$this->session->userdata('loggeddeps'),20);

        $data['ematendimento'] = $this->filamodel->getListWhereDeps(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'status'=>'1'),$this->session->userdata('loggeddeps'),20);

        $data['finalizados'] = $this->filamodel->getListWhereDeps(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'status'=>'2'),$this->session->userdata('loggeddeps'),20);

        $data['interacoes'] = $this->chatlistmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid')),20);

        $data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid')));

        $data['operadores'] = $this->operadoresmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid')));

        $data['agendadecontatos'] = $this->agendacontatosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid')));

		$this->load->view($this->session->userdata('folder').'dashboard',$data);
	}

    function loadfila()
    {
        $txt = $_REQUEST['txt'];
        if($_REQUEST['txt']<>''){
            $fila = $this->chatlistmodel->getListWhereLike(array('id_cliente'=>$this->session->userdata('loggedinclienteid')),array('receive'=>$txt));
        }else{
            $fila = $this->chatlistmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid')),20);    
        }

        
        ?>
        <div class="sidebar-menu">
            <ul class="filas">
                <li class="menu-title">
                    <th>Lista de Interação</th>
                    <th>
                        <a href="javascript://" onclick="agendacontatos()">Editar Contatos</a>
                    </th>
                </li>
                <?
                foreach ($fila as $key => $value) {
                ?>
                    <li>
                        <a href="javascript://" onclick="openchat('<?=str_replace('@c.us', '', $value->receive)?>')">
                            <span class="chat-avatar-sm user-img">
                                <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                                <span class="status online"></span>
                            </span> 
                            <?
                            if(trim($value->name)==''){
                                echo str_replace('@c.us', '', $value->receive);
                            }else{
                                echo $value->name;
                            }
                            ?> 
                            <span class="badge bg-success pull-right">Visualizar</span>
                        </a>
                        <span style="font-size: 10px;position: absolute;margin-top: -13px;margin-left: 54px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                    </li>
                <?  
                }
                ?>
            </ul>
        </div>
        <?
    }

    function loadfilaesquerda()
    {
        $aguardando = $this->filamodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'status'=>'0'),20);

        $ematendimento = $this->filamodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'id_atendente'=>$this->session->userdata('loggedinmonitor'),'status'=>'1'),20);

        $finalizados = $this->filamodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'id_atendente'=>$this->session->userdata('loggedinmonitor'),'status'=>'2'),20);
        
        ?>

        <li class="menu-title">Aguardando</li>
        <?
        foreach ($aguardando as $key => $value) {
        ?>
            <li>
                <a href="javascript://" onclick="atenderchat('<?=$value->id?>','<?=str_replace('', '', $value->receive)?>')">
                    <span class="chat-avatar-sm user-img">
                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                        <span class="status online"></span>
                    </span>
                    <?=$value->nome?><!--<?=str_replace('@c.us', '', $value->receive)?>-->
                    <br>
                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 35px;">Pedido: <?=$value->protocolo?></span>
                </a>
            </li>
        <?  
        }
        ?>

        <li class="menu-title">Em Atendimento</li>
        <?
        foreach ($ematendimento as $key => $value) {
        ?>
            <li>
                <a href="javascript://" onclick="openchat('<?=str_replace('', '', $value->receive)?>','<?=$value->id?>')">
                    <span class="chat-avatar-sm user-img">
                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                        <span class="status online"></span>
                    </span>
                    <?=$value->nome?><!--<?=str_replace('@c.us', '', $value->receive)?>-->
                    <br>
                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 35px;">Pedido: <?=$value->protocolo?></span>
                </a>
            </li>
        <?  
        }
        ?>

        <li class="menu-title">Finalizados</li>
        <?
        foreach ($finalizados as $key => $value) {
        ?>
            <li>
                <a href="javascript://" onclick="openchat('<?=str_replace('', '', $value->receive)?>','<?=$value->id?>')">
                    <span class="chat-avatar-sm user-img">
                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                        <span class="status online"></span>
                    </span>
                    <?=$value->nome?><!--<?=str_replace('@c.us', '', $value->receive)?>-->
                    <br>
                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 35px;">Pedido: <?=$value->protocolo?></span>
                </a>
            </li>
        <?  
        }
        ?>
        <?
    }

	function openchat($receive,$idfila=0)
	{
        if(strpos($receive, '@c.us') == true){
            $receive = $receive;
        }else{
            $receive = $receive.'@c.us';
        }
        
        $cliente = $this->clientesmodel->getById($this->session->userdata('loggedinclienteid'));
        $messages = $this->messagesmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'receive'=>$receive));

        //print_r($messages);

        foreach ($messages as $key => $value) {
            $ultimoatendente = $this->sendsmodel->getListWhereLastOperador(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'jid'=>$receive),$value->timestamp);

            if(isset($ultimoatendente->id_atendente) and $value->from_me==1){
                $operador = $this->operadoresmodel->getById($ultimoatendente->id_atendente);
                $value->operadornome = $operador->nome;
            }else{
                $value->operadornome = $cliente->nome;
            }
            

        }

		$messageslast = $this->messagesmodel->getListWhereLast(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'receive'=>$receive));

        $fila = $this->filamodel->getById($idfila);
		$id_atendente = $this->session->userdata('loggedinmonitor');
		?>
		<div class="chat-window">
            <div class="fixed-header">
                <div class="navbar">
                    <div class="user-details">
                        <!-- <div class="pull-left user-img m-r-10">
                            <a href="profile.html" title="Mike Litorus"><img src="<?=a_img('user.jpg')?>" alt="" class="w-40 img-circle"><span class="status online"></span></a>
                        </div> -->
                        <div class="user-info pull-left">
                            <span class="font-bold"><?=str_replace('@c.us', '', $receive)?></span> 
                                <!-- <i class="typing-text">Typing...</i> -->
                            
                            <span class="last-seen">Visto pela última vez hoje 7:50 AM</span>
                        </div>
                    </div>
                    <ul class="nav navbar-nav pull-right custom-menu">
                        <!-- <li>
                            <a href="#chat_sidebar" class="task-chat profile-rightbar pull-right"><i class="fa fa-user" aria-hidden="true"></i></a>
                        </li> -->
                        <li class="">
                            <a href="<?=site_url($this->session->userdata('folder').'dashboard/pdf/'.str_replace('@c.us', '', $receive))?>" target='blank'><i class="fa fa-file-pdf-o"></i></a>
                        </li>
                        <?
                        if($fila->status==2){
                        ?>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></a>
                            <ul class="dropdown-menu">
                                <!-- <li><a href="javascript:void(0)">Encaminhar</a></li> -->
                                <li><a href="javascript:void(0)" onclick="reabrirchat('<?=$idfila?>')">Reabrir</a></li>
                            </ul>
                        </li>
                        <?
                        }else{
                        ?>
                        
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)" onclick="encaminharchat('<?=$idfila?>')">Encaminhar</a></li>
                                <li><a href="javascript:void(0)" onclick="finalizarchat('<?=$idfila?>')">Finalizar</a></li>
                            </ul>
                        </li>
                        <?    
                        }
                        ?>
                    </ul>
                    <div class="search-box pull-right">
                        <div class="input-group input-group-sm">
                            <input type="text" class="form-control" placeholder="Pesquisa" required="" id="busca">
                            <span class="input-group-btn">
                                    <button class="btn" type="button" onclick="buscar()"><i class="fa fa-search"></i></button>
                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="chat-contents">
                <div class="chat-content-wrap">
                    <div class="chat-wrap-inner">
                        <div class="chat-box">
                            <div class="chats">
                            	<?
                                foreach ($messages as $key => $value) {
                                		if($value->texto <> '' or $value->media_url<>'data:;base64,' or $value->media_url<>''){
                                		if($value->from_me==0){
                                			$class = 'class="chat chat-right"';
                                            $atendente = '';
                                		}else{
                                			$class = 'class="chat chat-left"';
                                            $atendente = $value->operadornome;
                                		}
                                		$chattime = explode(' ', $value->timestamp);
                                		$chattimeh = $chattime[1];
                                		$chattimed = explode('-', $chattime[0]);
                                		$chattime = $chattimed[2].'.'.$chattimed[1].'.'.$chattimed[0].' '.$chattimeh;

                                        //$texto = nl2br(utf8_decode(htmlspecialchars($value->texto)));
                                        $texto = nl2br((htmlspecialchars($value->texto)));
                                        if($value->media_url<>'' or $value->media_type<>''){
                                            
                                            $extname = substr($value->media_name, -3);
                                            $ext = $this->mimetype($extname);
                                            
                                            if($value->media_type=='application/pdf'){
                                                $name = $value->media_name;
                                                $media_url = ($value->media_url);
                                            ?>
                                            <div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <ul class="attach-list">
                                                                <li class="pdf-file"><i class="fa fa-file-pdf-o"></i> <a href="<?=$media_url?>" target='blank' title='botão direito e abrir em nova aba'><?=$name?>.pdf</a></li>
                                                            </ul>
                                                            <span class="chat-time"><?=$chattime?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?
                                            }else if(substr($value->media_url, 0,10)=='data:image'){
                                                //if (strpos($value->media_url, 'base64') === false) {
                                                    //$media_url = ($value->media_url);
                                                //}else{
                                                    $media_url = ($value->media_url);
                                                //}
                                    		?>
                                    		<div <?=$class?>>
                                                <div class="chat-body">
    												<div class="chat-bubble">
                                                        <div class="chat-content img-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <div class="chat-img-group clearfix">
                                                                <a class="chat-img-attach" href="<?=$media_url?>" target="_blank">
                                                                    <img width="182" height="137" alt="" src="<?=$media_url?>">
                                                                    <div class="chat-placeholder">
                                                                        <div class="chat-img-name"><?=$value->media_name?></div>
                                                                        <div class="chat-file-desc"><?=$texto?><br><?=$chattime?></div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                            	</div>
                                        	</div>
                                    		<?
                                            }else if(substr($value->media_url, 0,10)=='data:audio'){
                                                //if (strpos($value->media_url, 'base64') === false) {
                                                    //$media_url = ($value->media_url);
                                                //}else{
                                                    $media_url = ($value->media_url);
                                                //}
                                            ?>
                                            <div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content img-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <div class="chat-img-group clearfix">
                                                                <audio controls src="<?=$media_url?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?
                                            }
                                		}else{
                                		?>
											<div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <p><?=$texto?></p>
                                                            <span class="chat-time"><?=$chattime?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    	<?	
                                		}
                                	}
                            	}
                            	?>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
		<?

	}


	function loadmsg()
	{
		$id_atendente   = $_REQUEST['id_atendente'];
		$jid    		= $_REQUEST['jid'];
		$number			= $_REQUEST['number'];
		$message	    = $_REQUEST['msg'];
		$last_id	    = $_REQUEST['last_id'];

        $cliente = $this->clientesmodel->getById($this->session->userdata('loggedinclienteid'));
		$messages = $this->messagesmodel->getListWhereNext(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'receive'=>$number),$last_id);
        foreach ($messages as $key => $value) {
            $ultimoatendente = $this->sendsmodel->getListWhereLastOperador(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'jid'=>$receive),$value->timestamp);

            // if(isset($ultimoatendente->id_atendente) and $value->from_me==1){
            //     $operador = $this->operadoresmodel->getById($ultimoatendente->id_atendente);
            //     $value->operadornome = $operador->nome;
            // }else{
            //     $value->operadornome = $cliente->nome;
            // }
            if($this->session->userdata('loggedinmonitor')<>0){
                $operador = $this->operadoresmodel->getById($this->session->userdata('loggedinmonitor'));
                $value->operadornome = $operador->nome;
            }else{
                if(isset($ultimoatendente->id_atendente) and $value->from_me==1){
                    $operador = $this->operadoresmodel->getById($ultimoatendente->id_atendente);
                    $value->operadornome = $operador->nome;
                }else{
                    $value->operadornome = $cliente->nome;
                }
            }
            

        }


        $id_atendente = $this->session->userdata('loggedinmonitor');
		
		foreach ($messages as $key => $value) {
            if($value->texto <> '' or $value->media_url<>'data:;base64,' or $value->media_url<>''){
                                        if($value->from_me==0){
                                            $class = 'class="chat chat-right"';
                                            $atendente = '';
                                        }else{
                                            $class = 'class="chat chat-left"';
                                            $atendente = $value->operadornome;
                                        }
                                        $chattime = explode(' ', $value->timestamp);
                                        $chattimeh = $chattime[1];
                                        $chattimed = explode('-', $chattime[0]);
                                        $chattime = $chattimed[2].'.'.$chattimed[1].'.'.$chattimed[0].' '.$chattimeh;

                                        //$texto = nl2br(utf8_decode(htmlspecialchars($value->texto)));
                                        //$texto = nl2br((htmlspecialchars($value->texto)));
                                        $texto = nl2br((htmlspecialchars($value->texto)));

                                        if($value->media_url<>'' or $value->media_type<>''){
                                            
                                            $extname = substr($value->media_name, -3);
                                            $ext = $this->mimetype($extname);
                                            
                                            if($value->media_type=='application/pdf'){
                                                $name = $value->media_name;
                                                $media_url = ($value->media_url);
                                            ?>
                                            <div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <ul class="attach-list">
                                                                <li class="pdf-file"><i class="fa fa-file-pdf-o"></i> <a href="<?=$media_url?>" target='blank' title='botão direito e abrir em nova aba'><?=$name?>.pdf</a></li>
                                                            </ul>
                                                            <span class="chat-time"><?=$chattime?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?
                                            }else if(substr($value->media_url, 0,10)=='data:image'){
                                                //if (strpos($value->media_url, 'base64') === false) {
                                                    //$media_url = ($value->media_url);
                                                //}else{
                                                    $media_url = ($value->media_url);
                                                //}
                                            ?>
                                            <div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content img-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <div class="chat-img-group clearfix">
                                                                <a class="chat-img-attach" href="<?=$media_url?>" target="blank">
                                                                    <img width="182" height="137" alt="" src="<?=$media_url?>">
                                                                    <div class="chat-placeholder">
                                                                        <div class="chat-img-name"><?=$value->media_name?></div>
                                                                        <div class="chat-file-desc"><?=$texto?><br><?=$chattime?></div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?
                                            }else if(substr($value->media_url, 0,10)=='data:audio'){
                                                //if (strpos($value->media_url, 'base64') === false) {
                                                    //$media_url = ($value->media_url);
                                                //}else{
                                                    $media_url = ($value->media_url);
                                                //}
                                            ?>
                                            <div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content img-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <div class="chat-img-group clearfix">
                                                                <audio controls src="<?=$media_url?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?
                                            }
                                        }else{
                                        ?>
                                            <div <?=$class?>>
                                                <div class="chat-body">
                                                    <div class="chat-bubble">
                                                        <div class="chat-content">
                                                            <a href="#"><?=$atendente?></a>
                                                            <p><?=$texto?></p>
                                                            <span class="chat-time"><?=$chattime?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?  
                                        }
                                    }
                
        }
    	
    	

	}

    function loadmsgbkp()
    {
        $id_atendente   = $_REQUEST['id_atendente'];
        $jid            = $_REQUEST['jid'];
        $number         = $_REQUEST['number'];
        $message        = $_REQUEST['msg'];
        $last_id        = $_REQUEST['last_id'];

        $cliente = $this->clientesmodel->getById($this->session->userdata('loggedinclienteid'));
        $messages = $this->messagesmodel->getListWhereNext(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'receive'=>$number),$last_id);
        foreach ($messages as $key => $value) {
            $ultimoatendente = $this->sendsmodel->getListWhereLastOperador(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'jid'=>$receive),$value->timestamp);

            if(isset($ultimoatendente->id_atendente) and $value->from_me==1){
                $operador = $this->operadoresmodel->getById($ultimoatendente->id_atendente);
                $value->operadornome = $operador->nome;
            }else{
                $value->operadornome = $cliente->nome;
            }
            

        }


        $id_atendente = $this->session->userdata('loggedinmonitor');
        
        foreach ($messages as $key => $value) {
                if($value->from_me==0){
                    $class = 'class="chat chat-right"';
                    $atendente = '';
                }else{
                    $class = 'class="chat chat-left"';
                    $atendente = $value->operadornome;
                }
                $chattime = explode(' ', $value->timestamp);
                $chattimeh = $chattime[1];
                $chattimed = explode('-', $chattime[0]);
                $chattime = $chattimed[2].'.'.$chattimed[1].'.'.$chattimed[0].' '.$chattimeh;

                $texto = nl2br((htmlspecialchars($value->texto)));

                $extname = substr($value->media_name, -3);
                $ext = $this->mimetype($extname);
                
                if($value->media_url<>'' and $value->from_me == 0){
                    if (strpos($value->media_url, 'base64') === false) {
                        $media_url = 'data:'.$ext.';base64,'.($value->media_url);
                    }else{
                        $media_url = ($value->media_url);
                    }
                    ?>
                    <div <?=$class?>>
                        <div class="chat-body">
                            <div class="chat-bubble">
                                <div class="chat-content img-content">
                                    <a href=""><?=$atendente?></a>
                                    <div class="chat-img-group clearfix">
                                        <a class="chat-img-attach" href="<?=$value->media_url?>" target="blank">
                                            <img width="182" height="137" alt="" src="<?=$value->media_url?>">
                                            <div class="chat-placeholder">
                                                <div class="chat-img-name"><?=$value->media_name?></div>
                                                <div class="chat-file-desc"><?=$texto?><br><?=$chattime?></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    
                }else{
                ?>
                    <div <?=$class?>>
                        <div class="chat-body">
                            <div class="chat-bubble">
                                <div class="chat-content">
                                    <p><?=$atendente?></p>
                                    <p><?=$texto?></p>
                                    <span class="chat-time"><?=$chattime?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?  
                }

            }
        
        

    }

	function lastid()
	{
		$id_atendente   = $_REQUEST['id_atendente'];
		$jid    		= $_REQUEST['jid'];
		$number			= $_REQUEST['number'];
		$message	    = $_REQUEST['msg'];
		$last_id	    = $_REQUEST['last_id'];

		$messageslast = $this->messagesmodel->getListWhereLast(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'receive'=>$number));

    	echo $messageslast->id;

	}


	function insertchat(){

        if(strpos($_REQUEST['number'], '@c.us') == true){
            $number = $_REQUEST['number'];
        }else{
            $number = $_REQUEST['number'].'@c.us';
        }

		$obj = new stdClass();
		$obj->id      				= '';
		$obj->id_atendente    		= $this->session->userdata('loggedinmonitor');
		$obj->jid    				= $number;
		$obj->number				= $number;
		//$obj->message	        	= utf8_encode($_REQUEST['msg']);
        $obj->message               = nl2br($_REQUEST['msg']);
        $obj->id_cliente            = $this->session->userdata('loggedinclienteid');
		if($_REQUEST['arquivo']<>''){
            $img = site_url().'uploads/sends/'.$_REQUEST['arquivo']; 
            $type = image_type_to_mime_type(exif_imagetype($img));
            $data = file_get_contents($img);
            $base64 = 'data:' . $type . ';base64,' . base64_encode($data);

            $obj->image	        		= $base64;	
            $obj->media_name                 = 'image';  
			$arquivo	        		= site_url().'uploads/sends/'.$_REQUEST['arquivo'];	
			$arquivoname	        	= $_REQUEST['arquivo'];	
		}
        $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
		
		$this->sendsmodel->insert($obj);

        //inserindo mensagem nas conversas
        $objm = new stdClass();
        $objm->receive = $number;
        $objm->from_me = 1;
        $objm->texto = ($_REQUEST['msg']);
        if($_REQUEST['arquivo']<>''){
            $objm->media_url = $base64;
            $objm->media_type = $type;
            $objm->media_name = 'image';
        }
        $objm->id_cliente = 1;
        $objm->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
        $this->messagesmodel->insert($objm); 


        $operador = $this->operadoresmodel->getById($this->session->userdata('loggedinmonitor'));
        $operador = $operador->nome;

		if($_REQUEST['arquivo']<>''){
            if(substr($_REQUEST['arquivo'], -3) == 'pdf'){
            ?>
            <!-- <div class="chat chat-left">
                <div class="chat-body">
                    <div class="chat-bubble">
                        <div class="chat-content">
                            <a href="#"><?=$operador?></a>
                            <ul class="attach-list">
                                <li class="pdf-file"><i class="fa fa-file-pdf-o"></i> <a href="<?=$arquivo?>"><?=$arquivoname?></a></li>
                            </ul>
                            <span class="chat-time"><?=date('d.m.Y G:i:s')?></span>
                        </div>
                    </div>
                </div>
            </div> -->
            <?
            }else{
    		?>
    		<!-- <div class="chat chat-left">
                <div class="chat-body">
    				<div class="chat-bubble">
                        <div class="chat-content img-content">
                            <a href="#"><?=$operador?></a>
                            <div class="chat-img-group clearfix">
                                <a class="chat-img-attach" href="<?=$arquivo?>" target="blank">
                                    <img width="182" height="137" alt="" src="<?=$arquivo?>">
                                    <div class="chat-placeholder">
                                        <div class="chat-img-name"><?=$arquivoname?></div>
                                        <div class="chat-file-desc"><?=($_REQUEST['msg'])?><br><?=date('d.m.Y G:i:s')?></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
            	</div>
        	</div> -->
    		<?
            }
		}else{
		?>
			<!-- <div class="chat chat-left">
	            <div class="chat-body">
	                <div class="chat-bubble">
	                    <div class="chat-content">
                            <a href="#"><?=$operador?></a>
	                        <p><?=($_REQUEST['msg'])?></p>
	                        <span class="chat-time"><?=date('d.m.Y G:i:s')?></span>
	                    </div>
	                </div>
	            </div>
	    	</div> -->
    	<?	
		}

	}

    function insertchatMenu($jid,$number,$message){

        if(strpos($number, '@c.us') == true){
            $number = $number;
        }else{
            $number = $number.'@c.us';
        }

        $obj = new stdClass();
        $obj->id                    = '';
        $obj->id_atendente          = $this->session->userdata('loggedinmonitor');
        $obj->jid                   = $number;
        $obj->number                = $number;
        $obj->message               = ($message);
        $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
        $obj->id_cliente            = 1;
        
        $this->sendsmodel->insert($obj);

        //inserindo mensagem nas conversas
        $objm = new stdClass();
        $objm->receive = $number;
        $objm->from_me = 1;
        $objm->texto = $message;
        $objm->id_cliente = 1;
        $objm->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
        $this->messagesmodel->insert($objm); 

        $operador = $this->operadoresmodel->getById($this->session->userdata('loggedinmonitor'));
        $operador = $operador->nome;
        ?>
            <!-- <div class="chat chat-left">
                <div class="chat-body">
                    <div class="chat-bubble">
                        <div class="chat-content">
                            <a href="#"><?=$operador?></a>
                            <p><?=($message)?></p>
                            <span class="chat-time"><?=date('d.m.Y G:i:s')?></span>
                        </div>
                    </div>
                </div>
            </div> -->
        <?  


        

    }

    function insertchatMenuSemRetorno($jid,$number,$message){
        if(strpos($number, '@c.us') == true){
            $number = $number;
        }else{
            $number = $number.'@c.us';
        }
        
        $obj = new stdClass();
        $obj->id                    = '';
        $obj->id_atendente          = $this->session->userdata('loggedinmonitor');
        $obj->jid                   = $number;
        $obj->number                = $number;
        $obj->message               = utf8_encode($message);
        $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
        $obj->id_cliente            = $this->session->userdata('loggedinclienteid');
        
        $this->sendsmodel->insert($obj);

        //inserindo mensagem nas conversas
        $objm = new stdClass();
        $objm->receive = $number;
        $objm->from_me = 1;
        $objm->texto = $message;
        $objm->id_cliente = 1;
        $objm->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
        $this->messagesmodel->insert($objm); 
        

    }

    
    function atenderchat(){
        // $id = $_REQUEST['id'];
        // $fila = $this->filamodel->getById($id);
        // $dep = $this->departamentosmodel->getById($fila->id_departamento);

        // $receive = $_REQUEST['receive'];
        // $number = $receive;
        // $numbertxt = str_replace('@c.us', '', $receive);
        // $saudacao = $this->saudacao();

        // $obj = new stdClass();
        // $obj->id                = $id;
        // $obj->status            = 1;
        // //$obj->protocolo         = $dep->sigla.'-'.date('ymdhis');
        // $obj->id_atendente      = $this->session->userdata('loggedinmonitor');
        // $obj->dataatendimento   = date('Y-m-d G:i:s');
        // $this->filamodel->update($obj);

        // //montando mensagem
        // $menu = $this->menusmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'id_menu'=>'1'));
        // $menu = $menu[0];

        // $message = str_replace('%nome%', $numbertxt, $menu->resposta);
        // $message = str_replace('%DTN%', $saudacao, $message);
        // $message = str_replace('%operador%', $this->session->userdata('loggednameoperador'), $message);
        // $message = str_replace('%protocolo%', $fila->protocolo, $message);

        // $this->insertchatMenu($receive,$number,($message));



    }

    function finalizarchat(){
        // $id = $_REQUEST['id'];
        // $fila = $this->filamodel->getById($id);
        // $dep = $this->departamentosmodel->getById($fila->id_departamento);

        // $receive = $fila->receive;
        // $number = $receive;
        // $numbertxt = str_replace('@c.us', '', $receive);
        // $saudacao = $this->saudacao();

        // $obj = new stdClass();
        // $obj->id              = $id;
        // $obj->status          = 2;
        // $obj->datatermino = date('Y-m-d G:i:s');
        // $this->filamodel->update($obj);

        // //montando mensagem
        // $menu = $this->menusmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'id_menu'=>'4'));
        // $menu = $menu[0];

        // $message = str_replace('%nome%', $numbertxt, $menu->resposta);
        // $message = str_replace('%DTN%', $saudacao, $message);
        // $message = str_replace('%operador%', $this->session->userdata('loggednameoperador'), $message);
        // $message = str_replace('%protocolo%', $fila->protocolo, $message);

        
        // $this->insertchatMenuSemRetorno($receive,$number,($message));



    }

    function reabrirchat(){
        // $id = $_REQUEST['id'];
        // $fila = $this->filamodel->getById($id);
        // $dep = $this->departamentosmodel->getById($fila->id_departamento);

        // $receive = $fila->receive;
        // $number = $receive;
        // $numbertxt = str_replace('@c.us', '', $receive);
        // $saudacao = $this->saudacao();

        // $obj = new stdClass();
        // $obj->id              = $id;
        // $obj->status          = 1;
        // $obj->datatermino = date('Y-m-d G:i:s');
        // $this->filamodel->update($obj);

        // //montando mensagem
        // $menu = $this->menusmodel->getById(4);

        // $message = str_replace('%nome%', $number, $menu->resposta);
        // $message = str_replace('%DTN%', $saudacao, $message);
        // $message = str_replace('%operador%', $this->session->userdata('loggednameoperador'), $message);
        // $message = str_replace('%protocolo%', $fila->protocolo, $message);

        //$this->insertchatMenu($receive,$number,$message);



    }

    function encaminharchat(){

        // $fila = $this->filamodel->getById($_REQUEST['idfila']);

        // $obj = new stdClass();
        // $obj->id              = $_REQUEST['idfila'];
        // $obj->status          = 2;
        // $obj->datatermino = date('Y-m-d G:i:s');
        // $this->filamodel->update($obj);

        // //criando nova fila
        // $objfila = new stdClass();
        // $objfila->id                    = '';
        // $objfila->id_atendente          = $_REQUEST['id_atendente'];
        // $objfila->receive                   = $fila->receive;
        // $objfila->id_departamento        = $_REQUEST['id_departamento'];
        // $objfila->datahora                = date('Y-m-d H:i:s');
        // $objfila->protocolo                = date('YmdHis');
        // $objfila->status                = 0;
        // $objfila->id_cliente            = 1;
        
        // $this->filamodel->insert($objfila); 


        // //montando mensagem
        // $receive = $fila->receive;
        // $number = $receive;

        // $dep = $this->departamentosmodel->getById($_REQUEST['id_departamento']);
        // $menu = $this->menusmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid'),'id_menu'=>'7'));
        // $menu = $menu[0];

        // $message = str_replace('%nome%', $numbertxt, $menu->resposta);
        // $message = str_replace('%DTN%', $saudacao, $message);
        // $message = str_replace('%nomedepartamento%', $dep->titulo, $message);
        // $message = str_replace('%operador%', $this->session->userdata('loggednameoperador'), $message);
        // $message = str_replace('%protocolo%', $fila->protocolo, $message);

        // $this->insertchatMenuSemRetorno($receive,$number,($message));
        
    }

    function editaragendacontatos($number){

        head_title('Modificar Contatos');
        
        $data['obj'] = $this->agendacontatosmodel->getContato($number);
        
        $this->load->view($this->session->userdata('folder').'/editaragendacontatos',$data);
    }

    function insert()
    {
        $obj = new stdClass();
        $obj->id                    = $_REQUEST['id'];
        //$obj->id_contato            = ($_REQUEST['id_contato']!=''?$_REQUEST['id_contato']:'');
        $obj->nome                  = ($_REQUEST['nome']!=''?$_REQUEST['nome']:'');
        $obj->nomeCompleto          = ($_REQUEST['nomeCompleto']!=''?$_REQUEST['nomeCompleto']:'');
        //$obj->number              = $_REQUEST['number'];
        $obj->complemento           = ($_REQUEST['complemento']!=''?$_REQUEST['complemento']:'');
        $obj->pontoDeReferencia     = ($_REQUEST['pontoDeReferencia']!=''?$_REQUEST['pontoDeReferencia']:'');
        $obj->rua                   = ($_REQUEST['rua']!=''?$_REQUEST['rua']:'');
        $obj->cep                   = ($_REQUEST['cep']!=''?$_REQUEST['cep']:'');
        $obj->cidade                = ($_REQUEST['cidade']!=''?$_REQUEST['cidade']:'');
        $obj->estado                = ($_REQUEST['estado']!=''?$_REQUEST['estado']:'');
        $obj->bairro                = ($_REQUEST['bairro']!=''?$_REQUEST['bairro']:'');
        $obj->numero                = ($_REQUEST['numero']!=''?$_REQUEST['numero']:'');
        $obj->email                 = ($_REQUEST['email']!=''?$_REQUEST['email']:'');
        $obj->facebook              = ($_REQUEST['facebook']!=''?$_REQUEST['facebook']:'');
        $obj->instagram             = ($_REQUEST['instagram']!=''?$_REQUEST['instagram']:'');
        $obj->cpf                   = ($_REQUEST['cpf']!=''?$_REQUEST['cpf']:'');
        $obj->id_cliente            = 1;//$this->session->userdata('loggedincliente');
        $obj->observacoes           = ($_REQUEST['observacoes']!=''?$_REQUEST['observacoes']:'');

        if (is_numeric($obj->id)) {
            
            $update = $this->agendacontatosmodel->update($obj);
            $update = $this->agendacontatosmodel->getById($obj->id);
            $this->filamodel->updateNome($update->number.'@c.us', $obj->nome);
            $this->chatlistmodel->updateNome($update->number.'@c.us', $obj->nome);
            //$this->entregadoresmodel->updateNome($update->number, $obj->nome);

            
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Contato alterado com sucesso!</p>
                </div>');
            
        } else {

            $insert = $this->agendacontatosmodel->insert($obj);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Contato cadastrado com sucesso!</p>
                </div>');
                
            }
        }

        redirect('monitor/'.$this->router->class);
    }

    function downloadpdf($id){
        
        $send = $this->messagesmodel->getById($id);
        header("Content-Type: application/pdf");
        header("Content-Disposition: inline; filename=\'" . 'aa'.'.pdf' . "\';");
        echo file_get_contents('data:application/pdf;base64,'. $send->media_url);
        //data:application/pdf;base64,
    }


	function upload(){
		//print_r($_FILES);

		if($_FILES['file']['name'] != ""){
			$file = $_FILES['file'];
			$config['upload_path']   = './uploads/sends/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|csv|rar|zip|pdf';
			$config['max_size']      = '300000';
			$config['max_width']     = '2000';
			$config['max_height']    = '2000';
			$config['file_name']     = date('ymdHis').rand();
			
			
			$this->load->library('upload', $config);
			
			// Alternativamente voc? pode configurar as prefer?ncias chamando a fun??o initialize. ? ?til se voc? auto-carregar a classe:
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('file'))
			{
				$error = array('error' => $this->upload->display_errors());
				//print_r($error);
				//$this->load->view('upload_form', $error);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				//print_r($data);
				//$this->load->view('upload_success', $data);
				$filename =  $config['file_name'].$data['upload_data']['file_ext'];
			}
		}

		echo $filename;
	}

    function saudacao(){
        $hr = date(" H ");
        if($hr >= 12 && $hr<18) {
        $dtn = "Boa tarde";}
        else if ($hr >= 0 && $hr <12 ){
        $dtn = "Bom dia";}
        else {
        $dtn = "Boa noite";}
        return $dtn;
    }

    function mimetype($mimetype){

        $mimet = array( 
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',

        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',

        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',

        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',

        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',


        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        return $mimet[$mimetype];
    }


    function pdf($receive)
    {

        $this->load->library('libtcpdf');
        
        $messages = $this->messagesmodel->getListWhere(array('receive'=>$receive.'@c.us'));

        //print_r($messages);

        foreach ($messages as $key => $value) {
            $ultimoatendente = $this->sendsmodel->getListWhereLastOperador(array('jid'=>$receive),$value->timestamp);

            if(isset($ultimoatendente->id_atendente) and $value->from_me==1){
                $operador = $this->operadoresmodel->getById($ultimoatendente->id_atendente);
                $value->operadornome = $operador->nome;
            }else{
                $value->operadornome = $cliente->nome;
            }
            

        }

        $html = '<body style="font-family:Times New Roman, Times, serif;">
            <div align="center"><img src="'.a_img('logo.jpg').'" width="150px"></div>
            <h2 style="text-align:center;font-size:40px;">Relatório: Número '.$receive.'<br>Data Gerado:'.date('d/m/Y').'</h2>
            <div style="text-align:justify;font-size:50px;">'.$texto.'</div><br>';

        foreach ($messages as $key => $value) {

            if($value->from_me==1){
                $html .='<div style="text-align:left;">Operador: '.$value->texto.'</div>';
            }else{
                $html .='<div style="text-align:left;">Cliente:'.$value->texto.'</div>';
            }
        }

            
            
        $html .= '</body>'; 
        

        ob_end_clean();
        $this->libtcpdf->to_pdfsembg($html,$receive.'.pdf');
    }

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */