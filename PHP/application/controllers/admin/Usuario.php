<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuario extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		
        if (!$this->session->userdata('loggedin'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('usuariomodel');
		
		head_title('Usuarios');

	}

	function index()
	{
		$this->page();
	}
	
	function adicionar()
	{
		head_title('Adicionar Usuario');
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $modulos);
	}
	
	function editar($id)
	{
		head_title('Modificar Usuario');
		$data['obj'] = $this->usuariomodel->getById($id);

		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form',$data);
	}
	
	function insert()
	{
		$obj = new stdClass();
		$obj->id     = $_REQUEST['id'];
		$obj->nome   = $_POST['nome'];
		$obj->login  = $_POST['login'];
		$obj->email  = $_POST['email'];
		//$obj->perfil = $this->session->userdata('loggedPerfil'); //perfil de quem está logado no momento
		//$obj->perms  = $this->biblioteca_de_funcoes->concatenarPermis($_POST['perms']);
		
		if(is_numeric($obj->id)){
			if($_POST['senha']){
				$senha = isset($_POST['senha'])?$_POST['senha']:null;
				$obj->senha = sha1($senha);
			}
			
			$update = $this->usuariomodel->update($obj);
			
			if($update){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Usuário alterado com sucesso!</p>
	            </div>');
			}
		}else{
			$senha = isset($_POST['senha'])?$_POST['senha']:null;
			$obj->senha = sha1($senha);
			$insert = $this->usuariomodel->insert($obj);
			
			if($insert){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Usuário cadastrado com sucesso!</p>
	            </div>');
				
			}	
		}
		
		redirect($this->session->userdata('folder').$this->router->class);
					
	}

	function update()
	{
		$obj = new stdClass();
		$obj->id     = $_REQUEST['id'];
		$obj->nome   = $_POST['nome'];
		$obj->login  = $_POST['login'];
		$obj->email  = $_POST['email'];
		$obj->perfil = $this->session->userdata('loggedPerfil'); //perfil de quem está logado no momento
		

		if($_POST['senha']){
			$senha = isset($_POST['senha'])?$_POST['senha']:null;
			$obj->senha = sha1($senha);
		}
		
		$update = $this->usuariomodel->update($obj);
		
		if($update){
			set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Usuário alterado com sucesso!</p>
            </div>');
		}
	
		
		redirect($this->session->userdata('folder'));
					
	}
	
	function apagar($id)
	{
		$del = $this->usuariomodel->delById($id);
		if($del){
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Usuário deletado com sucesso!</p>
	            </div>');
		}else{
			set_alert('<div class="alert alert-danger alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Usuário erro ao deletar!</p>
	            </div>');
		}	
		redirect($this->session->userdata('folder').$this->router->class);
	}
	
	function page($page = 0)
	{
		
		$data['list'] = $this->usuariomodel->getList();
    	
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
	function search($page = 0)
	{
		$titulo = $_GET['nome'];
		
		$cont = $this->usuariomodel->totalPaginasSearch($titulo);
		$config['base_url'] = base_url().$this->session->userdata('folder').$this->router->class.'/page/';
        $config['total_rows'] = $cont;
	    		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    $data['list'] = $this->usuariomodel->getListPageSearch($page,$this->pagination->per_page,$titulo);
    	
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
}