<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedin'))
            redirect('login');

		
		$this->load->library('session');
		$this->load->model('clientesmodel');	
		$this->load->model('chatlistmodel');	
		$this->load->model('messagesmodel');	
		
	}

	function index()
	{
		$data['fila'] = $this->chatlistmodel->getListWhereCount();
		$data['messages'] = $this->messagesmodel->getListWhereCount();

		$data['meses'] = $this->messagesmodel->getListMesaMes(date('Y'));

		$this->load->view($this->session->userdata('folder').'dashboard',$data);
	}

	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */