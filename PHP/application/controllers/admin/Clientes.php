<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Clientes extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        if (!$this->session->userdata('loggedin'))
            redirect($this->session->userdata('folder') . 'login');
        
        $this->load->model('clientesmodel');
        head_title('Clientes');
        
    }
    
    function index() {
        $this->page();
    }
    
    
    function ajax_cep($cep) {
        $cep = $cep;
        $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
        
        $dados['sucesso'] = (string) $reg->resultado;
        $dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
        $dados['bairro']  = (string) $reg->bairro;
        $dados['cidade']  = (string) $reg->cidade;
        $dados['estado']  = (string) $reg->uf;
        
        echo json_encode($dados);
        
    }
    
    function adicionar() {
        head_title('Adicionar Clientes');
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $modulos);
    }
    
    function editar($id) {
        head_title('Modificar Cliente');
        
        $data['obj'] = $this->clientesmodel->getById($id);
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $data);
    }
    
    function insert() {
        
        if ($_REQUEST['produto']) {
            foreach ($_REQUEST['produto'] as $key => $value) {
                $prods .= $value . ',';
            }
            $prods = substr($prods, 0, -1);
        } else {
            $prods = '';
        }
        
        
        $obj              = new stdClass();
        $obj->id          = $_REQUEST['id'];
        $obj->nome        = $_REQUEST['nome'];
        //$obj->mae                    = $_REQUEST['mae'];
        $obj->telefone    = $_REQUEST['telefone'];
        $obj->complemento = $_REQUEST['complemento'];
        $obj->endereco    = $_REQUEST['endereco'];
        
        $obj->cep    = $_REQUEST['cep'];
        $obj->cidade = $_REQUEST['cidade'];
        $obj->estado = $_REQUEST['estado'];
        $obj->bairro = $_REQUEST['bairro'];
        $obj->numero = $_REQUEST['numero'];
        
        $obj->email          = $_REQUEST['email'];
        $obj->cnpj           = $_REQUEST['cnpj'];
        $obj->rg_ie          = $_REQUEST['rg_ie'];
        //$obj->sexo                  = $_REQUEST['sexo'];
        $arrDataNasc         = explode("/", $_REQUEST['dataNascimento']);
        $dataNasc            = $arrDataNasc[2] . "-" . $arrDataNasc[1] . "-" . $arrDataNasc[0];
        $obj->dataNascimento = $dataNasc;
        
        $arrDataAtivacao   = explode("/", $_REQUEST['dataativacao']);
        $dataativacao      = $arrDataAtivacao[2] . "-" . $arrDataAtivacao[1] . "-" . $arrDataAtivacao[0];
        $obj->dataativacao = $dataativacao;
        
        $obj->razao             = $_REQUEST['razao'];
        $obj->usuario           = $_REQUEST['usuario'];
        $obj->dialogflow        = $_REQUEST['dialogflow'];
        $obj->dialogflowclient  = $_REQUEST['dialogflowclient'];
        $obj->frases            = $_REQUEST['frases'];
        $obj->menupersonalizado = $_REQUEST['menupersonalizado'];
        $obj->notificador       = $_REQUEST['notificador'];
        $obj->respostas         = $_REQUEST['respostas'];
        $obj->pedidos           = $_REQUEST['pedidos'];
        $obj->responsavel       = $_REQUEST['responsavel'];
        $obj->emailresp         = $_REQUEST['emailresp'];
        $obj->telefoneresp      = $_REQUEST['telefoneresp'];
        $obj->acessogestor      = $_REQUEST['acessogestor'];
        $obj->acessoatendente   = $_REQUEST['acessoatendente'];
        $obj->messenger         = $_REQUEST['messenger'];
        $obj->produto           = $prods;
        $obj->ipserver          = $_REQUEST['ipserver'];
        
        if (is_numeric($obj->id)) {
            if ($_POST['senha']) {
                $senha      = isset($_POST['senha']) ? $_POST['senha'] : null;
                $obj->senha = sha1($senha);
            }
            
            $update = $this->clientesmodel->update($obj);
            
            if ($update) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Dados do cliente alterado com sucesso!</p>
                </div>');
            }
        } else {
            $senha      = isset($_POST['senha']) ? $_POST['senha'] : null;
            $obj->senha = sha1($senha);
            $obj->token = md5(uniqid(rand(), true));
            $insert     = $this->clientesmodel->insert($obj);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Dados do cliente cadastrado com sucesso!</p>
                </div>');
                
            }
        }
        
        redirect($this->session->userdata('folder') . $this->router->class);
        
        
    }
    
    function apagar($id) {
        $del = $this->clientesmodel->delById($id);
        if ($del) {
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Dados do cliente deletado com sucesso!</p>
                </div>');
        } else {
            set_alert('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Dados do cliente apresentou erro ao deletar!</p>
                </div>');
        }
        redirect($this->session->userdata('folder') . $this->router->class);
    }
    
    
    function page($page = 0) {
        
        $data['list'] = $this->clientesmodel->getList();
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
        
        
    }
    
    function search($page = 0) {
        $titulo = $_GET['nome'];
        
        $cont                 = $this->clientesmodel->totalPaginasSearch($titulo);
        $config['base_url']   = base_url() . $this->session->userdata('folder') . $this->router->class . '/page/';
        $config['total_rows'] = $cont;
        
        $this->pagination->initialize($config);
        
        $data['pag']  = $this->pagination->create_links();
        $data['list'] = $this->clientesmodel->getListPageSearch($page, $this->pagination->per_page, $titulo);
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
        
        
    }
    
}