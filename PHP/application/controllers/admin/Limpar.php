<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Limpar extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedin'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('sendsmodel');
		$this->load->model('filamodel');
		$this->load->model('respostasmodel');
		$this->load->model('parametrosmodel');
		$this->load->model('messagesmodel');
		$this->load->model('pedidomodel');
		$this->load->model('entregadoresmodel');
		$this->load->model('bairrosmodel');
		$this->load->model('vendasmodel');
		$this->load->model('chatlistmodel');
		$this->load->model('agendacontatosmodel');
		head_title('Limpar');

	}

	function index()
	{
		$this->page();
	}
	

	function fila()
	{
		$del = $this->filamodel->apagarFila();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Filas excluidas!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function mensagens()
	{
		$del = $this->messagesmodel->apagarMessages();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Mensagens excluidas!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function pedidos()
	{
		$del = $this->pedidomodel->apagarPedidos();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Pedidos excluidos!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function entregadores()
	{
		$del = $this->entregadoresmodel->apagarEntregadores();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Entregadores excluidos!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function bairros()
	{
		$del = $this->bairrosmodel->apagarBairros();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Bairros excluidos!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function vendas()
	{
		$del = $this->vendasmodel->apagarVendas();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Vendas excluidas!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function dialogflow()
	{
		$del = $this->respostasmodel->apagarRespostas();
		$del = $this->parametrosmodel->apagarParametros();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Interações excluida!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function envios()
	{
		$del = $this->sendsmodel->apagarSends();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Envios excluida!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function chatlist()
	{
		$del = $this->chatlistmodel->apagarChatList();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Lista de Contatos excluida!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	function agendacontatos()
	{
		$del = $this->agendacontatosmodel->apagarAgendaContatos();
		
		set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Agenda de Contatos excluida!</p>
            </div>');
	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	
	function page($page = 0)
	{
		
		
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'');
		
		
	}
	
	function search($page = 0)
	{
		$titulo = $_GET['nome'];
		
		$cont = $this->clientesmodel->totalPaginasSearch($titulo);
		$config['base_url'] = base_url().$this->session->userdata('folder').$this->router->class.'/page/';
        $config['total_rows'] = $cont;
	    		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    $data['list'] = $this->clientesmodel->getListPageSearch($page,$this->pagination->per_page,$titulo);
    	
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
}