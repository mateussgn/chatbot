<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Sair extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		head_title('Sair');
		head_keywords('Sair');
		head_description('Sair');
		
		$delcookie = delete_cookie("id_usuario");
		
		
		$this->load->view('login');
		redirect('/');
		
		
	}
	
	function adicionar()
	{
		$this->load->view('professor_form');
	}
	
	function editar($id)
	{
		$data['obj'] = $this->ProfessorModel->getById($id);
		$this->load->view('professor_form',$data);
	}
	
	function insert()
	{
		$obj = new stdClass();
		$obj->id = $_REQUEST['id'];
		$obj->nome = $_POST['titulo'];
		$obj->curriculo = $_POST['curriculo'];
		$obj->foto =  $_FILES['foto']['name'];
		$foto = $_FILES['foto'];
		//echo $foto['name'];
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '10000000';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		
		$this->load->library('upload', $config);
		
		// Alternativamente voc� pode configurar as prefer�ncias chamando a fun��o initialize. � �til se voc� auto-carregar a classe:
		$this->upload->initialize($config);
		
		if ( ! $this->upload->do_upload('foto'))
		{
			$error = array('error' => $this->upload->display_errors());
			//print_r($error);
			//$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			//print_r($data);
			//$this->load->view('upload_success', $data);
		}
				
		
		
		if(is_numeric($obj->id)){
			//update
			
			$update = $this->ProfessorModel->update($obj);
			if($update){
				
				echo '<script>alert("Professor editado")</script>';
				echo '<script>location.href = "'.site_url("professor").'";</script>';	
			}
		}else{
			$insert = $this->ProfessorModel->insert($obj);
			if($insert){
				echo '<script>alert("Professor cadastrado")</script>';
				echo '<script>location.href = "'.site_url("professor").'";</script>';
			}	
		}
		
		
					
	}
	
	function apagar($id)
	{
		$del = $this->ProfessorModel->delById($id);
		if($del){
			echo '<script>alert("Professor Excluido")</script>';
			echo '<script>location.href = "'.site_url("professor").'";</script>';
		}else{
			echo '<script>alert("Erro ao excluir professor")</script>';
			echo '<script>location.href = "'.site_url("professor").'";</script>';
		}
	}
	
	function page($page = 0)
	{
		
		$qntPage = 10;
		if(empty($page)){
			$page = 0;
		}
		$cont = $this->ProfessorModel->totalPaginas();
		$config['base_url'] = base_url().'professor/page/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
		$config['num_links'] = 4;
        $config['total_rows'] = $cont;
	    $config['first_link'] = '';	
	
		//The text you would like shown in the "first" link on the left.
		$config['first_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "first" link.
		$config['first_tag_close'] = '</div>';
	
		//The closing tag for the "first" link.	Customizing the Last Link
		$config['last_link'] = 'Anterior';
	
		//The text you would like shown in the "last" link on the right.
		$config['last_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "last" link.
		$config['last_tag_close'] = '</div>';
	
		//The closing tag for the "last" link.Customizing the "Next" Link
		$config['next_link'] = 'Proximo';
	
		//The text you would like shown in the "next" page link.
		$config['next_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "next" link.
		$config['next_tag_close'] = '</div>';
	
		//The closing tag for the "next" link.Customizing the "Previous" Link
		$config['prev_link'] = 'Anterior';
	
		//The text you would like shown in the "previous" page link.
		$config['prev_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "previous" link.
		$config['prev_tag_close'] = '</div>';
	
		//The closing tag for the "previous" link.Customizing the "Current Page" Link
		$config['cur_tag_open'] = '<div class="paginacao_list_pag ativo">';
	
		//The opening tag for the "current" link.
		$config['cur_tag_close'] = '</div>';
	
		//The closing tag for the "current" link.
		//Customizing the "Digit" Link
		$config['num_tag_open'] = '<div class="paginacao_list_pag">';
	
		//The opening tag for the "digit" link.
		$config['num_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    
    	$data['list'] = $this->ProfessorModel->getListPage($page,$qntPage);
    	
		$this->load->view('professor',$data);
		
		
	}
	
	function search($page = 0)
	{
		$titulo = $_GET['titulo'];
		$qntPage = 10;
		if(empty($page)){
			$page = 0;
		}
		$cont = $this->ProfessorModel->totalPaginasSearch($titulo);
		$config['base_url'] = base_url().'professor/page/';
        $config['per_page'] = 10;
        $config['uri_segment'] = 3;
		$config['num_links'] = 4;
        $config['total_rows'] = $cont;
	    $config['first_link'] = '';	
	
		//The text you would like shown in the "first" link on the left.
		$config['first_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "first" link.
		$config['first_tag_close'] = '</div>';
	
		//The closing tag for the "first" link.	Customizing the Last Link
		$config['last_link'] = 'Anterior';
	
		//The text you would like shown in the "last" link on the right.
		$config['last_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "last" link.
		$config['last_tag_close'] = '</div>';
	
		//The closing tag for the "last" link.Customizing the "Next" Link
		$config['next_link'] = 'Proximo';
	
		//The text you would like shown in the "next" page link.
		$config['next_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "next" link.
		$config['next_tag_close'] = '</div>';
	
		//The closing tag for the "next" link.Customizing the "Previous" Link
		$config['prev_link'] = 'Anterior';
	
		//The text you would like shown in the "previous" page link.
		$config['prev_tag_open'] = '<div class="paginacao_list_after">';
	
		//The opening tag for the "previous" link.
		$config['prev_tag_close'] = '</div>';
	
		//The closing tag for the "previous" link.Customizing the "Current Page" Link
		$config['cur_tag_open'] = '<div class="paginacao_list_pag ativo">';
	
		//The opening tag for the "current" link.
		$config['cur_tag_close'] = '</div>';
	
		//The closing tag for the "current" link.
		//Customizing the "Digit" Link
		$config['num_tag_open'] = '<div class="paginacao_list_pag">';
	
		//The opening tag for the "digit" link.
		$config['num_tag_close'] = '</div>';
		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    
    	$data['list'] = $this->ProfessorModel->getListPageSearch($page,$qntPage,$titulo);
    	
		$this->load->view('professor',$data);
		
		
	}
	
}