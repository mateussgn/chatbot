<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Operadores extends MY_Controller_Operador {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedinoperador'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('operadoresmodel');
		$this->load->model('departamentosmodel');
		head_title('Operadores');

	}

	function index()
	{
		$this->editar($this->session->userdata('loggedinoperador'));
	}

	

    function ajax_cep($cep){
    	$cep = $cep;
    	$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
		 
		$dados['sucesso'] = (string) $reg->resultado;
		$dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
		$dados['bairro']  = (string) $reg->bairro;
		$dados['cidade']  = (string) $reg->cidade;
		$dados['estado']  = (string) $reg->uf;
		 
		echo json_encode($dados);

    } 

	
	function editar($id)
	{
		head_title('Modificar Operadores');
		
		$data['obj'] = $this->operadoresmodel->getById($id);
		$data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedinclienteid')));
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form',$data);
	}
	
	function insert()
	{
		$obj = new stdClass();
		$obj->id      				= $_REQUEST['id'];
		$obj->nome    				= ($_REQUEST['nome']!=''?$_REQUEST['nome']:'');
		$obj->nomecompleto    				= ($_REQUEST['nomecompleto']!=''?$_REQUEST['nomecompleto']:'');
		//$obj->mae    				= ($_REQUEST['mae']!=''?$_REQUEST['mae']:'');
		$obj->telefone				= ($_REQUEST['telefone']!=''?$_REQUEST['telefone']:'');
		$obj->complemento	        = ($_REQUEST['complemento']!=''?$_REQUEST['complemento']:'');
		$obj->endereco				= ($_REQUEST['endereco']!=''?$_REQUEST['endereco']:'');
	    $obj->cep 					= ($_REQUEST['cep']!=''?$_REQUEST['cep']:'');
        $obj->cidade 				= ($_REQUEST['cidade']!=''?$_REQUEST['cidade']:'');
        $obj->estado 				= ($_REQUEST['estado']!=''?$_REQUEST['estado']:'');
        $obj->bairro 				= ($_REQUEST['bairro']!=''?$_REQUEST['bairro']:'');
        $obj->numero 				= ($_REQUEST['numero']!=''?$_REQUEST['numero']:'');

		$obj->email					= ($_REQUEST['email']!=''?$_REQUEST['email']:'');
		$obj->cpf	    			= ($_REQUEST['cpf']!=''?$_REQUEST['cpf']:'');
		$obj->sexo  		        = ($_REQUEST['sexo']!=''?$_REQUEST['sexo']:'');
		$arrDataNasc = explode("/", $_REQUEST['dataNascimento']);
		$dataNasc    = $arrDataNasc[2]."-".$arrDataNasc[1]."-".$arrDataNasc[0];
		$obj->dataNascimento        = $dataNasc;
		$obj->usuario            		= $_REQUEST['usuario']; 
		
		
		if(is_numeric($obj->id)){
			if($_POST['senha']){
				$senha = isset($_POST['senha'])?$_POST['senha']:null;
				$obj->senha = sha1($senha);
			}

			$update = $this->operadoresmodel->update($obj);
			
			if($update){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Operador alterado com sucesso!</p>
	            </div>');
			}
		}else{
			$senha = isset($_POST['senha'])?$_POST['senha']:null;
			$obj->senha = sha1($senha);
			$insert = $this->operadoresmodel->insert($obj);
			
			if($insert){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Operador cadastrado com sucesso!</p>
	            </div>');
				
			}	
		}
		
		redirect($this->session->userdata('folder').$this->router->class);
		
					
	}
	
	
	
	
}