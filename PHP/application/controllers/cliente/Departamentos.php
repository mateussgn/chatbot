<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Departamentos extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('departamentosmodel');
		head_title('Departamentos');

	}

	function index()
	{
		$this->page();
	}
	
	function adicionar()
	{
		head_title('Adicionar Departamentos');
				
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $modulos);
	}
	
	function editar($id)
	{
		head_title('Modificar Cliente');
		
		$data['obj'] = $this->departamentosmodel->getById($id);
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form',$data);
	}
	
	function insert()
	{
	
		$obj = new stdClass();
		$obj->id      				= $_REQUEST['id'];
		$obj->titulo    				= $_REQUEST['titulo'];
		$obj->comando    				= $_REQUEST['comando'];
		$obj->sigla    				= $_REQUEST['sigla'];
		$obj->id_cliente    				= $this->session->userdata('loggedincliente');
		if(is_numeric($obj->id)){
			$update = $this->departamentosmodel->update($obj);
			
			if($update){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Departamento alterado com sucesso!</p>
	            </div>');
			}
		}else{
			$insert = $this->departamentosmodel->insert($obj);
			
			if($insert){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Departamento cadastrado com sucesso!</p>
	            </div>');
				
			}	
		}
		
		redirect($this->router->class);
		
					
	}
	
	function apagar($id)
	{
		$del = $this->departamentosmodel->delById($id);
		if($del){
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Departamento deletado com sucesso!</p>
	            </div>');
		}else{
			set_alert('<div class="alert alert-danger alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Departamento erro ao deletar!</p>
	            </div>');
		}	
		redirect($this->router->class);
	}

	
	function page($page = 0)
	{
		
		$data['list'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
	function search($page = 0)
	{
		$titulo = $_GET['nome'];
		
		$cont = $this->departamentosmodel->totalPaginasSearch($titulo);
		$config['base_url'] = base_url().$this->session->userdata('folder').$this->router->class.'/page/';
        $config['total_rows'] = $cont;
	    		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    $data['list'] = $this->departamentosmodel->getListPageSearch($page,$this->pagination->per_page,$titulo);
    	
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
}