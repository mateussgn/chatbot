<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Notificadorcron extends MY_Controller_Login_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('notificadormodel');
		$this->load->model('sendsmodel');
		$this->load->model('messagesmodel');
		$this->load->model('chatlistmodel');
		head_title('Respostas');

	}
	
	
	function index()
	{
		$this->page();
	}
	
	
	
	function page($page = 0)
	{
		$data['list'] = $this->notificadormodel->getListCron();
		foreach ($data['list'] as $key => $value) {

			if($this->chatlistmodel->getListWhereCount(array('receive'=>$value->telefone)) <= 0 ){
	            $obj = new stdClass();
	            $obj->name = $value->nome;
	            $obj->receive = $value->telefone;
	            $obj->id_cliente = 1;

	            $this->chatlistmodel->insert($obj);
	        }

			//send
			$obj = new stdClass();
            $obj->id                    = '';
            $obj->id_atendente          = 0;
            $obj->jid                   = $value->telefone;
            $obj->number                = $value->telefone;
            $obj->message               = $value->texto;
            //$obj->image               = $value->imagem;
            $obj->status               = 0;
            // if($value->texto<>''){
            //     $obj->media_name               = 'image';
            // }
            $obj->id_cliente            = 1;
            $obj->datatime = strftime("%Y-%m-%d %H:%M:%S" , time());
            
            $this->sendsmodel->insert($obj);


			//mensagem
			$obj = new stdClass();
	        $obj->receive = $value->telefone;
	        $obj->from_me = 0;
	        $obj->texto = $value->texto;
	        //$obj->media_url = $value->imagem;
	        //$obj->media_type = 'image/jpg';
	        //$obj->media_name = 'image';
	        $obj->id_cliente = 1;
	        $obj->timestamp = strftime("%Y-%m-%d %H:%M:%S" , time());
	        $this->messagesmodel->insert($obj);  

	        
	        //update no envio
			$obj = new stdClass();
	        $obj->id = $value->id;
	        $obj->status = 1;
	        $this->notificadormodel->update($obj);  
	        

		}

		
		
		
	}
	
}