<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller_Cliente {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect('login');

		
		$this->load->library('session');
		$this->load->model('clientesmodel');	
		$this->load->model('chatlistmodel');	
		$this->load->model('filamodel');	
		$this->load->model('messagesmodel');	
		$this->load->model('departamentosmodel');	
		$this->load->model('operadoresmodel');	
		$this->load->model('respostasmodel');
		$this->load->model('notificadormodel');	
		
	}

	function index()
	{
		$data['fila'] = $this->chatlistmodel->getListWhereCount(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		
		
		$data['messages'] = $this->messagesmodel->getListWhereCount(array('id_cliente'=>$this->session->userdata('loggedincliente')));

		$data['meses'] = $this->messagesmodel->getListMesaMes(date('Y'),$this->session->userdata('loggedincliente'));

		$data['aguardando'] = $this->filamodel->getListWhereCount(array('id_cliente'=>$this->session->userdata('loggedincliente'),'status'=>0));
		$data['finalizados'] = $this->filamodel->getListWhereCount(array('id_cliente'=>$this->session->userdata('loggedincliente'),'status'=>2));
		$data['atendendo'] = $this->filamodel->getListWhereCount(array('id_cliente'=>$this->session->userdata('loggedincliente'),'status'=>1));
		
		$data['atendimentos'] = $this->filamodel->getCountAtendimentos();
		$data['notificacoesEnviadas'] = $this->notificadormodel->getCountNotificacoesEnviadas();

	
		$data['depmaisacessado'] = $this->filamodel->getListDepartamentoMaisAcessado();
		foreach ($data['depmaisacessado'] as $key => $value) {
			$dep = $this->departamentosmodel->getById($value->id_departamento);
			$value->departamento = $dep->titulo;
		}

		$data['atendentesmaisativos'] = $this->filamodel->getListAtendentesMaisAtivos();
		foreach ($data['atendentesmaisativos'] as $key => $value) {
			$op = $this->operadoresmodel->getById($value->id_atendente);
			$value->atendente = $op->nome;
		}

		$data['menus'] = $this->respostasmodel->getList();
		foreach ($data['menus'] as $key => $value) {
			if($value->frases<>''){
			$counts = explode(',', $value->frases);
			foreach ($counts as $keyc => $valuec) {
			 	$counts =+ $this->messagesmodel->getListWhereSearchCount($valuec);
			}
			}
			$value->count = $counts;
		}
		
		$data['clicou'] = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
		
		//print_r($data['menus']);
		//die();

		$this->load->view($this->session->userdata('folder').'dashboard',$data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */