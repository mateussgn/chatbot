<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Notificador extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('notificadormodel');
		$this->load->model('notificadormensagemmodel');
		$this->load->model('tagsmodel');
		$this->load->model('departamentosmodel');
		head_title('Respostas');

	}
	
	
	function index()
	{
		$this->page();
	}
	
	function adicionar()
	{
		head_title('Adicionar Notificador Mensagem');
		$data['mensagens'] = $this->notificadormensagemmodel->getList();
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}

	function editar($id)
	{
		head_title('Adicionar Notificador Mensagem');
		$data['mensagens'] = $this->notificadormensagemmodel->getList();
		$data['obj'] = $this->notificadormodel->getById($id);
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}
	
	
	function insert()
	{	
		$msg = $this->notificadormensagemmodel->getById($_POST['id_mensagem']);				

		$csv = array_map("str_getcsv", file($_FILES['file']['tmp_name'])); 
		$count = 0;
		foreach ($csv as $c) {
			if($count <> 0){
				$dataenvio = date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +".$count." minutes"));

				$campos = explode(';', $c[0]);
				$nome = $campos[0];
				$telefone = $campos[1].'@c.us';
				$info1 = $campos[2];
				$info2 = $campos[3];
				$info3 = $campos[4];	
				$info4 = $campos[5];	

				$texto = str_replace('#Nome', $nome, $msg->texto);
				$texto = str_replace('#Fone', $telefone, $texto);
				$texto = str_replace('#Info1', $info1, $texto);
				$texto = str_replace('#Info2', $info2, $texto);
				$texto = str_replace('#Info3', $info3, $texto);
				$texto = str_replace('#Info4', $info4, $texto);

				$obj = new stdClass();
				$obj->id = '';
				$obj->id_mensagem = $_POST['id_mensagem'];
				$obj->nome  = $nome;
				$obj->telefone = $telefone;
				$obj->texto  = $texto;
				$obj->imagem  = $msg->foto;
				$obj->dataenvio  = $dataenvio;
				
				$insert = $this->notificadormodel->insert($obj);
			}
			$count++;
		}

		if($insert){
			set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Notificador cadastrado com sucesso!</p>
            </div>');
			
		}	
		redirect($this->router->class);
		
					
	}

	function apagar($id)
	{
		$del = $this->notificadormodel->delById($id);
		if($del){
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Envio deletado com sucesso!</p>
	            </div>');
		}else{
			set_alert('<div class="alert alert-danger alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Envio erro ao deletar!</p>
	            </div>');
		}	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	
	
	function page($page = 0)
	{
		$data['list'] = $this->notificadormodel->getListGroupMensagem();

		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
		
	}

	function view($id)
	{
		$data['mensagem'] = $this->notificadormensagemmodel->getById($id);
		
		$data['list'] = $this->notificadormodel->getListWhere(array('id_mensagem' => $id));
		
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_view',$data);
		
		
		
	}
	
}