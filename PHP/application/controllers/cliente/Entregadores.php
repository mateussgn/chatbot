<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
id_cliente INT
chave VARCHAR
interacao VARCHAR
*/

class Entregadores extends MY_Controller_Cliente {
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');
        if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
        
        $this->load->model('entregadoresmodel');
        $this->load->model('departamentosmodel');
        head_title('Entregadores');

    }

    function index()
    {
        $this->page();
    }

    function adicionar() {
        head_title('Adicionar Entregador');

        $data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $modulos);
    }

    function editar($id) {
        head_title('Editar Entregador');

        $data['obj']  = $this->entregadoresmodel->getById($id);
        $data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $data);
    }

    function page($page = 0) {
        $data['list'] = $this->entregadoresmodel->getListWhere(array('id_cliente' => $this->session->userdata('loggedincliente')));
                
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
    }

    function insert() {
        
        $obj                    = new stdClass();
        $obj->id                = $_REQUEST['id'];
        $obj->nome              = isset($_REQUEST['nome']) ? $_REQUEST['nome'] : '';
        $obj->nomeCompleto      = isset($_REQUEST['nomeCompleto']) ? $_REQUEST['nomeCompleto'] : '';
        $obj->telefone          = isset($_REQUEST['telefone']) ? $_REQUEST['telefone'] : '';
        $obj->email             = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
        $obj->rua               = isset($_REQUEST['rua']) ? $_REQUEST['rua']:'';
        $obj->bairro            = isset($_REQUEST['bairro']) ? $_REQUEST['bairro'] : '';
        $obj->cidade            = isset($_REQUEST['cidade']) ? $_REQUEST['cidade'] : '';
        $obj->estado            = isset($_REQUEST['estado']) ? $_REQUEST['estado'] : '';
        $obj->complemento       = isset($_REQUEST['complemento']) ? $_REQUEST['complemento'] : '';
        $obj->pontoDeReferencia = isset($_REQUEST['pontoDeReferencia']) ? $_REQUEST['pontoDeReferencia']:'';
        $obj->numero            = isset($_REQUEST['numero']) ? $_REQUEST['numero'] : '';
        $obj->cep               = isset($_REQUEST['cep']) ? $_REQUEST['cep'] : '';
        $obj->cpf               = isset($_REQUEST['cpf']) ? $_REQUEST['cpf'] : '';
        $obj->facebook          = isset($_REQUEST['facebook']) ? $_REQUEST['facebook']:'';
        $obj->instagram         = isset($_REQUEST['instagram']) ? $_REQUEST['instagram']:'';
        $obj->id_cliente        = $this->session->userdata('loggedincliente');
        $obj->observacoes       = isset($_REQUEST['observacoes']) ? $_REQUEST['observacoes']:'';
        $obj->horainicioseg     = isset($_REQUEST['horainicioseg']) ? $_REQUEST['horainicioseg']:'';
        $obj->horainicioter     = isset($_REQUEST['horainicioter']) ? $_REQUEST['horainicioter']:'';
        $obj->horainicioqua     = isset($_REQUEST['horainicioqua']) ? $_REQUEST['horainicioqua']:'';
        $obj->horainicioqui     = isset($_REQUEST['horainicioqui']) ? $_REQUEST['horainicioqui']:'';
        $obj->horainiciosex     = isset($_REQUEST['horainiciosex']) ? $_REQUEST['horainiciosex']:'';
        $obj->horainiciosab     = isset($_REQUEST['horainiciosab']) ? $_REQUEST['horainiciosab']:'';
        $obj->horainiciodom     = isset($_REQUEST['horainiciodom']) ? $_REQUEST['horainiciodom']:'';
        $obj->horafimseg        = isset($_REQUEST['horafimseg']) ? $_REQUEST['horafimseg']:'';
        $obj->horafimter        = isset($_REQUEST['horafimter']) ? $_REQUEST['horafimter']:'';
        $obj->horafimqua        = isset($_REQUEST['horafimqua']) ? $_REQUEST['horafimqua']:'';
        $obj->horafimqui        = isset($_REQUEST['horafimqui']) ? $_REQUEST['horafimqui']:'';
        $obj->horafimsex        = isset($_REQUEST['horafimsex']) ? $_REQUEST['horafimsex']:'';
        $obj->horafimsab        = isset($_REQUEST['horafimsab']) ? $_REQUEST['horafimsab']:'';
        $obj->horafimdom        = isset($_REQUEST['horafimdom']) ? $_REQUEST['horafimdom']:'';

        if($_REQUEST['id_departamento']){
            foreach ($_REQUEST['id_departamento'] as $key => $value) {
                $id_departamento .= $value.',';
            }
            $id_departamento = substr($id_departamento, 0,-1);
            $obj->id_departamento = $id_departamento;
        }

        if($_REQUEST['dias']){
            foreach ($_REQUEST['dias'] as $key => $value) {
                $dias .= $value.',';
            }
            $dias = substr($dias, 0,-1);
            $obj->dias = $dias;
        }

        $obj->id_cliente      = $this->session->userdata('loggedincliente');

        $arrDataNasc          = explode("/", $_REQUEST['dataNascimento']);
        $dataNasc             = $arrDataNasc[2]."-".$arrDataNasc[1]."-".$arrDataNasc[0];
        $obj->created_at      = $dataNasc;

        if (is_numeric($obj->id)) {
            
            $update = $this->entregadoresmodel->update($obj);
            
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Entregador alterado com sucesso!</p>
                </div>');
            
        } else {
            $insert = $this->entregadoresmodel->insert($obj);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Entregador cadastrado com sucesso!</p>
                </div>');
                
            }
        }
        
        redirect($this->router->class);
    }

    function apagar($id) {
        $del = $this->entregadoresmodel->delById($id);
        if ($del) {
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Entregador deletado com sucesso!</p>
                </div>');
        } else {
            set_alert('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Entregador apresentou erro ao deletar!</p>
                </div>');
        }
        redirect($this->router->class);
        //redirect($this->session->userdata('folder').$this->router->class);

    }
}