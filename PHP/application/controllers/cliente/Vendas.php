<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
id_cliente INT
chave VARCHAR
interacao VARCHAR
*/

class Vendas extends MY_Controller_Cliente {
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');
        if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
        
        $this->load->model('departamentosmodel');
        $this->load->model('messagesmodel');
        $this->load->model('chatlistmodel');
        $this->load->model('vendasmodel');
        $this->load->model('operadoresmodel');
        $this->load->model('bairrosmodel');
        $this->load->model('entregadoresmodel');
        $this->load->model('filamodel');
        head_title('Vendas');

    }

    function index()
    {
        $this->page();
    }

    function page($page = 0) {
        head_title('Vendas');

        //Recebendo os valores da pesquisa via post
        $protocolo = $_POST['protocolo'];
        $numero = $_POST['numero'];
        $status = $_POST['status'];
        $id_operador = $_POST['id_operador'];
        $id_departamento = $_POST['id_departamento'];
        
        //troca os -- por ''(vazio)
        $datainicio = ($this->dateformat($_POST['datainicio'],'EUA')) == '--'?'':$this->dateformat($_POST['datainicio'],'EUA');
        $datafim = ($this->dateformat($_POST['datafim'],'EUA')) =='--'?'':$this->dateformat($_POST['datafim'],'EUA');
        
        //Carregando os combobox
        $data['listDep'] = $this->departamentosmodel->getListAll();
        $data['listOper'] = $this->operadoresmodel->getListAll();

        if($datainicio<>'' and $datafim<>''){
            $arrayData = array('datahora >=' => $datainicio.' 00:00:01', 'datahora <=' => $datafim.' 23:59:59');
        
            $ArrayCamposCombobox = array(
                'fila.protocolo' => $protocolo,
                'fila.receive'=> $numero,
                'fila.status' => $status,
                'fila.id_atendente' => $id_operador,
                'fila.id_departamento' =>$id_departamento
            );


            // gera um array apenas com os campos que foram preenchidos na pesquisa
            foreach ($ArrayCamposCombobox as $key => $value) {
                if($value <>''){
                    $arrayCamposValidos[$key] = $value; 
                }
            }
            
            //Retorna a lista detalhada
            $data['list'] = $this->vendasmodel->getListWhereDetalhada($arrayData,$arrayCamposValidos);
            
            $data['protocolo'] = $_POST['protocolo'];
            $data['numero'] = $_POST['numero'];
            $data['status'] = $_POST['status'];
            $data['id_operador'] = $_POST['id_operador'];
            $data['id_departamento'] = $_POST['id_departamento'];   
            $data['datainicio'] = $_POST['datainicio'];
            $data['datafim'] = $_POST['datafim'];
                        
        }else{
            $arrayData = array();
        
            $ArrayCamposCombobox = array(
                'fila.protocolo' => $protocolo,
                'fila.receive'=> $numero,
                'fila.status' => $status,
                'fila.id_atendente' => $id_operador,
                'fila.id_departamento' =>$id_departamento
            );


            // gera um array apenas com os campos que foram preenchidos na pesquisa
            foreach ($ArrayCamposCombobox as $key => $value) {
                if($value <>''){
                    $arrayCamposValidos[$key] = $value; 
                }
            }

            //Retorna a lista total
            $data['list'] = $this->vendasmodel->getListWhereDetalhada($arrayData,$arrayCamposValidos);
        }

        //Atribuí o nome para cada status, bairro e entregador
        foreach ($data['list'] as $key => $value) {

                
            switch($value->status){
                case 0:
                    $value->status = 'Aguardando';
                break;
                case 1:
                    $value->status = 'Atendendo';
                break;
                case 2:
                    $value->status = 'Concluída';
                break;
                case 3:
                    $value->status = 'Abandono';
                break;
    
            }

            if($value->id_bairro<>0){
                $op = $this->bairrosmodel->getById($value->id_bairro);
                $value->id_bairro = $op->nome;
            }

            if($value->id_entregador<>0){
                $op = $this->entregadoresmodel->getById($value->id_entregador);
                $value->id_entregador = $op->nome;
            }

            if($value->id_atendente<>0){
                $op = $this->operadoresmodel->getById($value->id_atendente);
                $value->operador = $op->nome;
            }

            
            //Realiza o cálculo de duração do atentimento
            $dataAtendimento = new DateTime($value->dataatendimento);
            $dataTermino = new DateTime($value->datatermino);
            $duracao = $dataTermino->diff($dataAtendimento);
            $value->duracao = $duracao->format('%H:%I:%S');
        }
                
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
    }

    function dateformat($data,$tipo)
    {
        if($tipo == 'BRA'){
            $data = explode('-', $data);
            return $data[2].'/'.$data[1].'/'.$data[0];
        }

        if($tipo == 'EUA'){
            $data = explode('/', $data);
            return $data[2].'-'.$data[1].'-'.$data[0];
        }
    }

}