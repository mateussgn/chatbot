<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
id_cliente INT
chave VARCHAR
interacao VARCHAR
*/

class Fluxopedidos extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('interacoesmodel');
		head_title('Fluxo de Pedidos');

	}

	function index()
	{
		$this->page();
	}

    function editar($id) {
        head_title('Editar Interação');
        $data['obj']  = $this->interacoesmodel->getById($id);
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $data);
    }

    function page($page = 0) {
        $data['list'] = $this->interacoesmodel->getListWhere(array(
            'id_cliente' => $this->session->userdata('loggedincliente')
        ));
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
    }

    function insert() {
        
        $obj             = new stdClass();
        $obj->id         = $_REQUEST['id'];
        $obj->descricao      = isset($_REQUEST['descricao']) ? $_REQUEST['descricao'] : '';
        $obj->interacao  = isset($_REQUEST['interacao']) ? $_REQUEST['interacao'] : '';
        $obj->id_cliente = $this->session->userdata('loggedincliente');

        if (is_numeric($obj->id)) {
            
            $update = $this->interacoesmodel->update($obj);
            
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Interação alterada com sucesso!</p>
                </div>');
            
        } else {
            $insert = $this->interacoesmodel->insert($obj);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Interação cadastrada com sucesso!</p>
                </div>');
                
            }
        }
        
        redirect($this->router->class);
    }
}