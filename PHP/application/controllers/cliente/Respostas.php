'<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Respostas extends MY_Controller_Cliente {
    
    function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder') . 'login');
        
        $this->load->model('respostasmodel');
        $this->load->model('departamentosmodel');
        $this->load->model('tagsmodel');
        $this->load->model('tagsdialogflowmodel');
        $this->load->model('parametrosmodel');
        head_title('Respostas');
        
    }
    
    function index() {
        $this->page();
    }
    
    
    function ajax_cep($cep) {
        $cep = $cep;
        $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
        
        $dados['sucesso'] = (string) $reg->resultado;
        $dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
        $dados['bairro']  = (string) $reg->bairro;
        $dados['cidade']  = (string) $reg->cidade;
        $dados['estado']  = (string) $reg->uf;
        
        echo json_encode($dados);
        
    }
    
    function adicionar() {
        head_title('Adicionar Respostas');
        $data['cliente']        = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $data['departamentos']  = $this->departamentosmodel->getListWhere(array(
            'id_cliente' => $this->session->userdata('loggedincliente')
        ));
        $data['tags']           = $this->tagsmodel->getList();
        $data['tagsDialogflow'] = $this->tagsdialogflowmodel->getList();
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $data);
    }
    
    function editar($id) {
        
        head_title('Modificar Respostas');
        $data['cliente']       = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $data['obj']           = $this->respostasmodel->getById($id);
        $data['departamentos'] = $this->departamentosmodel->getListWhere(array(
            'id_cliente' => $this->session->userdata('loggedincliente')
        ));
        
        $data['perguntas'] = $this->parametrosmodel->getListWhere(array(
            'id_intencao' => $id
        ));
        
        $data['tags']           = $this->tagsmodel->getList();
        $data['tagsDialogflow'] = $this->tagsdialogflowmodel->getList();
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $data);
    }
    
    function insert() {
        $obj             = new stdClass();
        $obj->id         = $_REQUEST['id'];
        $obj->intencao   = ($_REQUEST['intencao'] != '' ? $_REQUEST['intencao'] : '');
        $obj->frases     = ($_REQUEST['frases'] != '' ? $_REQUEST['frases'] : '');
        $obj->respostas  = ($_REQUEST['respostas'] != '' ? $_REQUEST['respostas'] : '');
        $obj->id_cliente = $this->session->userdata('loggedincliente');
        
        if (is_numeric($obj->id)) {
            $update = $this->respostasmodel->update($obj);
            
            
            $this->parametrosmodel->delByIdAllIntencao($obj->id);
            
            if ($_REQUEST['campo']) {
                foreach ($_REQUEST['campo'] as $key => $value) {
                    if ($value <> '') {
                        $campo    = $value;
                        $prompts  = $_REQUEST['prompts'][$key];
                        $dataType = $_REQUEST['dataType'][$key];
                        
                        $objp              = new stdClass();
                        $objp->id          = '';
                        $objp->id_intencao = $obj->id;
                        $objp->dataType    = $dataType;
                        $objp->name        = $campo;
                        $objp->value       = '$' . $campo;
                        $objp->prompts     = $prompts;
                        $this->parametrosmodel->insert($objp);
                    }
                }
            }
            
            
            $this->dialogflowupdate($_REQUEST['id']);
            
            
            if ($update) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Resposta alterada com sucesso!</p>
                </div>');
            }
        } else {
            $insert  = $this->respostasmodel->insert($obj);
            $last_id = $this->db->insert_id();
            
            if ($_REQUEST['campo']) {
                foreach ($_REQUEST['campo'] as $key => $value) {
                    if ($value <> '') {
                        $campo    = $value;
                        $prompts  = $_REQUEST['prompts'][$key];
                        $dataType = $_REQUEST['dataType'][$key];
                        
                        $objp              = new stdClass();
                        $objp->id          = '';
                        $objp->id_intencao = $last_id;
                        $objp->dataType    = $dataType;
                        $objp->name        = $campo;
                        $objp->value       = '$' . $campo;
                        $objp->prompts     = $prompts;
                        $this->parametrosmodel->insert($objp);
                    }
                }
            }
            
            
            //cria e atualiza o id do dialogflow
            $id_dialogflow       = $this->dialogflowcreate($last_id);
            $obj2                = new stdClass();
            $obj2->id            = $last_id;
            $obj2->id_dialogflow = $id_dialogflow;
            $this->respostasmodel->update($obj2);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Resposta cadastrada com sucesso!</p>
                </div>');
                
            }
        }
        
        redirect($this->router->class);
        
        
    }
    
    function apagar($id) {
        $this->dialogflowdelete($id);
        redirect($this->router->class);
    }
    
    
    function page($page = 0) {
        $this->dialogflowlist();
        
        $this->dialogflowlistCreate();
        
        $data['list'] = $this->respostasmodel->getListWhere(array(
            'id_cliente' => $this->session->userdata('loggedincliente')
        ));
        foreach ($data['list'] as $key => $value) {
            $deps = explode(',', $value->id_departamento);
            foreach ($deps as $key1 => $value1) {
                $deps1 = $this->departamentosmodel->getById($value1);
                $departamentos .= $deps1->titulo . ',';
            }
            $value->departamentos = substr($departamentos, 0, -1);
            
        }
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
        
        
    }
    
    function search($page = 0) {
        $titulo = $_GET['nome'];
        
        $cont                 = $this->respostasmodel->totalPaginasSearch($titulo);
        $config['base_url']   = base_url() . $this->session->userdata('folder') . $this->router->class . '/page/';
        $config['total_rows'] = $cont;
        
        $this->pagination->initialize($config);
        
        $data['pag']  = $this->pagination->create_links();
        $data['list'] = $this->respostasmodel->getListPageSearch($page, $this->pagination->per_page, $titulo);
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
        
        
    }
    
    function dialogflowcreate($intencao) {
        
        $cliente   = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $resposta  = $this->respostasmodel->getById($intencao);
        $perguntas = $this->parametrosmodel->getListWhere(array(
            'id_intencao' => $intencao
        ));
        $respostas = utf8_decode($resposta->respostas);
        $intencao  = utf8_decode($resposta->intencao);
        $frases    = explode(',', $resposta->frases);
        
        $parameters = '';
        $usersays   = '';
        if (count($perguntas) > 0) {
            foreach ($perguntas as $key => $value) {
                $parameters .= '{
              "dataType": "' . $value->dataType . '",
              "isList": true,
              "name": "' . $value->name . '",
              "prompts": [
                "' . utf8_decode($value->prompts) . '"
              ],
              "required": true,
              "value": "' . $value->value . '"},';
                
                $usersays .= '{
              "count": 0,
              "data": [
                {
                  "alias": "' . $value->name . '",
                  "meta": "' . $value->dataType . '",
                  "text": "' . $value->name . '",
                  "userDefined": true
                }
              ]
            },';
                
            }
            $parameters = substr($parameters, 0, -1);
            $usersays   = substr($usersays, 0, -1);
            
        }
        
        
        if (count($frases) > 0) {
            foreach ($frases as $key => $value) {
                if ($value <> '') {
                    $frasestxt .= ' {
                  "count": 0,
                  "isTemplate": true,
                  "data": [
                    {
                      "text": "' . utf8_decode($value) . '"
                    }
                  ]
                },';
                }
            }
        }
        $frasestxt = substr($frasestxt, 0, -1);
        
        if ($usersays <> '') {
            $frasestxt = ($frasestxt <> '') ? $frasestxt . ',' . $usersays : $usersays;
        }
        
        if ($cliente->frases <> 1) {
            $frasestxt = '';
        }
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dialogflow.com/v1/intents?v=20150910",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n  \"contexts\": [],\r\n  \"events\": [],\r\n  \"fallbackIntent\": false,\r\n  \"name\": \"$intencao\",\r\n  \"priority\": 500000,\r\n  \"responses\": [\r\n    {\r\n      \"action\": \"\",\r\n      \"affectedContexts\": [],\r\n      \"defaultResponsePlatforms\": {\r\n        \"google\": true\r\n      },\r\n      \"messages\": [\r\n        {\r\n          \"speech\": \"$respostas\",\r\n          \"type\": 0\r\n        }\r\n      ],\r\n      \"parameters\": [$parameters],\r\n      \"resetContexts\": false\r\n    }\r\n  ],\r\n  \"templates\": [],\r\n  \"userSays\": [\r\n  $frasestxt  \r\n  ],\r\n  \"webhookForSlotFilling\": false,\r\n  \"webhookUsed\": false\r\n}",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $cliente->dialogflow . "",
                "cache-control: no-cache",
                "content-type: application/json"
            )
        ));
        
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
            return 0;
        } else {
            echo $response;
            $responsearray = json_decode($response);
            return $responsearray->id;
        }
        
        
    }
    
    function dialogflowlist() {
        $cliente = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $curl    = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dialogflow.com/v1/intents?v=20150910",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $cliente->dialogflow,
                "cache-control: no-cache",
                "content-type: application/json"
            )
        ));
        
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        
        curl_close($curl);
        
        $responsearray = json_decode($response);
        foreach ($responsearray as $key => $value) {
            $menu          = $value->name;
            $id_dialogflow = $value->id;
            
            $resp = $this->respostasmodel->getListWhere(array(
                'id_cliente' => $this->session->userdata('loggedincliente'),
                'id_dialogflow' => $id_dialogflow
            ));
            
            if (!isset($resp['0']->id)) {
                
                $curl2 = curl_init();
                curl_setopt_array($curl2, array(
                    CURLOPT_URL => "https://api.dialogflow.com/v1/intents/" . $value->id,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "authorization: Bearer " . $cliente->dialogflow . "",
                        "cache-control: no-cache",
                        "content-type: application/json"
                    )
                ));
                
                $response2 = curl_exec($curl2);
                $err2      = curl_error($curl2);
                
                curl_close($curl2);
                //echo $response2;
                //echo '<br>';
                $responsearray2 = json_decode($response2);
                //echo '<pre>';print_r($responsearray2);
                //die();
                
                
                $gettype = $responsearray2->responses['0']->messages['0']->speech;
                if (gettype($gettype) == 'array') {
                    $texto = $responsearray2->responses['0']->messages['0']->speech['0'];
                } else {
                    $texto = $responsearray2->responses['0']->messages['0']->speech;
                }
                
                if (isset($responsearray2->userSays)) {
                    $frases = '';
                    foreach ($responsearray2->userSays as $key => $value) {
                        $frases .= $value->data['0']->text . ',';
                    }
                    $frases = substr($frases, 0, -1);
                } else {
                    $frases = '';
                }
                
                $events         = $responsearray2->events['0']->name;
                $action         = $responsearray2->responses['0']->action;
                $fallbackIntent = $responsearray2->fallbackIntent;
                $auto           = $responsearray2->auto;
                
                //echo '<pre>';print_r($responsearray2);
                
                
                $obj                 = new stdClass();
                $obj->id             = '';
                $obj->intencao       = $menu;
                $obj->frases         = $frases;
                $obj->respostas      = $texto;
                $obj->action         = $action;
                $obj->fallbackIntent = $fallbackIntent;
                $obj->events         = $events;
                $obj->auto           = $auto;
                $obj->id_cliente     = $this->session->userdata('loggedincliente');
                $obj->id_dialogflow  = $id_dialogflow;
                
                $this->respostasmodel->insert($obj);
            }
            
        }
        
        
        
    }
    
    function dialogflowlistCreate() {
        
        $cliente = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $curl    = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dialogflow.com/v1/intents?v=20150910",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $cliente->dialogflow . "",
                "cache-control: no-cache",
                "content-type: application/json"
            )
        ));
        
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        
        curl_close($curl);
        
        $responsearray = json_decode($response);
        
        //echo '<pre>';print_r($responsearray);
        //die();
        
        $array = array();
        foreach ($responsearray as $key => $value) {
            $id_dialogflow = $value->id;
            array_push($array, $id_dialogflow);
        }
        
        $data['list'] = $this->respostasmodel->getListWhere(array(
            'id_cliente' => $this->session->userdata('loggedincliente')
        ));
        
        foreach ($data['list'] as $key => $value) {
            if (in_array($value->id_dialogflow, $array)) {
                
            } else {
                
                $this->dialogflowcreate($value->id);
                
            }
        }
        
        
        
    }
    
    
    function dialogflowupdate($intencao) {
        
        $cliente   = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $resposta  = $this->respostasmodel->getById($intencao);
        $perguntas = $this->parametrosmodel->getListWhere(array(
            'id_intencao' => $intencao
        ));
        $respostas = utf8_decode($resposta->respostas);
        $intencao  = utf8_decode($resposta->intencao);
        $frases    = explode(',', $resposta->frases);
        
        $parameters = '';
        $usersays   = '';
        if (count($perguntas) > 0) {
            foreach ($perguntas as $key => $value) {
                $parameters .= '{
              "dataType": "' . $value->dataType . '",
              "isList": true,
              "name": "' . $value->name . '",
              "prompts": [
                "' . utf8_decode($value->prompts) . '"
              ],
              "required": true,
              "value": "' . $value->value . '"},';
                
                $usersays .= '{
              "count": 0,
              "data": [
                {
                  "alias": "' . $value->name . '",
                  "meta": "' . $value->dataType . '",
                  "text": "' . $value->name . '",
                  "userDefined": true
                }
              ]
            },';
                
            }
            $parameters = substr($parameters, 0, -1);
            $usersays   = substr($usersays, 0, -1);
            
        }
        
        
        if (count($frases) > 0) {
            foreach ($frases as $key => $value) {
                if ($value <> '') {
                    $frasestxt .= ' {
                  "count": 0,
                  "isTemplate": true,
                  "data": [
                    {
                      "text": "' . utf8_decode($value) . '"
                    }
                  ]
                },';
                }
            }
        }
        $frasestxt = substr($frasestxt, 0, -1);
        
        if ($usersays <> '') {
            $frasestxt = ($frasestxt <> '') ? $frasestxt . ',' . $usersays : $usersays;
            
            
        }
        if ($cliente->frases <> 1) {
            $frasestxt = '';
        }
        
        //echo $frasestxt;
        //die();
        
        if ($resposta->action == 'input.welcome' or $resposta->action == 'input.unknown') {
            if ($resposta->fallbackIntent == 1) {
                $fallbackIntent = 'true';
            } else {
                $fallbackIntent = 'false';
            }
            
            if ($resposta->events <> '') {
                $events = '{ "name": "' . $resposta->events . '" }';
            } else {
                $events = '';
            }
            
            $action = $resposta->action;
            
            $POSTFIELDS = "{\r\n  \"contexts\": [],\r\n  \"auto\": true,\r\n  \"events\": [$events],\r\n  \"fallbackIntent\": $fallbackIntent,\r\n  \"name\": \"$intencao\",\r\n  \"priority\": 500000,\r\n  \"responses\": [\r\n    {\r\n      \"action\": \"$action\",\r\n      \"affectedContexts\": [],\r\n      \"defaultResponsePlatforms\": {\r\n        \"google\": true\r\n      },\r\n      \"messages\": [\r\n        {\r\n          \"speech\": \"$respostas\",\r\n          \"type\": 0\r\n        }\r\n      ],\r\n      \"parameters\": [ $parameters ],\r\n      \"resetContexts\": false\r\n    }\r\n  ],\r\n  \"templates\": [],\r\n  \"userSays\": [\r\n  $frasestxt  \r\n  ],\r\n  \"webhookForSlotFilling\": false,\r\n  \"webhookUsed\": false\r\n}";
            
        } else {
            $POSTFIELDS = "{\r\n  \"contexts\": []," . $auto . "" . $events . "" . $fallbackIntent . "\r\n  \"name\": \"$intencao\",\r\n  \"priority\": 500000,\r\n  \"responses\": [\r\n    {" . $action . "\r\n      \"affectedContexts\": [],\r\n      \"defaultResponsePlatforms\": {\r\n        \"google\": true\r\n      },\r\n      \"messages\": [\r\n        {\r\n          \"speech\": \"$respostas\",\r\n          \"type\": 0\r\n        }\r\n      ],\r\n      \"parameters\": [ $parameters ],\r\n      \"resetContexts\": false\r\n    }\r\n  ],\r\n  \"templates\": [],\r\n  \"userSays\": [\r\n  $frasestxt  \r\n  ],\r\n  \"webhookForSlotFilling\": false,\r\n  \"webhookUsed\": false\r\n}";
        }
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dialogflow.com/v1/intents/" . $resposta->id_dialogflow . "?v=20150910",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => $POSTFIELDS,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $cliente->dialogflow . "",
                "cache-control: no-cache",
                "content-type: application/json"
            )
        ));
        
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
            return 0;
        } else {
            echo $response;
            $responsearray = json_decode($response);
            return $responsearray->id;
        }
        
    }
    
    function dialogflowdelete($intencao) {
        
        $cliente  = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));
        $resposta = $this->respostasmodel->getById($intencao);
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.dialogflow.com/v1/intents/" . $resposta->id_dialogflow . "?v=20150910",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $cliente->dialogflow . "",
                "cache-control: no-cache",
                "content-type: application/json"
            )
        ));
        
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            echo "cURL Error #:" . $err;
            return 0;
        } else {
            echo $response;
            //$responsearray = json_decode($response);
            //return $responsearray->id;
        }
        
        $del = $this->respostasmodel->delById($intencao);
        if ($del) {
            
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Operador deletado com sucesso!</p>
                </div>');
        } else {
            set_alert('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Dados Operador apresentou erro ao deletar!</p>
                </div>');
        }
    }
    
}