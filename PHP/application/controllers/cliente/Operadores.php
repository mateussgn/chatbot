<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Operadores extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('operadoresmodel');
		$this->load->model('departamentosmodel');
		head_title('Operadores');

	}

	function index()
	{
		$this->page();
	}
	

    function ajax_cep($cep){
    	$cep = $cep;
    	$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
		 
		$dados['sucesso'] = (string) $reg->resultado;
		$dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
		$dados['bairro']  = (string) $reg->bairro;
		$dados['cidade']  = (string) $reg->cidade;
		$dados['estado']  = (string) $reg->uf;
		 
		echo json_encode($dados);

    } 

	function adicionar()
	{
		head_title('Adicionar Operadores');
				
		$data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}
	
	function editar($id)
	{
		head_title('Modificar Operadores');
		
		$data['obj'] = $this->operadoresmodel->getById($id);
		$data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form',$data);
	}
	
	function insert()
	{
		$obj = new stdClass();
		$obj->id      				= $_REQUEST['id'];
		$obj->nome    				= ($_REQUEST['nome']!=''?$_REQUEST['nome']:'');
		$obj->nomecompleto    	    = ($_REQUEST['nomecompleto']!=''?$_REQUEST['nomecompleto']:'');
		//$obj->mae    				= ($_REQUEST['mae']!=''?$_REQUEST['mae']:'');
		$obj->telefone				= ($_REQUEST['telefone']!=''?$_REQUEST['telefone']:'');
		$obj->complemento	        = ($_REQUEST['complemento']!=''?$_REQUEST['complemento']:'');
		$obj->endereco				= ($_REQUEST['endereco']!=''?$_REQUEST['endereco']:'');
	    $obj->cep 					= ($_REQUEST['cep']!=''?$_REQUEST['cep']:'');
        $obj->cidade 				= ($_REQUEST['cidade']!=''?$_REQUEST['cidade']:'');
        $obj->estado 				= ($_REQUEST['estado']!=''?$_REQUEST['estado']:'');
        $obj->bairro 				= ($_REQUEST['bairro']!=''?$_REQUEST['bairro']:'');
        $obj->numero 				= ($_REQUEST['numero']!=''?$_REQUEST['numero']:'');

		$obj->email					= ($_REQUEST['email']!=''?$_REQUEST['email']:'');
		$obj->cpf	    			= ($_REQUEST['cpf']!=''?$_REQUEST['cpf']:'');
		$obj->sexo  		        = ($_REQUEST['sexo']!=''?$_REQUEST['sexo']:'');
		$arrDataNasc                = explode("/", $_REQUEST['dataNascimento']);
		$dataNasc                   = $arrDataNasc[2]."-".$arrDataNasc[1]."-".$arrDataNasc[0];
		$obj->created_at            = $dataNasc;
		$obj->usuario               = $_REQUEST['usuario']; 
		$obj->horainicio  		    = ($_REQUEST['horainicio']!=''?$_REQUEST['horainicio']:'');
		$obj->horafim  		        = ($_REQUEST['horafim']!=''?$_REQUEST['horafim']:'');
		$obj->modulointeracoes  	= ($_REQUEST['modulointeracoes']!=''?$_REQUEST['modulointeracoes']:'');
		$obj->id_cliente    		= $this->session->userdata('loggedincliente');
		$obj->horainicioseg         = isset($_REQUEST['horainicioseg']) ? $_REQUEST['horainicioseg']:'';
        $obj->horainicioter         = isset($_REQUEST['horainicioter']) ? $_REQUEST['horainicioter']:'';
        $obj->horainicioqua         = isset($_REQUEST['horainicioqua']) ? $_REQUEST['horainicioqua']:'';
        $obj->horainicioqui         = isset($_REQUEST['horainicioqui']) ? $_REQUEST['horainicioqui']:'';
        $obj->horainiciosex         = isset($_REQUEST['horainiciosex']) ? $_REQUEST['horainiciosex']:'';
        $obj->horainiciosab         = isset($_REQUEST['horainiciosab']) ? $_REQUEST['horainiciosab']:'';
        $obj->horainiciodom         = isset($_REQUEST['horainiciodom']) ? $_REQUEST['horainiciodom']:'';
        $obj->horafimseg            = isset($_REQUEST['horafimseg']) ? $_REQUEST['horafimseg']:'';
        $obj->horafimter            = isset($_REQUEST['horafimter']) ? $_REQUEST['horafimter']:'';
        $obj->horafimqua            = isset($_REQUEST['horafimqua']) ? $_REQUEST['horafimqua']:'';
        $obj->horafimqui            = isset($_REQUEST['horafimqui']) ? $_REQUEST['horafimqui']:'';
        $obj->horafimsex            = isset($_REQUEST['horafimsex']) ? $_REQUEST['horafimsex']:'';
        $obj->horafimsab            = isset($_REQUEST['horafimsab']) ? $_REQUEST['horafimsab']:'';
        $obj->horafimdom            = isset($_REQUEST['horafimdom']) ? $_REQUEST['horafimdom']:'';    

		if($_REQUEST['dias']){
			foreach ($_REQUEST['dias'] as $key => $value) {
				$dias .= $value.',';
			}
			$dias = substr($dias, 0,-1);
			$obj->dias = $dias;
		}

		if($_REQUEST['id_departamento']){
			foreach ($_REQUEST['id_departamento'] as $key => $value) {
				$id_departamento .= $value.',';
			}
			$id_departamento = substr($id_departamento, 0,-1);
			$obj->id_departamento = $id_departamento;
		}

		
		if(is_numeric($obj->id)){
			if($_POST['senha']){
				$senha = isset($_POST['senha'])?$_POST['senha']:null;
				$obj->senha = sha1($senha);
			}

			$update = $this->operadoresmodel->update($obj);
			
			if($update){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Operador alterado com sucesso!</p>
	            </div>');
			}
		}else{
			$senha = isset($_POST['senha'])?$_POST['senha']:null;
			$obj->senha = sha1($senha);
			$insert = $this->operadoresmodel->insert($obj);
			
			if($insert){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Operador cadastrado com sucesso!</p>
	            </div>');
				
			}	
		}
		
		redirect($this->router->class);
		
					
	}
	
	function apagar($id)
	{
		$del = $this->operadoresmodel->delById($id);
		if($del){
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Operador deletado com sucesso!</p>
	            </div>');
		}else{
			set_alert('<div class="alert alert-danger alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados Operador apresentou erro ao deletar!</p>
	            </div>');
		}	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	
	function page($page = 0)
	{
		
		$data['list'] = $this->operadoresmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		foreach ($data['list'] as $key => $value) {
			$deps = explode(',', $value->id_departamento);
			
			foreach ($deps as $key1 => $value1) {
				$deps1 = $this->departamentosmodel->getById($value1);	
				$departamentos .= $deps1->titulo.',';
			}
			$value->departamentos = substr($departamentos, 0,-1);
			
		}
		
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
	function search($page = 0)
	{
		$titulo = $_GET['nome'];
		
		$cont = $this->operadoresmodel->totalPaginasSearch($titulo);
		$config['base_url'] = base_url().$this->session->userdata('folder').$this->router->class.'/page/';
        $config['total_rows'] = $cont;
	    		
		$this->pagination->initialize($config);
   
	    $data['pag'] = $this->pagination->create_links();
	    $data['list'] = $this->operadoresmodel->getListPageSearch($page,$this->pagination->per_page,$titulo);
    	
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
	}
	
}