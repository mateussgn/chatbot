<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Agendacontatos extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('agendacontatosmodel');
		$this->load->model('bairrosmodel');
		head_title('Agenda de Contatos');

	}

	function index()
	{
		$this->page();
	}
	

    function ajax_cep($cep){
    	$cep = $cep;
    	$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
		 
		$dados['sucesso'] = (string) $reg->resultado;
		$dados['rua']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
		$dados['bairro']  = (string) $reg->bairro;
		$dados['cidade']  = (string) $reg->cidade;
		$dados['estado']  = (string) $reg->uf;
		 
		echo json_encode($dados);

    } 

	function adicionar()
	{
		head_title('Adicionar Contatos');

		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}
	
	function editar($id)
	{
		head_title('Modificar Contatos');
		
		$data['obj'] = $this->agendacontatosmodel->getById($id);
		
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form',$data);
	}
	
	function insert()
	{
		$obj = new stdClass();
		$obj->id      				= $_REQUEST['id'];
		$obj->id_contato            = ($_REQUEST['id_contato']!=''?$_REQUEST['id_contato']:'');
		$obj->nome    				= ($_REQUEST['nome']!=''?$_REQUEST['nome']:'');
		$obj->nomeCompleto    	    = ($_REQUEST['nomeCompleto']!=''?$_REQUEST['nomeCompleto']:'');
		//$obj->number				= $_REQUEST['number'];
		$obj->complemento	        = ($_REQUEST['complemento']!=''?$_REQUEST['complemento']:'');
		$obj->pontoDeReferencia     = ($_REQUEST['pontoDeReferencia']!=''?$_REQUEST['pontoDeReferencia']:'');
		$obj->rua   				= ($_REQUEST['rua']!=''?$_REQUEST['rua']:'');
	    $obj->cep 					= ($_REQUEST['cep']!=''?$_REQUEST['cep']:'');
        $obj->cidade 				= ($_REQUEST['cidade']!=''?$_REQUEST['cidade']:'');
        $obj->estado 				= ($_REQUEST['estado']!=''?$_REQUEST['estado']:'');
        $obj->bairro 				= ($_REQUEST['bairro']!=''?$_REQUEST['bairro']:'');
        $obj->numero 				= ($_REQUEST['numero']!=''?$_REQUEST['numero']:'');
		$obj->email					= ($_REQUEST['email']!=''?$_REQUEST['email']:'');
		$obj->facebook		        = ($_REQUEST['facebook']!=''?$_REQUEST['facebook']:'');
		$obj->instagram				= ($_REQUEST['instagram']!=''?$_REQUEST['instagram']:'');
		$obj->cpf	    			= ($_REQUEST['cpf']!=''?$_REQUEST['cpf']:'');
		$obj->id_cliente    		= $this->session->userdata('loggedincliente');
		$obj->observacoes           = ($_REQUEST['observacoes']!=''?$_REQUEST['observacoes']:'');

		if (is_numeric($obj->id)) {
            
            $update = $this->agendacontatosmodel->update($obj);
            
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Contato alterado com sucesso!</p>
                </div>');
            
        } else {
            $insert = $this->agendacontatosmodel->insert($obj);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Contato cadastrado com sucesso!</p>
                </div>');
                
            }
        }

		redirect($this->router->class);
		
					
	}
	
	function apagar($id)
	{
		$del = $this->agendacontatosmodel->delById($id);
		if($del){
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Contato deletado com sucesso!</p>
	            </div>');
		}else{
			set_alert('<div class="alert alert-danger alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Dados do Contato apresentou erro ao deletar!</p>
	            </div>');
		}	
		redirect($this->session->userdata('folder').$this->router->class);
	}

	
	function page($page = 0)
	{
		
		$data['list'] = $this->agendacontatosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));

		foreach ($data['list'] as $key => $value) {
			if($value->bairro<>0){
                $op = $this->bairrosmodel->getById($value->bairro);
                $value->bairro = $op->nome;
            }
		}

		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);		
		
	}
	
}