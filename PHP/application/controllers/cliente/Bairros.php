<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
id_cliente INT
chave VARCHAR
interacao VARCHAR
*/

class Bairros extends MY_Controller_Cliente {
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');
        if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
        
        $this->load->model('bairrosmodel');
        head_title('Bairros');

    }

    function index()
    {
        $this->page();
    }

    function adicionar() {
        head_title('Adicionar Bairro');
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $modulos);
    }

    function editar($id) {
        head_title('Editar Bairro');
        $data['obj']  = $this->bairrosmodel->getById($id);
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '_form', $data);
    }

    function page($page = 0) {
        $data['list'] = $this->bairrosmodel->getListWhere(array(
            'id_cliente' => $this->session->userdata('loggedincliente')
        ));
        
        $this->load->view($this->session->userdata('folder') . $this->router->class . '/' . $this->router->class . '', $data);
    }

    function insert() {
        
        $obj                 = new stdClass();
        $obj->id             = $_REQUEST['id'];
        $obj->nome           = isset($_REQUEST['nome']) ? $_REQUEST['nome'] : '';        
        $obj->preco_credito  = isset($_REQUEST['preco_credito']) ? $_REQUEST['preco_credito'] : '';
        $obj->preco_debito   = isset($_REQUEST['preco_debito']) ? $_REQUEST['preco_debito'] : '';
        $obj->preco_dinheiro = isset($_REQUEST['preco_dinheiro']) ? $_REQUEST['preco_dinheiro'] : '';
        $obj->preco_vale     = isset($_REQUEST['preco_vale']) ? $_REQUEST['preco_vale'] : '';

        if (is_numeric($obj->id)) {
            
            $update = $this->bairrosmodel->update($obj);
            
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Bairro alterado com sucesso!</p>
                </div>');
            
        } else {
            $insert = $this->bairrosmodel->insert($obj);
            
            if ($insert) {
                set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Bairro cadastrado com sucesso!</p>
                </div>');
                
            }
        }
        
        redirect($this->router->class);
    }

    function apagar($id) {
        $del = $this->bairrosmodel->delById($id);
        if ($del) {
            set_alert('<div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Bairro deletado com sucesso!</p>
                </div>');
        } else {
            set_alert('<div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><strong>Sucesso</strong></h4>
                    <p>Bairro apresentou erro ao ser deletado!</p>
                </div>');
        }
        
        redirect($this->session->userdata('folder') . $this->router->class);
    }
}