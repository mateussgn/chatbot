<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Notificadormensagem extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('notificadormensagemmodel');
		$this->load->model('tagsmodel');
		$this->load->model('departamentosmodel');
		head_title('Respostas');

	}
	
	
	function index()
	{
		$this->page();
	}
	
	function adicionar()
	{
		head_title('Adicionar Notificador Mensagem');
		$data['tags'] = $this->tagsmodel->getList();
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}

	function editar($id)
	{
		head_title('Adicionar Notificador Mensagem');
		$data['tags'] = $this->tagsmodel->getList();
		$data['obj'] = $this->notificadormensagemmodel->getById($id);
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}
	
	
	function insert()
	{	

		$obj = new stdClass();
		$obj->id = $_REQUEST['id'];
		$obj->titulo = isset($_REQUEST['titulo'])?$_REQUEST['titulo']:'';
		$obj->texto = isset($_REQUEST['texto'])?$_REQUEST['texto']:'';
		//$obj->id_cliente    				= $this->session->userdata('loggedincliente');
		
		if($_FILES['foto']['name'] != ""){
			$file = $_FILES['foto'];
			$config['upload_path']   = './uploads/sends/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|csv|rar|zip|pdf';
			$config['max_size']      = '300000';
			$config['max_width']     = '2000';
			$config['max_height']    = '2000';
			$config['file_name']     = date('ymdHis').rand();
			
			
			$this->load->library('upload', $config);
			
			// Alternativamente voc? pode configurar as prefer?ncias chamando a fun??o initialize. ? ?til se voc? auto-carregar a classe:
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload('foto'))
			{
				$error = array('error' => $this->upload->display_errors());
				//print_r($error);
				//$this->load->view('upload_form', $error);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				//print_r($data);
				//$this->load->view('upload_success', $data);
				$fotolink =  $config['file_name'].$data['upload_data']['file_ext'];

				$img = site_url().'uploads/sends/'.$fotolink; 
	            $type = image_type_to_mime_type(exif_imagetype($img));
	            $data = file_get_contents($img);
	            $base64 = 'data:' . $type . ';base64,' . base64_encode($data);

	            $obj->foto	        		= $base64;	
			}
		}

		
		
		if(is_numeric($obj->id)){
			
			$update = $this->notificadormensagemmodel->update($obj);
			
			set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Notificador Mensagem alterado com sucesso!</p>
	            </div>');

		}else{
			$insert = $this->notificadormensagemmodel->insert($obj);
			
			if($insert){
				set_alert('<div class="alert alert-success alert-dismissable">
	                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	                <h4><strong>Sucesso</strong></h4>
	                <p>Notificador Mensagem cadastrado com sucesso!</p>
	            </div>');
				
			}	
		}

		redirect($this->router->class);
		
					
	}
	
	
	function page($page = 0)
	{
		$data['list'] = $this->notificadormensagemmodel->getList();

		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'',$data);
		
		
		
	}
	
}