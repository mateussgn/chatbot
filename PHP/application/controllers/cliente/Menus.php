<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Menus extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('menusmodel');
		$this->load->model('tagsmodel');
		$this->load->model('departamentosmodel');
		head_title('Respostas');

	}
	
	function menus()
	{
		//$arrayName = array('6' => 'Apresentação - Boas vindas','1' => 'Resposta para início do atendimento', '4' => 'Resposta para fim do atendimento:','2' => 'Resposta para atendimento fora do horário de funcionamento do departamento:','3' => 'Resposta de apresentação do atendente:', '7' => 'Transferência pra outro departamento/atendente', '5' => 'Resposta para atendimento em espera:' );
		$arrayName = array('1' => 'Apresentação - Boas vindas','2' => 'Resposta para início do atendimento', '5' => 'Resposta para fim do atendimento:','7' => 'Transferência pra outro departamento/atendente');
		return $arrayName;
	}


	function index()
	{
		$this->page();
	}
	
	function adicionar()
	{
		head_title('Adicionar Respostas');
		$data['cliente'] = $this->clientesmodel->getById($this->session->userdata('loggedincliente'));			
		$data['departamentos'] = $this->departamentosmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));
		$data['tags'] = $this->tagsmodel->getList();
		$data['menus'] = $this->menus();
		$data['menuslist'] = $this->menusmodel->getListWhere(array('id_cliente'=>$this->session->userdata('loggedincliente')));

		//echo '<pre>';print_r($data['menuslist']);
		//die();

		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class.'_form', $data);
	}
	
	
	function insert()
	{	

		foreach ($_POST['id_menu'] as $key => $value) {
			$menu = $this->menusmodel->getListWhere(array('id_cliente' => $this->session->userdata('loggedincliente'), 'id_menu' => $value));
			if(isset($menu[0])){
				$obj = new stdClass();
				$obj->id      				= $menu[0]->id;
				$obj->id_menu    			= $value;
				$obj->resposta				= $_REQUEST['resposta'][$key];
				$obj->id_cliente    		= $this->session->userdata('loggedincliente');
				$insert = $this->menusmodel->update($obj);	
			}else{
				$obj = new stdClass();
				$obj->id      				= '';
				$obj->id_menu    			= $value;
				$obj->resposta				= $_REQUEST['resposta'][$key];
				$obj->id_cliente    		= $this->session->userdata('loggedincliente');
				$insert = $this->menusmodel->insert($obj);
			}
			
		}

		if($insert){
			set_alert('<div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><strong>Sucesso</strong></h4>
                <p>Dados salvos com sucesso!</p>
            </div>');
		}
		redirect($this->router->class);
		
					
	}
	
	
	function page($page = 0)
	{
		
		$this->adicionar();
		
		
	}
	
}