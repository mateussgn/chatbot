<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Relatorios extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('departamentosmodel');
		$this->load->model('messagesmodel');
		$this->load->model('chatlistmodel');
		$this->load->model('relatoriomodel');
		$this->load->model('operadoresmodel');
		$this->load->model('entregadoresmodel');
		head_title('Relatórios');

	}

	function index()
	{
		//$this->page();
	}
	
	
	function geral($page = 0)
	{
		head_title('Relatórios - Geral');

		$titulo = $_POST['texto'];
		
		$datainicio = $this->dateformat($_POST['datainicio'],'EUA');
		$datafim = $this->dateformat($_POST['datafim'],'EUA');

		if($titulo<>''){
			$data['messages'] = $this->messagesmodel->getListWhereSearch(array('id_cliente'=>$this->session->userdata('loggedincliente')), array('texto'=>$titulo),$datainicio,$datafim);
			
			$data['contatos'] = $this->messagesmodel->getListWhereSearch(array('id_cliente'=>$this->session->userdata('loggedincliente')), array('texto'=>$titulo),$datainicio,$datafim);
			$receives = array();
			foreach ($data['contatos'] as $key => $value) {
				array_push($receives,$value->receive);
			}

			$data['receives'] = $receives;
			
			$data['meses'] = $this->messagesmodel->getListWhereSearchMesaMes($this->session->userdata('loggedincliente'),$titulo,$datainicio,$datafim);


		}

		$data['texto'] = $_POST['texto'];
		
		$data['datainicio'] = $_POST['datainicio'];
		$data['datafim'] = $_POST['datafim'];

		$this->load->view($this->session->userdata('folder').$this->router->class.'/geral',$data);
		
		
	}

	function detalhado($page = 0)
	{

		head_title('Relatórios - Detalhado');

		//Recebendo os valores da pesquisa via post
		$protocolo = $_POST['protocolo'];
		$numero = $_POST['numero'];
		$status = $_POST['status'];
		$id_operador = $_POST['id_operador'];
		$id_departamento = $_POST['id_departamento'];
		
		//troca os -- por ''(vazio)
		$datainicio = ($this->dateformat($_POST['datainicio'],'EUA')) == '--'?'':$this->dateformat($_POST['datainicio'],'EUA');
		$datafim = ($this->dateformat($_POST['datafim'],'EUA')) =='--'?'':$this->dateformat($_POST['datafim'],'EUA');
		
		//Carregando os combobox
		$data['listDep'] = $this->departamentosmodel->getListAll();
		$data['listOper'] = $this->operadoresmodel->getListAll();

		if($datainicio<>'' and $datafim<>''){
			$arrayData = array('datahora >=' => $datainicio.' 00:00:01', 'datahora <=' => $datafim.' 23:59:59');
		
			$ArrayCamposCombobox = array('fila.protocolo' => $protocolo, 'fila.receive'=> $numero,  'fila.status' => $status, 'fila.id_atendente' => $id_operador, 'fila.id_departamento' =>$id_departamento);


			// gera um array apenas com os campos que foram preenchidos na pesquisa
			foreach ($ArrayCamposCombobox as $key => $value) {
				if($value <>''){
					$arrayCamposValidos[$key] = $value; 
				}
			}
			
			//Retorna a lista detalhada
			$data['list'] = $this->relatoriomodel->getListWhereDetalhada($arrayData,$arrayCamposValidos);
			
			$data['protocolo'] = $_POST['protocolo'];
			$data['numero'] = $_POST['numero'];
			$data['status'] = $_POST['status'];
			$data['id_operador'] = $_POST['id_operador'];
			$data['id_departamento'] = $_POST['id_departamento'];	
			$data['datainicio'] = $_POST['datainicio'];
			$data['datafim'] = $_POST['datafim'];
						
		}else{
			$arrayData = array();
		
			$ArrayCamposCombobox = array('fila.protocolo' => $protocolo, 'fila.receive'=> $numero,  'fila.status' => $status, 'fila.id_atendente' => $id_operador, 'fila.id_departamento' =>$id_departamento);


			// gera um array apenas com os campos que foram preenchidos na pesquisa
			foreach ($ArrayCamposCombobox as $key => $value) {
				if($value <>''){
					$arrayCamposValidos[$key] = $value; 
				}
			}

			//Retorna a lista total
			$data['list'] = $this->relatoriomodel->getListWhereDetalhada($arrayData,$arrayCamposValidos);
		}

		//Atribuí o nome para cada status
		foreach ($data['list'] as $key => $value) {

				
			switch($value->status){
				case 0:
					$value->status = 'Aguardando';
				break;
				case 1:
					$value->status = 'Atendendo';
				break;
				case 2:
					$value->status = 'Concluída';
				break;
				case 3:
					$value->status = 'Abandono';
				break;
	
			}
			if($value->id_atendente<>0){
				$op = $this->operadoresmodel->getById($value->id_atendente);
				$value->operador = $op->nome;
			}
			if($value->id_entregador<>0){
				$op = $this->entregadoresmodel->getById($value->id_entregador);
				$value->id_entregador = $op->nome;
			}
			
			//Realiza o cálculo de duração do atentimento
			$dataAtendimento = new DateTime($value->dataatendimento);
			$dataTermino = new DateTime($value->datatermino);
			$duracao = $dataTermino->diff($dataAtendimento);
			$value->duracao = $duracao->format('%H:%I:%S');
		}
		
		
		$this->load->view($this->session->userdata('folder').$this->router->class.'/detalhado',$data);
		
		
	}


	function gerarRelatorioDetalhado(){

		//Recebendo os valores da pesquisa via post
		$protocolo = $_POST['protocolo'];
		$numero = $_POST['numero'];
		$status = $_POST['status'];
		$id_operador = $_POST['id_operador'];
		$id_departamento = $_POST['id_departamento'];
		
		//troca os -- por ''(vazio)
		$datainicio = ($this->dateformat($_POST['datainicio'],'EUA')) == '--'?'':$this->dateformat($_POST['datainicio'],'EUA');
		$datafim = ($this->dateformat($_POST['datafim'],'EUA')) =='--'?'':$this->dateformat($_POST['datafim'],'EUA');
		
		//Carregando os combobox
		$data['listDep'] = $this->departamentosmodel->getListAll();
		$data['listOper'] = $this->operadoresmodel->getListAll();

		
		if($datainicio<>'' and $datafim<>''){
			
			$arrayData = array('datahora >=' => $datainicio.' 00:00:01', 'datahora <=' => $datafim.' 23:59:59');
			$ArrayCamposCombobox = array('fila.protocolo' => $protocolo, 'fila.receive'=> $numero,  'fila.status' => $status, 'fila.id_atendente' => $id_operador, 'fila.id_departamento' =>$id_departamento);


			// gera um array apenas com os campos que foram preenchidos na pesquisa
			foreach ($ArrayCamposCombobox as $key => $value) {
				if($value <>''){
					$arrayCamposValidos[$key] = $value; 
				}
			}

			//Retorna a lista detalhada
			$data['list'] = $this->relatoriomodel->getListWhereDetalhada($arrayData,$arrayCamposValidos);
			
			$data['protocolo'] = $_POST['protocolo'];
			$data['numero'] = $_POST['numero'];
			$data['status'] = $_POST['status'];
			$data['id_operador'] = $_POST['id_operador'];
			$data['id_departamento'] = $_POST['id_departamento'];	
			$data['datainicio'] = $_POST['datainicio'];
			$data['datafim'] = $_POST['datafim'];
						
		}else{
			//Retorna a lista total
			$data['list'] = $this->relatoriomodel->getListDetalhada();
		}

		//Atribuí o nome para cada status
		foreach ($data['list'] as $key => $value) {

				
			switch($value->status){
				case 0:
					$value->status = 'Aguardando';
				break;
				case 1:
					$value->status = 'Atendendo';
				break;
				case 2:
					$value->status = 'Concluída';
				break;
				case 3:
					$value->status = 'Abandono';
				break;
	
			}
			if($value->id_atendente<>0){
				$op = $this->operadoresmodel->getById($value->id_atendente);
				$value->operador = $op->nome;
			}
			if($value->id_entregador<>0){
				$op = $this->entregadoresmodel->getById($value->id_entregador);
				$value->id_entregador = $op->nome;
			}

			//Realiza o cálculo de duração do atentimento
			$dataAtendimento = new DateTime($value->dataatendimento);
			$dataTermino = new DateTime($value->datatermino);
			$duracao = $dataTermino->diff($dataAtendimento);
			$value->duracao = $duracao->format('%H:%I:%S');
		}

	

		
		$filename = "Relatório" . date('Ymd') . ".xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
  		header("Content-Type: application/vnd.ms-excel");

		echo '<table border="1" cellspadding="5" cellspacing="0" width="100%">';
		echo '<tr><th colspan="8">Relatório</th></tr>';
		//echo '<tr><th>Protoloco: '.($protocolo).' / Numero: '.($numero).' / Data Inicio: '.$_POST['datainicio'].' / Data Fim: '.$_POST['datafim'].'</th></tr>';
		echo '<tr>
			<th>Protoloco</th>
			<th>Nome</th>
			<th>Numero</th>
			<th>Status</th>
			<th>Departamento</th>
			<th>Operador</th>
			<th>Entregador</th>
			<th>Data Inicio</th>
			<th>Data Fim</th>
			<th>'.utf8_decode('Duração').' </th>
			</tr>';
		foreach ($data['list'] as $key => $value) {
			$contato = str_replace('@c.us', '', $value->receive);
			//echo '<tr><th>'.$value->protocolo.' / '.($contato).' / '.$value->status.' / '.$value->titulo.' / '.$value->operador.' / '.$value->dataatendimento.' / '.$value->datatermino.' / '.$value->duracao.'</th></tr>';
			echo '<tr>
			<td>'.$value->protocolo.'</td>
			<td>'.$value->nome.'</td>
			<td>'.$contato.'</td>
			<td>'.utf8_decode($value->status).'</td>
			<td>'.$value->titulo.'</td>
			<td>'.$value->operador.'</td>
			<td>'.$value->id_entregador.'</td>
			<td>'.$value->dataatendimento.'</td>
			<td>'.$value->datatermino.'</td>
			<td>'.$value->duracao.' </td>
			</tr>';
		}
		echo '</table>';
	}


	function gerarcontatos($page = 0)
	{
		head_title('Relatórios - Geral');

		$titulo = $_POST['texto'];
		
		$datainicio = $this->dateformat($_POST['datainicio'],'EUA');
		$datafim = $this->dateformat($_POST['datafim'],'EUA');

		if($titulo<>''){
			$data['messages'] = $this->messagesmodel->getListWhereSearch(array('id_cliente'=>$this->session->userdata('loggedincliente')), array('texto'=>$titulo),$datainicio,$datafim);
			
			$data['contatos'] = $this->messagesmodel->getListWhereSearch(array('id_cliente'=>$this->session->userdata('loggedincliente')), array('texto'=>$titulo),$datainicio,$datafim);
			$receives = array();
			foreach ($data['contatos'] as $key => $value) {
				array_push($receives,$value->receive);
			}

			$data['receives'] = array_unique($receives);
			
			$data['meses'] = $this->messagesmodel->getListWhereSearchMesaMes($this->session->userdata('loggedincliente'),$titulo,$datainicio,$datafim);


		}

		$filename = "contatos" . date('Ymd') . ".xls";
		header("Content-Disposition: attachment; filename=\"$filename\"");
  		header("Content-Type: application/vnd.ms-excel");

		echo '<table border="1" cellspadding="5" cellspacing="0" width="100%">';
		echo '<tr><th>Contatos</th></tr>';
		echo '<tr><th>Pesquisa: '.($titulo).' / Data Inicio: '.$_POST['datainicio'].' / Data Fim: '.$_POST['datafim'].'</th></tr>';
		foreach ($data['receives'] as $key => $value) {
			$contato = str_replace('@s.whatsapp.net', '', $value);
			echo '<tr><th>'.($contato).'</th></tr>';
		}
		echo '</table>';
		
		
		
	}
	
	function dateformat($data,$tipo)
	{
		if($tipo == 'BRA'){
			$data = explode('-', $data);
			return $data[2].'/'.$data[1].'/'.$data[0];
		}

		if($tipo == 'EUA'){
			$data = explode('/', $data);
			return $data[2].'-'.$data[1].'-'.$data[0];
		}
	}
}