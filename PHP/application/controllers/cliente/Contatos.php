<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Fields in DATABASE

id INT
nome VARCHAR
email VARCHAR
login VARCHAR
senha VARCHAR
*/

class Contatos extends MY_Controller_Cliente {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('session');
		if (!$this->session->userdata('loggedincliente'))
            redirect($this->session->userdata('folder').'login');
		
		$this->load->model('chatlistmodel');
		$this->load->model('messagesmodel');
		$this->load->model('agendacontatosmodel');
		head_title('Leads de Contatos');

	}

	function index()
	{
		$this->page();
	}
	
	
	function page($page = 0)
	{
		$datainicio = $this->dateformat($_POST['datainicio'],'EUA');
		$datafim = $this->dateformat($_POST['datafim'],'EUA');

		$cliente = $this->session->userdata('loggedincliente');
		
		if($_POST['datainicio']){
			$data['messages'] = $this->messagesmodel->getListWhereSearchGroupby(array('id_cliente'=>$this->session->userdata('loggedincliente')),$datainicio,$datafim);
			foreach ($data['messages'] as $key => $value) {
				$primeira = $this->messagesmodel->getListFirstLastMessage(array('id_cliente'=>$cliente,'receive'=>$value->receive),'ASC');
				$ultima = $this->messagesmodel->getListFirstLastMessage(array('id_cliente'=>$cliente,'receive'=>$value->receive),'DESC');

				$value->primeira = $this->datetime($primeira->timestamp);
				$value->ultima = $this->datetime($ultima->timestamp);
				$contato = $this->agendacontatosmodel->getUltimoContato(str_replace('@c.us', '', $value->receive));
				$value->nome = $contato->nome;
			}

			//print_r($data['messages']);
			//die();
			
			
		}

    	$data['datainicio'] = $_POST['datainicio'];
		$data['datafim'] = $_POST['datafim'];
    	//echo '<pre>';print_r($data['list']);
    	//die();
		$this->load->view($this->session->userdata('folder').$this->router->class.'/'.$this->router->class,$data);
		
		
	}

	function gerarpdf()
	{
		$datainicio = $this->dateformat($_POST['datainicio'],'EUA');
		$datafim = $this->dateformat($_POST['datafim'],'EUA');

		$cliente = $this->session->userdata('loggedincliente');
		
		if($datainicio){
			$messages = $this->messagesmodel->getListWhereSearchGroupby(array('id_cliente'=>$this->session->userdata('loggedincliente')),$datainicio,$datafim);
			foreach ($messages as $key => $value) {
				$primeira = $this->messagesmodel->getListFirstLastMessage(array('id_cliente'=>$cliente,'receive'=>$value->receive),'ASC');
				$ultima = $this->messagesmodel->getListFirstLastMessage(array('id_cliente'=>$cliente,'receive'=>$value->receive),'DESC');

				$value->primeira = $this->datetime($primeira->timestamp);
				$value->ultima = $this->datetime($ultima->timestamp);
				$contato = $this->agendacontatosmodel->getUltimoContato(str_replace('@c.us', '', $value->receive));
				$value->nome = $contato->nome;
			}

			//print_r($data['messages']);
			//die();			
		}
		
		
    	// Load library
		$this->load->library('dompdf_gen');

		$html = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Relatorio</title>
		<style>
	    body {
	        font-size:12px;
	        font-family:Calibri;
	    }
	    div,p{text-align:justify;position:relative;z-index:99999;}
	    @page { margin: 50px; }
	    table, th, td {
		    border: 1px solid black;
		    border-collapse: collapse;
		}
	    </style>
		</head>
		
		<body>
		<div style="text-align:center;font-size:18px;">
        <b>'.utf8_decode('Relatório de Contatos').'</b>
        </div>
        <br><br>
        <br>
        <table cellpadding="2" cellspacing="0" width="100%" border="1">
		<tr>
                        <th>Numero</th>
                        <th>Nome</th>
                        <th>Primeira '.utf8_decode('Interação').'</th>
                        <th>Ultima '.utf8_decode('Interação').'</th>
                        
                    </tr>
		
		';

		foreach ($messages as $key => $l) {

			$html .= '
			<tr>
				<td class="text-center">'.str_replace('@c.us', '', $l->receive).'</td>
				<td class="text-center">'.$l->nome.'</td>
                <td class="text-center">'.$l->primeira.'</td>
                <td class="text-center">'.$l->ultima.'</td>
                </tr>
			';
		}
		
		$html .='
		
		
		</table>

		</body>
		</html>
       	';

        // Convert to PDF
        //$html = utf8_decode($html);
		$this->dompdf->load_html($html, 'ISO-8859-1');
		$this->dompdf->render();
		$this->dompdf->output();
		$this->dompdf->stream("relatoriocontatos.pdf", array("Attachment" => 0));  

		
		
	}

	function dateformat($data,$tipo)
	{
		if($tipo == 'BRA'){
			$data = explode('-', $data);
			return $data[2].'/'.$data[1].'/'.$data[0];
		}

		if($tipo == 'EUA'){
			$data = explode('/', $data);
			return $data[2].'-'.$data[1].'-'.$data[0];
		}
	}
	
	function datetime($datetime)
	{
		$datetime = explode(' ', $datetime);
		$hora = $datetime[1];
		$data = explode('-', $datetime[0]);
		$data = $data[2].'/'.$data[1].'/'.$data[0].' '.$hora;
		return $data;
	}
	
}