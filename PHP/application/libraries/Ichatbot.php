<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dialogflow {

	function usa_api($sender, $msg, $dialogflowclient) {
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		require_once(BASEPATH.'../application/libraries/nbot/vendor/autoload.php');

		use DialogFlow\Client;
		use DialogFlow\Model\Query;
		use DialogFlow\Method\QueryApi;


		
		try {

	    $client = new Client($dialogflowclient);
	    $queryApi = new QueryApi($client);

	    $meaning = $queryApi->extractMeaning("$msg", [
	        'sessionId' => "$sender",
	        'lang' => 'pt-br',
	    ]);
	    $response = new Query($meaning);
		} catch (\Exception $error) {
		    echo $error->getMessage();
		}

		$response = json_encode($response);
		$r = json_decode($response);

		//print_r($r);


		$r = $r->result;
		$action = $r->action;
		$resolvedQuery = $r->resolvedQuery;
		$parameters = $r->parameters;
		print_r($r);
		die();
		$textResposta = $r->fulfillment->messages[0]->textToSpeech;



		//$textResposta = preg_replace('/\r?\n|\r/','<br/>', $textResposta);
		//$textResposta =  strip_tags(str_replace('<br/>', '\r\n', nl2br($textResposta)));
		$textResposta =  nl2br($textResposta,false);
		


		

		return $textResposta;
	}
}
