<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 41106: PAC sem contrato
 * 40010: SEDEX sem contrato
 * 40045: SEDEX a Cobrar, sem contrato
 * 40126: SEDEX a Cobrar, com contrato
 * 40215: SEDEX 10, sem contrato
 * 40290: SEDEX Hoje, sem contrato
 * 40096: SEDEX com contrato
 * 40436: SEDEX com contrato
 * 40444: SEDEX com contrato
 * 81019: e-SEDEX, com contrato
 * 41068: PAC com contrato
 * 40568: SEDEX com contrato
 * 40606: SEDEX com contrato
 * 81868: (Grupo 1) e-SEDEX, com contrato
 * 81833: (Grupo 2) e-SEDEX, com contrato
 * 81850: (Grupo 3) e-SEDEX, com contrato
 */

class Correios {
    
    public $ci;
    protected $nome = array(
        '41106' => 'PAC',
        '40010' => 'SEDEX',
        '40045' => 'SEDEX a Cobrar',
        '40126' => 'SEDEX a Cobrar',
        '40215' => 'SEDEX 10',
        '40290' => 'SEDEX Hoje',
        '40096' => 'SEDEX',
        '40436' => 'SEDEX',
        '40444' => 'SEDEX',
        '81019' => 'e-SEDEX',
        '41068' => 'PAC',
        '40568' => 'SEDEX',
        '40606' => 'SEDEX',
        '81868' => 'e-SEDEX',
        '81833' => 'e-SEDEX',
        '81850' => 'e-SEDEX',
		'1' => 'RETIRAR NO LOCAL'
    );

    public function __construct()
    {
        $this->ci =& get_instance();
        $this->ci->config->load('uniauto');
    }

    function consulta($cep,$qtd)
    {
        $servicos = $this->ci->config->item('envio');

        foreach($servicos as $tipo => $servico)
        {
			
            $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=58037000&sCepDestino=$cep&nVlPeso=".$qtd."&nCdFormato=1&nVlComprimento=20&nVlAltura=4&nVlLargura=20&sCdMaoPropria=n&nVlValorDeclarado=100&sCdAvisoRecebimento=n&nCdServico=$tipo&nVlDiametro=0&StrRetorno=xml";
			/*
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$xml = curl_exec ($c);
			curl_close ($c); */
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);    // get the url contents
			
			$data = curl_exec($ch); // execute curl request
			curl_close($ch);
			
			$xml = simplexml_load_string($data);
			
            //$xml = simplexml_load_file($url);
			
			if ($xml->cServico->Erro == 0)
            {
                $retorno[(string)$xml->cServico->Codigo]['nome'] = $this->nome[$tipo];
                $retorno[(string)$xml->cServico->Codigo]['tipo'] = (string)$tipo;
                $retorno[(string)$xml->cServico->Codigo]['valor'] = (float)str_replace(",",".",$xml->cServico->Valor);
                $retorno[(string)$xml->cServico->Codigo]['prazo_entrega'] = (string)$xml->cServico->PrazoEntrega;
            }else{
				$retorno[(string)$xml->cServico->Codigo]['nome'] = $this->nome[$tipo];
                $retorno[(string)$xml->cServico->Codigo]['tipo'] = (string)$tipo;
                $retorno[(string)$xml->cServico->Codigo]['valor'] = 0;
                $retorno[(string)$xml->cServico->Codigo]['prazo_entrega'] = 0;
			}
        }
		
		return $retorno;
    }

}

/* End of file Correios.php */
/* Location: ./application/libraries/correios.php */
