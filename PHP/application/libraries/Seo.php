<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SEO {
	
	private $baseTitle = _TITLE_;
	private $title = _TITLE_;
	private $description;
	private $keywords;
	
	function title($n = null){
		if(!is_null($n))
			$this->title = ($n==''?'':$n.' | ').$this->baseTitle;
		return $this->title;
	}
	
	function description($n = null){
		if(!is_null($n))
			$this->description = $n;
		return $this->description;
	}
	
	function keywords($n = null){
		if(!is_null($n))
			$this->keywords = $n;
		return $this->keywords;
	}
}