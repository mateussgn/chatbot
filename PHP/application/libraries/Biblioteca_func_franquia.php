<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 /*
  AUTOR: CLEBER
 */
class Biblioteca_func_franquia {
    public function __construct()
    {
    }
 

   

   public function configuracoesModulares_Franquia(){
     $modulos['ITEMMODULAR'] = array(
        "USUÁRIOS"        => array('franquia','relatoriocadastro','relatorioacesso'),
        "ADMINISTRATIVO" => array('contasreceber', 'contaspagar','resulFinanc', 'relatorios','documento'),
        "COMERCIAL"      => array('clientes', 'clientes_preCad','colaboradores','taxacolaborador','fornecedores','parceiros','indicacoes','eventos', 'pre_reserva_evento', 'visitascomerciais','relatorioscom'),
        "SUPRIMENTOS"    => array('estoqueFranquia','estoquefranqcompleto')
      );

      //CORREÇÃO DA ESCRITA DOS ITENS MODULARES PARA NAVEGAÇÃO
        $modulos['ITEM_MODCORRIGIR'] = ARRAY(
               //"usuario"            => "Usuários",   
               "franquia"           => "Usuários",   
               "relatoriocadastro"  => "Relatório de Cadastros",   
               "relatorioacesso"    => "Relatório de Acessos",   
               "clientes"           => "Cadastro de Cliente",   
               "colaboradores"      => "Colaboradores",
               "taxacolaborador"      => "Taxas Colaborador",
               "clientes_preCad"    => "Pré Cadastro Cliente",
               "fornecedores"       => "Fornecedores",
               "parceiros"          => "Parceiros",   
               "indicacoes"         => "Indicações", 
               "eventos"            => "Eventos", 
               "pre_reserva_evento" => "Pré reserva de Evento",

               "visitascomerciais"  => "Visitas Comerciais", 
               // "ofclientesativos"   => "Oferta Clientes Ativos", 
               // "ofclientesinativos" => "Oferta Clientes Inativos", 
               
               "relatorioscom"      => "Relatórios", 
               "contasreceber"      => "Contas a receber", 
               "contaspagar"        => "Contas a pagar", 
               "resulFinanc"        => "Resultado Financeiro", 
               "indicadores"        => "Indicadores", 
               "relatorios"         => "Relatórios", 
               "documento"          => "Documentos", 
               
               "estoqueFranquia"    => "Solicitação de Estoque",
               "estoquefranqcompleto" => "Estoque Franquia", 
        );


        //APLICANDO ICONES
        $modulos['ICONE'] = ARRAY(
               "USUÁRIOS"       => "fa fa-gears",   
               "COMERCIAL"      => "fa fa-building-o",  
               "ADMINISTRATIVO" => "hi hi-globe", 
               "SUPRIMENTOS"    => "gi gi-notes_2", 
        );


  	return $modulos;
  }




/*
  | Recebe um conjunto de dados e os organiza em
  | fila seperados por ponto e virgula
*/
	public function concatenarPermis($array) {
		$result = count( $array );
		$permi = "";
		for ($i = 0; $i <= $result-1; $i++) {
			$permi .= $array[$i] . ";";
		}
		return $permi;
	}

  
  public function concatenarArr($array) {
    $result = count( $array );
    $strings = "";
    for ($i = 0; $i <= $result-1; $i++) {
      $strings .= $array[$i] . ",";
    }
    $strings = substr($strings, 0,-1);
    return $strings;
  }
/*
  -----------------------------------------------
*/




}