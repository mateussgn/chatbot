<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once __DIR__.'/nbot/vendor/autoload.php';
use DialogFlow\Client;
use DialogFlow\Model\Query;
use DialogFlow\Method\QueryApi;

class bot {

	function usa_api($sender, $msg, $dialogflowclient) {
		
		try {

	    $client = new Client($dialogflowclient);
	    $queryApi = new QueryApi($client);

	    $meaning = $queryApi->extractMeaning("$msg", [
	        'sessionId' => "$sender",
	        'lang' => 'pt-br',
	    ]);
	    $response = new Query($meaning);
	    $houveerro = 0;
		} catch (\Exception $error) {
		    $error->getMessage();
		    $houveerro = 1;
		}

		if($houveerro==0){
		$response = json_encode($response);
		$r = json_decode($response);

		//echo '<pre>';print_r($response);
		// echo $textResposta = $r->result->fulfillment->messages[0]->textToSpeech;
		//die();


		$r = $r->result;
		$action = $r->action;
		$resolvedQuery = $r->resolvedQuery;
		$parameters = $r->parameters;

		if(isset($r->fulfillment->messages[0]->speech)){
			$textResposta = $r->fulfillment->messages[0]->speech;	
		}else{
			$textResposta = $r->fulfillment->messages[0]->textToSpeech;	
		}


		$textResposta = preg_replace('/\r?\n|\r/','<br/>', $textResposta);
		$textResposta =  strip_tags(str_replace('<br/>', '\r\n', nl2br($textResposta)));

		return $textResposta;
		}
	}
}
