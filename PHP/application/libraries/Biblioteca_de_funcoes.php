<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
 /*
  AUTOR: CLEBER
 */


class Biblioteca_de_funcoes {
    public function __construct()
    {
    }


   public function configuracoesModulares(){
  	 $modulos['ITEMMODULAR'] = array(
        "USUÁRIOS"       => array('usuario', 'franquia','relatoriocadastro','relatorioacesso'),
        
        "ADMINISTRATIVO" => array('contasreceber', 'contaspagar', 'resulFinanc', 'relatorios','documento'),

        "COMERCIAL"      => array('clientes', 'clientes_preCad','colaboradores','fornecedores','parceiros','indicacoes','eventos', 'pre_reserva_evento', 'visitascomerciais', 'ofclientesativos', 'ofclientesinativos','relatorioscom'),
       
        "SUPRIMENTOS"    => array(//estoquefranqcompleto o admin não precisa
                              'estoqueInsumos', 'estoqueFranquiadora','estoqueFranquia','estoquefranqcompleto'),

      "LOJA"    => array('loja_produto', 'pedidos'),
        
      "SITE"    => array('categoriasite', 'galeria', 'fundo_site', 'layout_site', 'pagina_site','banner'),


        "CONFIGURAÇÕES"  => array('atividades', 'tipos_eventos', 'layout', 'fundo','duracao','grupo_deSuprimentos','estado')
      );

  		//CORREÇÃO DA ESCRITA DOS ITENS MODULARES PARA NAVEGAÇÃO
        $modulos['ITEM_MODCORRIGIR'] = ARRAY(
               "usuario"            => "Usuários",   
               "franquia"           => "Franquia",   
               "relatoriocadastro"  => "Relatório de Cadastros",   
               "pedidos"            => "Pedidos",   
               "relatorioacesso"    => "Relatório de Acessos",   
               "clientes"           => "Cadastro de Cliente",   
               "colaboradores"      => "Colaboradores",   
               "clientes_preCad"    => "Pré Cadastro Cliente",
               "fornecedores"       => "Fornecedores",
               "parceiros"          => "Parceiros",   
               "indicacoes"         => "Indicações", 
               
               "eventos"            => "Eventos", 
               "pre_reserva_evento" => "Pré reserva de Evento",

               "visitascomerciais"  => "Visitas Comerciais", 
               "ofclientesativos"   => "Oferta Clientes Ativos", 
               "ofclientesinativos" => "Oferta Clientes Inativos", 
               "relatorioscom"      => "Relatórios", 
               "contasreceber"      => "Contas a receber", 
               "contaspagar"        => "Contas a pagar", 
               "resulFinanc"        => "Resultado Financeiro", 
               "indicadores"        => "Indicadores", 
               "relatorios"         => "Relatórios", 
               "documento"          => "Documentos", 
               
               "estoqueInsumos"       => "Estoque Insumos", 
               "estoqueFranquiadora"  => "Estoque Franqueadora", 
               "estoqueFranquia"      => "Solicitar Suprimento", 
               "estoquefranqcompleto" => "Estoque Franquia", 
               
               "atividades"         => "Atividades",
               "tipos_eventos"      => "Tipos de eventos",
               "layout"             => "Layout",
               "fundo"              => "Fundos (backgrounds)",
               "duracao"            => "Duração Eventos",
               "produto"              => "Produto",
               "grupo_deSuprimentos"  => "Grupo de Suprimentos",
               "estado"  => "Estados",
               "banner"  => "Banners",
               "estoquefranqcompleto" => "Estoque Franquia Completo", 
               
               "loja_produto"         => "Produto", 

                "categoriasite" => "Galeria: Categorias",
                "galeria"       => "Galeria de Fotos",
                "fundo_site"    => "Fundos",
                "layout_site"   => "Layouts",
                "pagina_site"   => "Paginas Internas",

        );


        //APLICANDO ICONES
        $modulos['ICONE'] = ARRAY(
               "USUÁRIOS"       => "gi gi-more_items",   
               "COMERCIAL"      => "fa fa-building-o",  
               "ADMINISTRATIVO" => "hi hi-globe", 
               "SUPRIMENTOS"    => "gi gi-notes_2", 
               "LOJA"           => "gi gi-shopping_cart",
               "SITE"           => "fa fa-clone",
               "CONFIGURAÇÕES"  => "fa fa-gears", 
                
        );

  	return $modulos;
  }




/*
  | Recebe um conjunto de dados e os organiza em
  | fila seperados por ponto e virgula
*/
	public function concatenarPermis($array) {
		$result = count( $array );
		$permi = "";
		for ($i = 0; $i <= $result-1; $i++) {
			$permi .= $array[$i] . ";";
		}
		return $permi;
	}

  public function concatenarArr($array) {
    $result = count( $array );
    $strings = "";
    for ($i = 0; $i <= $result-1; $i++) {
      $strings .= $array[$i] . ",";
    }
    $strings = substr($strings, 0,-1);
    return $strings;
  }
/*
  -----------------------------------------------
*/




}