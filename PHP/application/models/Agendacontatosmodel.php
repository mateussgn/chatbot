<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE

id INT
*/

class Agendacontatosmodel extends CI_Model {
	static private $DB_TABLE = 'contacts';
	function __construct(){
		parent::__construct();
	}

	function apagarAgendaContatos(){
        $this->db->truncate(self::$DB_TABLE);
    }
	
	function getListAll(){
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function countContatos(){
		return $this->db->count_all_results(self::$DB_TABLE);
	}

    //listagem de clientes para Administrador
	function getList($estado=''){
		$this->db->order_by('nome', 'ASC');
		if($estado <> ''){
			$this->db->where("estadoCadastro = '$estado'");
			$query = $this->db->get(self::$DB_TABLE);
		}else{
			$query = $this->db->get(self::$DB_TABLE);
		}
		
		return $query->result();
	}

	function getListWhere($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListAutenticar($login, $senha){
		
		$this->db->where("email = '$login'");
		$this->db->where("senha = '$senha'");
		$query = $this->db->get(self::$DB_TABLE);
				
		return $query->result();
	}

	function getUsu_porEmailCPF($email,$cpf){
		$this->db->where('email =', $email);
		$this->db->where('cnpj_cpf =', $cpf);
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListPage($page, $qntPage){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$query = $this->db->get(self::$DB_TABLE);

		return $query->result();
	}
	
	function getListPageSearch($page, $qntPage, $titulo){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$this->db->like('titulo',$titulo,'both');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}
	
	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function updateNome($number, $nome){
		$data = array('nome' => $nome);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateBairro($number, $bairro){
		$data = array('bairro' => $bairro);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateRua($number, $rua){
		$data = array('rua' => $rua);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateNumero($number, $numero){
		$data = array('numero' => $numero);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateComplemento($number, $complemento){
		$data = array('complemento' => $complemento);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updatePontoDeReferencia($number, $pontoDeReferencia){
		$data = array('pontoDeReferencia' => $pontoDeReferencia);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateCodigo($number, $codigoWifi){
		$data = array('codigoWifi' => $codigoWifi);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}
	
	function totalPaginas() {
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function totalPaginasSearch($titulo) {
		$this->db->order_by('id', 'DESC');
		$this->db->like('nome', $titulo, 'both');
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function getByCPF($cpf){
		$this->db->where('cpf =', $cpf);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}
	
	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}

	function authenticate($login,$senha){
		$this->db->where(self::$DB_TABLE.'.usuario =', $login);
		$this->db->where(self::$DB_TABLE.'.senha =', $senha);
		$result = $this->db->get(self::$DB_TABLE, 1);
		
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function getContatoById($id){
		$result = $this->db->select("*")
		    ->where('id', $id)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $contato){
            return $contato;
        }
	}

	function getContato($number){
		$result = $this->db->select("*")
		    ->where('number', $number)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $contato){
            return $contato;
        }
	}

	function getUltimoContato($number){
		$result = $this->db->select("*")
		    ->where('number', $number)
            ->get(self::$DB_TABLE);
        $contato = $result->last_row();
        return $contato;
	}

    function getNumber($number){
        $result = $this->db->select('number')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->number;
        }
    }

    function getNome($number){
        $result = $this->db->select('nome')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->nome;
        }
    }

    function getBairro($number){
        $result = $this->db->select('bairro')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->bairro;
        }
    }
	
	function getRua($number){
		$result = $this->db->select('rua')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->rua;
        }
	}
	
	function getNumero($number){
		$result = $this->db->select('numero')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->numero;
        }
	}

    function getComplemento($number){
		$result = $this->db->select('complemento')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->complemento;
        }
	}
	
	function getPontoDeReferencia($number){
		$result = $this->db->select('pontoDeReferencia')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $contato){
            return $contato->pontoDeReferencia;
        }
	}		
}