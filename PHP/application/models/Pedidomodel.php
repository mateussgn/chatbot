<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE


id INT
*/

class PedidoModel extends CI_Model {
	static private $DB_TABLE = 'pedido';
	function __construct(){
		parent::__construct();

		$this->load->model('bairrosmodel');
		$this->load->model('agendacontatosmodel');
	}
	
	function apagarPedidos(){
		$this->db->truncate(self::$DB_TABLE);
	}

    //listagem de clientes para Administrador
	function getList(){
		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();
	}

	function getListWhere($array,$limit){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if($limit<>''){
			$this->db->limit($limit);	
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListWhereSearch($array, $arraysearch,$datainicio,$datafim){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}

		$this->db->where('timestamp >=',$datainicio.' 00:00:01');		
		$this->db->where('timestamp <=',$datafim.' 23:59:59');		

		if(count($arraysearch) > 0){
			foreach ($arraysearch as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}

	function getListWhereLast($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if($limit<>''){
			$this->db->limit($limit);	
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}


	function getListWhereCount($arr = 0){
		$this->db->order_by('id', 'DESC');
		if(count($arr) > 0){
			foreach ($arr as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;

	}

	function getListWhereLike($array,$likes){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if(count($likes) > 0){
			foreach ($likes as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListAutenticar($login, $senha){
		
		$this->db->where("email = '$login'");
		$this->db->where("senha = '$senha'");
		$query = $this->db->get(self::$DB_TABLE);
				
		return $query->result();
	}

	function getUsu_porEmailCPF($email,$cpf){
		$this->db->where('email =', $email);
		$this->db->where('cnpj_cpf =', $cpf);
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}



	function getListPage($page, $qntPage){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$query = $this->db->get(self::$DB_TABLE);

		return $query->result();
	}
	
	function getListPageSearch($page, $qntPage, $titulo){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$this->db->like('titulo',$titulo,'both');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}
	
	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function updatenumber($ObjVO){
		$this->db->where('receive =', $ObjVO->receive);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

///////////////////////////////////////////////////////
	function updateFaseDoPedido($number, $faseDoPedido){
		$data = array('faseDoPedido' => $faseDoPedido);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateNome($number, $nome){
		$data = array('nome' => $nome);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateBairro($number, $bairro){
		$data = array('bairro' => $bairro);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateRua($number, $rua){
		$data = array('rua' => $rua);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateNumero($number, $numero){
		$data = array('numero' => $numero);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateComplemento($number, $complemento){
		$data = array('complemento' => $complemento);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updatePontoDeReferencia($number, $pontoDeReferencia){
		$data = array('pontoDeReferencia' => $pontoDeReferencia);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateQuantidade($number, $quantidade){
		$data = array('quantidade' => $quantidade);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateFormaDePagamento($number, $formaDePagamento){
		$data = array('formaDePagamento' => $formaDePagamento);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateTroco($number, $troco){
		$data = array('troco' => $troco);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateBandeiraCartao($number, $bandeiraCartao){
		$data = array('bandeiraCartao' => $bandeiraCartao);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateValor($number, $valor){
		$contato = $this->agendacontatosmodel->getUltimoContato(str_replace('@c.us', '', $number));
        $bairro = $this->bairrosmodel->getBairro((int)$contato->bairro);
        $pedido = $this->pedidomodel->getUltimoPedido($number);

		if($valor == "Debito"){
            $valor = $bairro->preco_debito * $pedido->quantidade;
		} elseif ($valor == "Credito"){
            $valor = $bairro->preco_credito * $pedido->quantidade;
		} elseif ($valor == "Vale") {
			$valor = $bairro->preco_vale * $pedido->quantidade;
		} else {
		    $valor = $bairro->preco_dinheiro * $pedido->quantidade;
		}
		$data = array('valor' => $valor);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateVale($number, $vale){
		$data = array('vale' => $vale);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateIdEntregador($number, $id_entregador){
		$data = array('id_entregador' => $id_entregador);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateData($number, $dataDoPedido){
		$date = Date('d/m/Y H:i:s',strtotime($date));
		$data = array('dataDoPedido' => $dataDoPedido);

		$this->db->where('number =', $number);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}
////////////////////////////////////
	function getPedido($number){
		$result = $this->db->select("*")
		    ->where('number', $number)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $pedido){
            return $pedido;
        }
	}

	function getUltimoPedido($number){
		$result = $this->db->select("*")
		    ->where('number', $number)
            ->get(self::$DB_TABLE);
        $pedido = $result->last_row();
        return $pedido;
	}

	function getFasedoPedido($number){
        $result = $this->db->select('faseDoPedido')
            ->where('number', $number)
            ->get(self::$DB_TABLE);
        if(empty($result)){
        	return 0;
        }
        foreach($result->result() as $pedido){
            return $pedido->faseDoPedido;
        }
    }

    function getNumber($number){
        $result = $this->db->select('number')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->number;
        }
    }

    function getNome($number){
        $result = $this->db->select('nome')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->nome;
        }
    }

	function getBairro($number){
        $result = $this->db->select('bairro')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->bairro;
        }
    }
	
	function getRua($number){
		$result = $this->db->select('rua')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->rua;
        }
	}
	
	function getNumero($number){
		$result = $this->db->select('numero')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->numero;
        }
	}
	
	function getComplemento($number){
		$result = $this->db->select('complemento')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->complemento;
        }
	}
	
	function getPontoDeReferencia($number){
		$result = $this->db->select('pontoDeReferencia')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->pontoDeReferencia;
        }
	}

	function getFormaDePagamento($number){
		$result = $this->db->select('formaDePagamento')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->formaDePagamento;
        }
	}

	function getValor($number){
		$result = $this->db->select('valor')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->valor;
        }
	}

	function getTroco($number){
		$result = $this->db->select('troco')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->troco;
        }
	}

	function getBandeiraCartao($number){
		$result = $this->db->select("*")
		    ->where('number', $number)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $pedido){
            return $pedido->bandeiraCartao;
        }
	}

	function getData($number){
		$result = $this->db->select('dataDoPedido')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->dataDoPedido;
        }
	}

	function getVale($number){
		$result = $this->db->select('vale')
            ->where('number', $number)
            ->get(self::$DB_TABLE);

        foreach($result->result() as $pedido){
            return $pedido->vale;
        }
	}
////////////////////////////////////
	function totalPaginas() {
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function totalPaginasSearch($titulo) {
		$this->db->order_by('id', 'DESC');
		$this->db->like('nome', $titulo, 'both');
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function getByCPF($cpf){
		$this->db->where('cpf =', $cpf);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}
	
	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}

	function authenticate($login,$senha){
		$this->db->where(self::$DB_TABLE.'.usuario =', $login);
		$this->db->where(self::$DB_TABLE.'.senha =', $senha);
		$result = $this->db->get(self::$DB_TABLE, 1);
		
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}	
}