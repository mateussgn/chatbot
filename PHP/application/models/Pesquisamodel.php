<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE


id INT
*/

class PesquisaModel extends CI_Model {
	static private $DB_TABLE = 'pesquisa';
	function __construct(){
		parent::__construct();
    }

    function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function updateNota($number, $nota){
        $data = array('nota' => $nota);

		$this->db->where('number =', $number);
		$this->db->update(self::$DB_TABLE, $data);
	}

	function updateComentario($number, $comentario){
        $data = array('comentario' => $comentario);

		$this->db->where('number =', $number);
		$this->db->update(self::$DB_TABLE, $data);
	}    
}