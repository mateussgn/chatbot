<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE

id INT
*/

class CidadeModel extends CI_Model {
	static private $DB_TABLE = 'cidade';

	function __construct(){
		parent::__construct();
	}
	
	

    //listagem de clientes para Administrador
	function getList(){
		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListGeral(){
		$result = $this->db->query("
			SELECT cidade.id as id,cidade.nome as nome, estado.uf as uf,estado.nome as estado FROM `cidade` Inner JOIN estado on estado.id = cidade.estado
			");			

		return $result->result();
	}
	


	function getListByEstado($uf){
		$this->db->where('uf', $uf);
		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListPage($page, $qntPage){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$query = $this->db->get(self::$DB_TABLE);

		return $query->result();
	}
	
	function getListPageSearch($page, $qntPage, $titulo){
		$this->db->order_by('id', 'DESC');
		$this->db->limit($qntPage,$page);
		$this->db->like('titulo',$titulo,'both');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}
	
	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}
	
	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}
	
	function totalPaginas() {
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function totalPaginasSearch($titulo) {
		$this->db->order_by('id', 'DESC');
		$this->db->like('nome', $titulo, 'both');
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}
	
	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}

	function authenticate($login,$senha){
		$this->db->where(self::$DB_TABLE.'.login =', $login);
		$this->db->where(self::$DB_TABLE.'.senha =', $senha);
		$result = $this->db->get(self::$DB_TABLE, 1);
		
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;


	}

	
	
}