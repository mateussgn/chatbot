<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE


id INT
*/

class EntregadoresModel extends CI_Model {
	static private $DB_TABLE = 'entregadores';
	function __construct(){
		parent::__construct();
	}

	function apagarEntregadores(){
		$this->db->truncate(self::$DB_TABLE);
	}

	function getEntregador($telefone){
		$result = $this->db->select("*")
		    ->where('telefone', $telefone)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $pedido){
            return $pedido;
        }
	}

	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function updateNome($telefone, $nome){
		$data = array('nome' => $nome);

		$this->db->where('telefone =', $telefone);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateStatus($telefone, $status){
		$data = array('status' => $status);

		$this->db->where('telefone =', $telefone);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function insertEntregador($ObjVO){
		/*$insert = */$this->db->insert(self::$DB_TABLE, $ObjVO);
		//return $insert;
	}

	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}

	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}

	function getListWhere($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListWhereActive($array){
		$this->db->where('status =', 1);
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}
}