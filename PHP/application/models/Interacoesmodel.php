<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE

id INT
*/

class InteracoesModel extends CI_Model {
	static private $DB_TABLE = 'interacoes';
	function __construct(){
		parent::__construct();
	}

    function getInteracao($id, $id_cliente){
		$result = $this->db->select("*")
		    ->where('id', $id)
		    ->where('id_cliente', $id_cliente)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $interacao){
            return $interacao;
        }
	}

	function getListWhere($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}
}