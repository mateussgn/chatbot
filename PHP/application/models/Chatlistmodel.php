<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE

id INT
*/

class ChatlistModel extends CI_Model {
	static private $DB_TABLE = 'chat_list';
	function __construct(){
		parent::__construct();
	}
	
	function apagarChatList(){
        $this->db->truncate(self::$DB_TABLE);
    }

    //listagem de clientes para Administrador
	function getList(){

		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();
	}

	function getListWhere($array,$limit){
		$this->db->not_like('receive','-','both');		
		$this->db->not_like('receive','status','both');		
		
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if($limit<>''){
			$this->db->limit($limit);	
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListWhereSearch($array, $arraysearch,$datainicio,$datafim){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}

		$this->db->where('timestamp >=',$datainicio.' 00:00:01');		
		$this->db->where('timestamp <=',$datafim.' 23:59:59');		

		if(count($arraysearch) > 0){
			foreach ($arraysearch as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}

	function getListWhereLast($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if($limit<>''){
			$this->db->limit($limit);	
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}


	function getListWhereCount($arr = 0){
		$this->db->order_by('id', 'DESC');
		if(count($arr) > 0){
			foreach ($arr as $key => $value) {
				$this->db->where($key,$value);		
			}			
		}

		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;

	}

	function getListWhereLike($array,$likes){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if(count($likes) > 0){
			foreach ($likes as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListAutenticar($login, $senha){
		
		$this->db->where("email = '$login'");
		$this->db->where("senha = '$senha'");
		$query = $this->db->get(self::$DB_TABLE);
				
		return $query->result();
	}

	function getUsu_porEmailCPF($email,$cpf){
		$this->db->where('email =', $email);
		$this->db->where('cnpj_cpf =', $cpf);
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}



	function getListPage($page, $qntPage){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$query = $this->db->get(self::$DB_TABLE);

		return $query->result();
	}
	
	function getListPageSearch($page, $qntPage, $titulo){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$this->db->like('titulo',$titulo,'both');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}
	
	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function updatenumber($ObjVO){
		$this->db->where('receive =', $ObjVO->receive);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function updateNome($receive, $name){
		$data = array('name' => $name);

		$this->db->where('receive =', $receive);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}
	
	function totalPaginas() {
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function totalPaginasSearch($titulo) {
		$this->db->order_by('id', 'DESC');
		$this->db->like('nome', $titulo, 'both');
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function getByNumber($number){
		$this->db->where('receive =', $number);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function getByCPF($cpf){
		$this->db->where('cpf =', $cpf);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}


	


	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}

	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}

	function authenticate($login,$senha){
		$this->db->where(self::$DB_TABLE.'.usuario =', $login);
		$this->db->where(self::$DB_TABLE.'.senha =', $senha);
		$result = $this->db->get(self::$DB_TABLE, 1);
		
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;


	}

	
	
}