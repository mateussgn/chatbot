<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE


id INT
*/

class BairrosModel extends CI_Model {
	static private $DB_TABLE = 'bairros';
	function __construct(){
		parent::__construct();
	}

	function apagarBairros(){
		$this->db->truncate(self::$DB_TABLE);
	}

	function getBairro($id){
		$result = $this->db->select("*")
		    ->where('id', $id)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $bairro){
            return $bairro;
        }
	}

	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}

	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}

	function getListWhere($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}
}