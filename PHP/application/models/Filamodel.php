<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE

id INT
*/

class FilaModel extends CI_Model {
	static private $DB_TABLE = 'fila';
	function __construct(){
		parent::__construct();
	}
	
	function apagarFila(){
		$this->db->truncate(self::$DB_TABLE);
	}

    //listagem de clientes para Administrador
	function getList(){
		$this->db->order_by('nome', 'ASC');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();
	}

	function getListWhere($array,$limit){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if($limit<>''){
			$this->db->limit($limit);	
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListWhereDeps($array,$deps,$limit){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if(count($deps) > 0){
			$this->db->where_in('id_departamento',$deps);		
		}
		if($limit<>''){
			$this->db->limit($limit);	
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}


	function getListWhereCount($array){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}

	function getListWhereCountFila($number){
		$this->db->order_by('id', 'DESC');
		$this->db->where('receive',$number);
		$this->db->where('status = ','1');
		$this->db->limit(1);
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}

	function getListWhereCountFilaNovo($number){
		$this->db->order_by('id', 'DESC');
		$this->db->where('receive',$number);
		$status = array('1', '0');

		$this->db->where_in('status',$status);
		//$this->db->or_where('status = ','1');
		//$this->db->limit(1);
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}

	function getCountAtendimentos(){
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}

	function getListWhereCountFilaAll($number){
		
		$this->db->order_by('id', 'DESC');
		$this->db->where('receive',$number);		
		
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}



	function getListDepartamentoMaisAcessado(){
			
		$result = $this->db->query("
		SELECT count(id) as qtd,id_departamento
		from fila
		GROUP BY id_departamento
		");				
	
		

		return $result->result();
	}

	function getListAtendentesMaisAtivos(){
			
		$result = $this->db->query("
		SELECT count(id) as qtd,id_atendente
		from fila
		GROUP BY id_atendente
		");				
	
		

		return $result->result();
	}


	function getListFilhaDetalhada(){
			
		$result = $this->db->query("
		SELECT fila.protocolo, fila.receive, fila.status, departamentos.titulo, operador.nome AS operador, 
		fila.dataatendimento, fila.datatermino
		FROM fila
		INNER JOIN departamentos ON departamentos.id = fila.id_departamento
		INNER JOIN operador ON operador.id = fila.id_atendente
		INNER JOIN chat_list ON fila.receive = chat_list.receive
		");				
	
		

		return $result->result();
	}




	function getListWhereLike($array,$likes){
		$this->db->order_by('id', 'DESC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		if(count($likes) > 0){
			foreach ($likes as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListAutenticar($login, $senha){
		
		$this->db->where("email = '$login'");
		$this->db->where("senha = '$senha'");
		$query = $this->db->get(self::$DB_TABLE);
				
		return $query->result();
	}

	function getUsu_porEmailCPF($email,$cpf){
		$this->db->where('email =', $email);
		$this->db->where('cnpj_cpf =', $cpf);
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}



	function getListPage($page, $qntPage){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$query = $this->db->get(self::$DB_TABLE);

		return $query->result();
	}
	
	function getListPageSearch($page, $qntPage, $titulo){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$this->db->like('titulo',$titulo,'both');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}
	
	function insert($ObjVO){
		$insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		return $insert;
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}

	function updateNome($receive, $nome){
		$data = array('nome' => $nome);

		$this->db->where('receive =', $receive);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateStatus($protocolo, $status){
		$data = array('status' => $status);

		$this->db->where('protocolo =', $protocolo);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}

	function updateData($protocolo, $datatermino){
		$data = array('datatermino' => $datatermino);

		$this->db->where('protocolo =', $protocolo);
		/*$update = */$this->db->update(self::$DB_TABLE, $data);
		//return $update;
	}
	
	function totalPaginas() {
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function totalPaginasSearch($titulo) {
		$this->db->order_by('id', 'DESC');
		$this->db->like('nome', $titulo, 'both');
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	/*function getByProtocolo($protocolo){
		$this->db->where('protocolo =', $protocolo);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}*/
	
	function getByProtocolo($protocolo){
		$result = $this->db->select("*")
		    ->where('protocolo', $protocolo)
            ->get(self::$DB_TABLE);
        foreach($result->result() as $fila){
            return $fila;
        }
	}

	function getByCPF($cpf){
		$this->db->where('cpf =', $cpf);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}


	


	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}

	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}

	function authenticate($login,$senha){
		$this->db->where(self::$DB_TABLE.'.usuario =', $login);
		$this->db->where(self::$DB_TABLE.'.senha =', $senha);
		$result = $this->db->get(self::$DB_TABLE, 1);
		
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;


	}



	function getListWhereSearch($arraysearch,$datainicio,$datafim){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}

		$this->db->where('timestamp >=',$datainicio.' 00:00:01');		
		$this->db->where('timestamp <=',$datafim.' 23:59:59');		

		if(count($arraysearch) > 0){
			foreach ($arraysearch as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}

	
	
}
