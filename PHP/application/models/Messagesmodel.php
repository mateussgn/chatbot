<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
Fields in DATABASE

id INT
*/

class MessagesModel extends CI_Model {
	static private $DB_TABLE = 'messages';
	function __construct(){
		parent::__construct();
	}
	
	function apagarMessages(){
		$this->db->truncate(self::$DB_TABLE);
	}

    //listagem de clientes para Administrador
	function getList($estado=''){
		$this->db->order_by('nome', 'ASC');
		if($estado <> ''){
			$this->db->where("estadoCadastro = '$estado'");
			$query = $this->db->get(self::$DB_TABLE);
		}else{
			$query = $this->db->get(self::$DB_TABLE);
		}
		
		return $query->result();
	}

	function getListFirstLastMessage($array,$timestamp='ASC'){
		$this->db->order_by('timestamp', $timestamp);
		$this->db->limit('1');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;

	}

	function getListWhereCountMessageAll($number){
		
		$this->db->order_by('id', 'DESC');
		$this->db->where('receive',$number);		
		
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}


	function getListWhereLast($array){
		$this->db->order_by('id', 'DESC');
		$this->db->limit('1');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			
			return $result[0];
		}
		return null;

	}

	function getListWhere($array){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListWhereLimit($array,$limite){
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limite);

		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListWhereSearch($array, $arraysearch,$datainicio,$datafim){
		$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		

		$this->db->where('timestamp >=',$datainicio.' 00:00:01');		
		$this->db->where('timestamp <=',$datafim.' 23:59:59');		

		if(count($arraysearch) > 0){
			foreach ($arraysearch as $key => $value) {
				$this->db->like($key,$value,'both');		
			}
			
		}
		
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}

	function getListWhereSearchCount($texto){
		$this->db->like('texto',$texto,'both');		
		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}

	function getListWhereSearchGroupby($array,$datainicio,$datafim){
		
		$this->db->select('receive');
		$this->db->group_by('receive');
		//$this->db->order_by('id', 'ASC');
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}

		$this->db->where('timestamp >=',$datainicio.' 00:00:01');		
		$this->db->where('timestamp <=',$datafim.' 23:59:59');		

		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}

	function getListWhereSearchMesaMes($id_cliente, $texto,$datainicio,$datafim){
			
			$result = $this->db->query("
			SELECT sum(1) as qtd, Month(timestamp) as Mes
			from messages
			WHERE timestamp >='".$datainicio." 00:00:01' and timestamp <='".$datafim." 00:00:01' and id_cliente='$id_cliente' and  texto like '%$texto%'
			GROUP BY Month(timestamp)
			Order By Month(timestamp) ASC
			");				
		
		return $result->result();
	}

	function getListWhereCount($arr = 0){
		$this->db->order_by('id', 'DESC');
		if(count($arr) > 0){
			foreach ($arr as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}

		$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;

	}


	function getListMesaMes($ano = 0,$id_cliente = 0){
		if($id_cliente <> ''){
			$result = $this->db->query("
			SELECT sum(1) as qtd, Month(timestamp) as Mes
			from messages
			WHERE YEAR(datahora) = '$ano' and id_cliente='$id_cliente'
			GROUP BY Month(timestamp)
			Order By Month(timestamp) ASC
			");				
		}else{
			$result = $this->db->query("
			SELECT sum(1) as qtd, Month(timestamp) as Mes
			from messages
			WHERE YEAR(datahora) = '$ano'
			GROUP BY Month(timestamp)
			Order By Month(timestamp) ASC
			");				
		}
		

		return $result->result();
	}


	function getListWhereNext($array,$id){
		$this->db->order_by('id', 'ASC');
		//$this->db->limit(1);
		if(count($array) > 0){
			foreach ($array as $key => $value) {
				$this->db->where($key,$value);		
			}
			
		}
		$this->db->where('id >',$id);		
		
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}

	function getListAutenticar($login, $senha){
		
		$this->db->where("email = '$login'");
		$this->db->where("senha = '$senha'");
		$query = $this->db->get(self::$DB_TABLE);
				
		return $query->result();
	}

	function getUsu_porEmailCPF($email,$cpf){
		$this->db->where('email =', $email);
		$this->db->where('cnpj_cpf =', $cpf);
		$query = $this->db->get(self::$DB_TABLE);
		return $query->result();
	}



	function getListPage($page, $qntPage){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$query = $this->db->get(self::$DB_TABLE);

		return $query->result();
	}
	
	function getListPageSearch($page, $qntPage, $titulo){
		$this->db->order_by('nome', 'ASC');
		$this->db->limit($qntPage,$page);
		$this->db->like('titulo',$titulo,'both');
		$query = $this->db->get(self::$DB_TABLE);
		
		return $query->result();	
	}

	function getLastMessage($receive){
		$result = $this->db->select("*")
		    ->where('receive', $receive)
            ->get(self::$DB_TABLE);
        $LastMessage = $result->last_row();
        return $LastMessage;
	}
	
	function insert($ObjVO){
		if($ObjVO->media_type != "video/mp4"){
		    $insert = $this->db->insert(self::$DB_TABLE, $ObjVO);
		    return $insert;
	    }
	}

	function update($ObjVO){
		$this->db->where('id =', $ObjVO->id);
		$update = $this->db->update(self::$DB_TABLE, $ObjVO);
		return $update;
	}
	
	function totalPaginas() {
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function totalPaginasSearch($titulo) {
		$this->db->order_by('id', 'DESC');
		$this->db->like('nome', $titulo, 'both');
	   	$query = $this->db->get(self::$DB_TABLE);
		$cont = $query->num_rows();
		return $cont;
	}
	
	function getById($id){
		$this->db->where('id =', $id);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}

	function getByCPF($cpf){
		$this->db->where('cpf =', $cpf);
		$result = $this->db->get(self::$DB_TABLE, 1);
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;
	}


	function delByALL(){
		$this->db->where('id >', 0);
		return $this->db->delete(self::$DB_TABLE);
	}


	function delById($id){
		$this->db->where('id =', $id);
		return $this->db->delete(self::$DB_TABLE);
	}

	function authenticate($login,$senha){
		$this->db->where(self::$DB_TABLE.'.usuario =', $login);
		$this->db->where(self::$DB_TABLE.'.senha =', $senha);
		$result = $this->db->get(self::$DB_TABLE, 1);
		
		if($result->num_rows()>0){
			$result = $result->result();
			return $result[0];
		}
		return null;


	}

	
	
}