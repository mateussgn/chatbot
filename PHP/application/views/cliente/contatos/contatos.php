<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-10">
                        <h4 class="page-title">Leads de Contatos</h4>
                    </div>
                    
                </div>

                <div class="card-box">
                <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class)?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left">
                    
                    <div class="row">
                       
                        <div class="col-md-3">
                                <label for="example-hf-dataN">Data Inicio
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" required="required">
                        </div>
                        <div class="col-md-3">
                                <label for="example-hf-dataN">Data Fim
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" required="required">
                        </div>
                        <div class="col-md-2">
                                <br>
                                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Pesquisar">
                        </div>
                        
        
                    </div>
                    
                </form>

                <!--<form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/gerarpdf')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left" target="blank">
                    
                    <div class="row">
                        <input type="hidden" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" required="required">
                        <input type="hidden" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" required="required">
                        <div class="col-md-2">
                                <br>
                                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Gerar Relatório">
                        </div>
                    </div>
                    
                </form>-->
                <br clear="all">
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        
                        <div class="card-box">
                            <div class="card-block">
                                <?
                                if(count($messages)>0){
                                ?>
                                <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Numero</th>
                                            <th>Nome</th>
                                            <th>Primeira Interação</th>
                                            <th>Ultima Interação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($messages as $l) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=str_replace('@c.us', '', $l->receive)?></td>
                                            <td><?=$l->nome?></td>
                                            <td><?=$l->primeira?></td>
                                            <td><?=$l->ultima?></td>
                                            
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                                <?
                                }else{
                                ?>
                                    <h4 align="center">Por favor preencha os campos acima!</h4>
                                <?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

