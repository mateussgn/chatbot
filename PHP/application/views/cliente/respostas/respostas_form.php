<?php
$this->load->view($this->session->userdata('folder') . 'add-on/header', $modulos);
?>
<?php
if (isset($obj)) {
    $id        = $obj->id;
    $intencao  = $obj->intencao;
    $frases    = $obj->frases;
    $respostas = $obj->respostas;
    
    $btnsalvar = 'Salvar';
} else {
    $id        = '';
    $intencao  = '';
    $frases    = '';
    $respostas = '';
    
    $btnsalvar = 'Salvar';
    
}

?>
<div class="content container-fluid">
    <?php
show_alert();
?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Respostas</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?= site_url($this->session->userdata('folder') . $this->router->class . '') ?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php
echo site_url($this->session->userdata('folder') . $this->router->class . '/insert');
?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<?
echo $id;
?>">
                     
            <div class="row">
                <div class="col-md-12">
                        <label for="example-hf-nome">Intenção
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="intencao" class="form-control" placeholder="Intenção" value="<?= $intencao ?>" required="required">
                    
                </div>

            </div>

            <?
if ($cliente->frases == 1) {
?>
           <br clear="all">
            
            <div class="row">
                <div class="col-md-12">
                        <label for="example-hf-nome">Frases de Treinamento
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="frases" class="form-control" placeholder="Frases de Treinamento" value="<?= $frases ?>"  data-role="tagsinput">
                        <span class="label label-success-border">Para deletar algum item, com as setas de seu teclado navegue até o item desejado e com backspace remova.</span>
                    
                </div>

            </div>
            <?
}
?>
           <br clear="all">

             <div class="row">
                <div class="col-md-12">
                        <label for="example-hf-nome">Respostas
                            <span class="text-danger">*</span>
                        </label>
                        <br clear="all">
                        <?
foreach ($tags as $key => $value) {
?>
                       <!-- <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;"><?= $value->titulo ?> = <?= $value->tag ?></span> -->
                        <?
}
?>
                       <!-- <br clear="all"><br clear="all"> -->
                        <textarea rows="10" cols="5" class="form-control" name="respostas" placeholder="Descreva sua resposta"><?= $respostas ?></textarea>

                        
                </div>

            </div>

            <br clear="all">
            
            <div class="row">
                <div class="col-md-11">
                    <h4>Perguntas</h4>
                </div>
                <div class="col-md-1">
                    <a href="javascript://" class="btn btn-primary" onclick="clonediv('#clone','.listclones')"><i class="fa fa-plus"></i></a>
                </div>
            </div>
            
            <div class="row" id="clone">
                <div class="col-md-6">
                    <label for="example-hf-nome">Pergunta</label>
                    <input type="text" id="val-username" name="prompts[]" class="form-control" placeholder="Ex.: Qual seu nome?" value="<?= $prompts ?>">
                
                </div>
                <div class="col-md-3">
                    <label for="example-hf-nome">Campo</label>
                    <input type="text" id="val-username" name="campo[]" class="form-control" placeholder="Ex.: nome" value="<?= $prompts ?>">
                
                </div>
                <div class="col-md-3">
                    <label for="example-hf-nome">Tipo</label>
                    <select name="dataType[]" class="form-control">
                        <option value=""></option>
                        <?
foreach ($tagsDialogflow as $key => $value) {
?>
                           <option value="<?= $value->tag ?>" <?
    echo ($sexo == "m") ? "selected" : "";
?>><?= $value->titulo ?></option>
                        <?
}
?>
                       
                        
                    </select>    
                
                
                </div>
                <br clear="all"><br clear="all">
            </div>

            <div class="listclones">
                <?
if ($perguntas) {
    foreach ($perguntas as $key => $p) {
?>
                   <div class="row">
                        <div class="col-md-6">
                            <label for="example-hf-nome">Pergunta</label>
                            <input type="text" id="val-username" name="prompts[]" class="form-control" placeholder="Ex.: Qual seu nome?" value="<?= $p->prompts ?>">
                        
                        </div>
                        <div class="col-md-3">
                            <label for="example-hf-nome">Campo</label>
                            <input type="text" id="val-username" name="campo[]" class="form-control" placeholder="Ex.: nome" value="<?= $p->name ?>">
                        
                        </div>
                        <div class="col-md-3">
                            <label for="example-hf-nome">Tipo</label>
                            <select name="dataType[]" class="form-control">
                                <option value=""></option>
                                <?
        foreach ($tagsDialogflow as $key => $value) {
            if ($p->dataType == $value->tag) {
                $sel = 'selected="selected"';
            } else {
                $sel = '';
            }
?>
                                   <option <?= $sel ?> value="<?= $value->tag ?>" <?
            echo ($sexo == "m") ? "selected" : "";
?>><?= $value->titulo ?></option>
                                <?
        }
?>
                               
                                
                            </select>    
                        
                        
                        </div>
                        <br clear="all"><br clear="all">
                    </div>
                <?
    }
}
?>
           </div>

            <br clear="all">



            <br clear="all">
            
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?= $btnsalvar ?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php
$this->load->view($this->session->userdata('folder') . 'add-on/footer');
?>

<script type="text/javascript">
function clonediv(de,para){
    $( de ).clone().appendTo( para );
}

</script>