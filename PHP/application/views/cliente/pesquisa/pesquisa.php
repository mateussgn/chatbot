<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-10">
                        <h4 class="page-title">Pesquisa</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                                <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            <th width="7%">ID</th>
                                            <th width="7%">Descricao</th>
                                            <th>Interação</th>
                                            <th width="10%">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($list as $l) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=$l->id?></td>
                                            <td class="text-center"><?=$l->descricao?></td>
                                            <td><?=$l->interacao?></td>
                                            <td class="text-center">

                                                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/editar/'.$l->id)?>" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-sm btn-success" style="overflow: hidden; position: relative;" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                         </td><!--data-toggle="modal" data-target="#myModal"-->
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

