<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-10">
                        <h4 class="page-title">Vendas</h4>
                    </div>                    
                </div>

                <!--<form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/gerarRelatorioDetalhado')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left" target="blank">
                    
                    <div class="row">
                        <input type="hidden" id="val-username" name="protocolo" class="form-control" placeholder="Digite o protocolo" value="<?=$protocolo?>" >
                        <input type="hidden" id="val-username" name="numero" class="form-control" placeholder="Digite o número do telefone" value="<?=$numero?>" >
                        <input type="hidden" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" required="required">
                        <input type="hidden" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" required="required">
                        <div class="col-md-2">
                                <br>
                                <input type="submit" name="gerarRelatorioDetalhado" class="btn btn-effect-ripple btn-primary" value="Gerar Relatório">
                        </div>
                    </div>
                    
                </form>-->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                            <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Protocolo</th>
                                            <th>Numero</th>
                                            <th>Nome</th>
                                            <th>Status</th>
                                            <th>Bairro</th>
                                            <th>Operador</th>
                                            <th>Entregador</th>
                                            <th>Data</th>
                                            <!-- <th>Fila de Espera</th> -->
                                            <!--<th>ID Cliente</th>
                                            <th>Numero Celular</th>
                                            <th>Nome Cliente</th>
                                            <th>Departamento</th>
                                            <th>Operador</th>
                                            <th>Protocolo</th>
                                            <th>Status</th>
                                            <th>Entregador</th>
                                            <th>Bairro</th>
                                            <th>Forma de Pagamento</th>
                                            <th>Início</th>
                                            <th>Término</th>
                                            <th>Duração</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach (array_reverse($list) as $l) {
                                        ?>
                                        <!--
                                            <td></td>

                                        -->
                                        <tr>
                                            <td class="text-center"><?=$l->id?></td>
                                            <td class="text-center"><?=$l->protocolo?></td>
                                            <td><?=str_replace('@c.us', '', $l->receive)?></td>
                                            <td><?=$l->nome?></td>
                                            <td><?=$l->status?></td>
                                            <td><?=$l->id_bairro?></td>
                                            <td><?=$l->operador?></td>
                                            <td><?=$l->id_entregador?></td>
                                            <td><?=Date('d/m/Y H:i:s',strtotime($l->data))?></td>
                                            <!-- <td><?=$l->filaEspera?></td> 
                                            <td><?=Date('d/m/Y H:i:s',strtotime($l->dataatendimento))?></td>
                                            <td><?=Date('d/m/Y H:i:s',strtotime($l->datatermino))?></td>
                                            <td><?=$l->duracao?></td>-->
                                            
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

<?
//montando grafico
foreach ($meses as $key => $value) {
    $bars .= "{
                y: '$value->Mes',
                a: $value->qtd
            },";
}
$bars = substr($bars, 0,-1);
?>
<script type="text/javascript">
if ($("#bar-example").length > 0) {
    var colors = [
        '#E94B3B',
        '#39C7AA',
        '#1C7EBB',
        '#F98E33',
        '#ad96da'
    ];
    Morris.Bar({
        lineColors: colors,
        element: 'bar-example',
        data: [<?=$bars?>],
        xkey: 'y',
        ykeys: ['a'],
        resize: true,
        labels: ['Interações']
    });
    
}
</script>