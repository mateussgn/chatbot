<?php
$this->load->view($this->session->userdata('folder') . 'add-on/header', $modulos);
?>
<?php
if (isset($obj)) {
    $id        = $obj->id;
    $descricao = $obj->descricao;
    $interacao = $obj->interacao;
    
    $btnsalvar = 'Atualizar';
} else {
    $id        = '';
    $descricao = '';
    $interacao = '';

    $btnsalvar = 'Salvar';
    
}

?>
<div class="content container-fluid">
    <?php
show_alert();
?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Fluxo de Pedido</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?= site_url($this->session->userdata('folder') . $this->router->class . '') ?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php
echo site_url($this->session->userdata('folder') . $this->router->class . '/insert');
?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' enctype="multipart/form-data">

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<?
echo $id;
?>">
                
                <div class="row">
                    <div class="col-md-12">
                        <label>Descricao</label>
                        <input type="text" id="example-titulo" name="descricao" class="form-control" placeholder="Ex.: Menu" value="<?= $descricao ?>">
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                            <label for="example-hf-nome">Interação
                                <span class="text-danger">*</span>
                            </label>
                            <br clear="all">
                            <textarea rows="10" cols="5" class="form-control" name="interacao" placeholder="Descreva sua mensagem"><?= $interacao ?></textarea>

                            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Negrito = *Texto*</span>             
                            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Itálico = _Texto_</span>
                            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Nome do Cliente = %nome%</span>

                            <?
                            if ($id == 7) {
                            ?>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Valor Unitario = %preco_dinheiro% ou %preco_debito% ou %preco_credito% ou %preco_vale%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Valor Cheio = %pagamento_dinheiro% ou %pagamento_debito% ou %pagamento_credito% ou %pagamento_vale%</span>
                            <?
                            }
                            ?>

                            <?
                            if ($id == 11 || $id == 12 || $id == 14) {
                            ?>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Numer do Pedido = %numeroDoPedido%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Data  do Pedido = %dataDoPedido%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Celular = %number%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Bairro = %bairro%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Rua = %rua%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Numero = %numero%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Complemento = %complemento%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Ponto de Referencia = %pontoDeReferencia%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Quantidade = %quantidade%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Valor = %valor%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Forma de Pagamento = %formaDePagamento%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Troco = %troco%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Bandeira do Cartao = %bandeiraCartao%</span>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Vale = %vale%</span>
                                <?
                                if ($id == 14) {
                                ?>
                                    <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Localizacao = %localizacao%</span>
                                <?
                                }
                                ?>
                            <?
                            }
                            ?>
                            <?
                            if ($id == 15 || $id == 19) {
                            ?>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Protocolo = %protocolo%</span>
                            <?
                            }
                            ?>
                            <?
                            if ($id == 16) {
                            ?>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Entregador = %entregador%</span>
                            <?
                            }
                            ?>
                            <?
                            if ($id == 18) {
                            ?>
                                <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Status = %status%</span>
                            <?
                            }
                            ?>
                   </div>

                </div>


                <br clear="all">
               
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Salvar">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php
$this->load->view($this->session->userdata('folder') . 'add-on/footer');
?>