<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-10">
                        <h4 class="page-title">Relatórios - Detalhado</h4>
                    </div>
                    
                </div>
                
                <div class="card-box">
                <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/detalhado')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left">
                    

                        <div class="row">


                         <div class="col-md-4">
                                <label for="example-hf-nome">Protocolo
                                    <span class="text-danger"></span>
                                </label>
                                <input type="text" id="val-username" name="protocolo" class="form-control" placeholder="Digite o protocolo" value="<?=$protocolo?>" >
                            
                        </div> 

                        <div class="col-md-4">
                                <label for="example-hf-nome">Número
                                    <span class="text-danger"></span>
                                </label>
                                <input type="text" id="val-username" name="numero" class="form-control" placeholder="Digite o número do telefone" value="<?=$numero?>" >
                            
                        </div>  
                                
                        <div class="col-md-3">
                            <label for="example-hf-nome">Status
                                    <span class="text-danger"></span>
                            </label>
                            <select name="status" class="form-control">
                                    <option value="">Todos</option>
                                    <option value="0" class="ng-binding">Aguardando</option>
                                    <option value="1" class="ng-binding">Atendendo</option>
                                    <option value="2" class="ng-binding">Concluída</option>
                                    <option value="3" class="ng-binding">Abandono</option>

                            </select>
                        </div>

                        <br clear="all"><br clear="all">

                        <div class="col-md-3">
                            <label for="example-hf-nome">Operadores
                                    <span class="text-danger"></span>
                            </label>
                            <select name="id_operador" class="form-control ">
                                    <option value="">Todos</option>
                                    <?
                                    foreach($listOper as $l){
                                    ?>
                                    
                                    <option value="<?=$l->id?>"><?=$l->nome?></option>
                                    <?
                                    }
                                    ?>

                            </select>
                        </div>

                        <div class="col-md-3">
                            <label for="example-hf-nome">Departamentos
                                    <span class="text-danger"></span>
                            </label>
                            <select name="id_departamento" class="form-control">
                                <option value="">Todos</option>
                                    <?
                                    foreach($listDep as $l){
                                    ?>
                                    
                                    <option value=<?=$l->id?> class="ng-binding"><?=$l->titulo?></option>
                                    <?
                                    }
                                    ?>

                            </select>
                        </div>                      

                        
                        <div class="col-md-2">
                                <label for="example-hf-dataN">Data Inicio
                                    
                                </label>
                                <input type="text" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" >
                        </div>
                        <div class="col-md-2">
                                <label for="example-hf-dataN">Data Fim
                                    
                                </label>
                                <input type="text" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" >
                        </div>
                        <div class="col-md-2">
                                <br>
                                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Pesquisar">
                        </div>
                        
        
                    </div>
                    
                </form>

                <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/gerarRelatorioDetalhado')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left" target="blank">
                    
                    <div class="row">
                        <input type="hidden" id="val-username" name="protocolo" class="form-control" placeholder="Digite o protocolo" value="<?=$protocolo?>" >
                        <input type="hidden" id="val-username" name="numero" class="form-control" placeholder="Digite o número do telefone" value="<?=$numero?>" >
                        <input type="hidden" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" required="required">
                        <input type="hidden" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" required="required">
                        <div class="col-md-2">
                                <br>
                                <input type="submit" name="gerarRelatorioDetalhado" class="btn btn-effect-ripple btn-primary" value="Gerar Relatório">
                        </div>
                    </div>
                    
                </form>
                <br clear="all">
                </div>
                <!-- END Horizontal Form Content -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                            <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            <th>Protocolo</th>
                                            <th>Nome</th>
                                            <th>Numero</th>
                                            <th>Status</th>
                                            <th>Departamento</th>
                                            <th>Operador</th>
                                            <th>Entregador</th>
                                            <!-- <th>Fila de Espera</th> -->
                                            <th>Início</th>
                                            <th>Término</th>
                                            <th>Duração</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($list as $l) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=$l->protocolo?></td>
                                            <td><?=$l->nome?></td>
                                            <td><?=str_replace('@c.us', '', $l->receive)?></td>                                            
                                            <td><?=$l->status?></td>
                                            <td><?=$l->titulo?></td>
                                            <td><?=$l->operador?></td>
                                            <td><?=$l->id_entregador?></td>
                                            <!-- <td><?=$l->filaEspera?></td> -->
                                            <td><?=Date('d/m/Y H:i:s',strtotime($l->dataatendimento))?></td>
                                            <td><?=Date('d/m/Y H:i:s',strtotime($l->datatermino))?></td>
                                            <td><?=$l->duracao?></td>
                                            
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

<?
//montando grafico
foreach ($meses as $key => $value) {
    $bars .= "{
                y: '$value->Mes',
                a: $value->qtd
            },";
}
$bars = substr($bars, 0,-1);
?>
<script type="text/javascript">
if ($("#bar-example").length > 0) {
    var colors = [
        '#E94B3B',
        '#39C7AA',
        '#1C7EBB',
        '#F98E33',
        '#ad96da'
    ];
    Morris.Bar({
        lineColors: colors,
        element: 'bar-example',
        data: [<?=$bars?>],
        xkey: 'y',
        ykeys: ['a'],
        resize: true,
        labels: ['Interações']
    });
    
}
</script>