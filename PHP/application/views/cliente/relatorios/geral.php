<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-10">
                        <h4 class="page-title">Relatórios - Geral</h4>
                    </div>
                    
                </div>
                
                <div class="card-box">
                <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/geral')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left">
                    
                    <div class="row">
                        <div class="col-md-6">
                                <label for="example-hf-nome">Pesquisa
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" id="val-username" name="texto" class="form-control" placeholder="Digite sua pesquisa" value="<?=$texto?>" required="required">
                            
                        </div>
                        <div class="col-md-2">
                                <label for="example-hf-dataN">Data Inicio
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" required="required">
                        </div>
                        <div class="col-md-2">
                                <label for="example-hf-dataN">Data Fim
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" required="required">
                        </div>
                        <div class="col-md-2">
                                <br>
                                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Pesquisar">
                        </div>
                        
        
                    </div>
                    
                </form>

                <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/gerarcontatos')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' style="float: left" target="blank">
                    
                    <div class="row">
                        <input type="hidden" id="val-username" name="texto" class="form-control" placeholder="Digite sua pesquisa" value="<?=$texto?>" required="required">
                        <input type="hidden" id="example-datepicker" name="datainicio" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Inicio" value="<?=$datainicio?>" required="required">
                        <input type="hidden" id="example-datepicker" name="datafim" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data Fim" value="<?=$datafim?>" required="required">
                        <div class="col-md-2">
                                <br>
                                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Gerar Contatos">
                        </div>
                    </div>
                    
                </form>
                <br clear="all">
                </div>
                <!-- END Horizontal Form Content -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                                <?
                                if(count($messages)>0){
                                ?>
                                
                                <div class="content container-fluid">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="dash-widget dash-widget5">
                                                <span class="dash-widget-icon bg-info"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                                <div class="dash-widget-info">
                                                    <h3><?=count(array_unique($receives))?></h3>
                                                    <span>Contatos</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-lg-6">
                                            <div class="dash-widget dash-widget5">
                                                <span class="dash-widget-icon bg-info   "><i class="fa fa-tasks" aria-hidden="true"></i></span>
                                                <div class="dash-widget-info">
                                                    <h3><?=count($messages)?></h3>
                                                    <span>Interações</span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card-box">
                                                <div id="bar-example"></div>
                                            </div>
                                        </div>
                                    </div>
                                        
                                </div>
                                <?
                                }else{
                                ?>
                                    <h4 align="center">Por favor preencha os campos acima para obter os indicadores!</h4>
                                <?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

<?
//montando grafico
foreach ($meses as $key => $value) {
    $bars .= "{
                y: '$value->Mes',
                a: $value->qtd
            },";
}
$bars = substr($bars, 0,-1);
?>
<script type="text/javascript">
if ($("#bar-example").length > 0) {
    var colors = [
        '#E94B3B',
        '#39C7AA',
        '#1C7EBB',
        '#F98E33',
        '#ad96da'
    ];
    Morris.Bar({
        lineColors: colors,
        element: 'bar-example',
        data: [<?=$bars?>],
        xkey: 'y',
        ykeys: ['a'],
        resize: true,
        labels: ['Interações']
    });
    
}
</script>