<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
if(isset($obj)){ 
	$id                    = $obj->id;
    $titulo                  = $obj->titulo;
    $comando                  = $obj->comando;
    $sigla                  = $obj->sigla;
    
    $btnsalvar = 'Atualizar';
}else{
    $id                    = '';
    $titulo                  = '';
    $comando                  = '';
    $sigla                  = '';
    $btnsalvar = 'Salvar';

}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Departamentos</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                     
            <div class="row">
                <div class="col-md-8">
                        <label>Titulo</label>
                        <input type="text" id="example-titulo" name="titulo" class="form-control" placeholder="Titulo" value="<?=$titulo?>">
                    
                </div>
                <div class="col-md-2">
                        <label>Sigla</label>
                        <input type="text" id="example-titulo" name="sigla" class="form-control" placeholder="Sigla" value="<?=$sigla?>">
                    
                </div>
                <div class="col-md-2">
                        <label>Comando</label>
                        <input type="text" id="example-titulo" name="comando" class="form-control" placeholder="Comando" value="<?=$comando?>">
                    
                </div>

            </div>
            <br clear="all">
            
            <br clear="all">
            
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
