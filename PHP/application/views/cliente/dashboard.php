<?php  
$this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
$this->load->library('session');
$this->load->model('clientesmodel');
?>


<div class="content container-fluid">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <!-- <span class="dash-widget-icon bg-info"><i class="fa fa-user-o" aria-hidden="true"></i></span> -->
                <span class="dash-widget-icon bg-info"><img src="<?=a_assets('img/contatos.png')?>" width="90px" height = "88px"></span>
                <div class="dash-widget-info">
                    <h3><?=($fila)?></h3>
                    <span>Contatos</span>
                </div>
                
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-info"><img src="<?=a_assets('img/interacoes.png')?>" width="90px" height = "88px"></i></span>
                <div class="dash-widget-info">
                    <h3><?=($messages)?></h3>
                    <span>Interações</span>
                </div>
                
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-warning"><img src="<?=a_assets('img/aguardando.png')?>"width="90px" height = "88px"></i></span>
                <div class="dash-widget-info">
                    <h3><?=$aguardando?></h3>
                    <span>Aguardando</span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-success"><img src="<?=a_assets('img/finalizados.png')?>" width="90px" height = "88px"></i></span>
                <div class="dash-widget-info">
                    <h3><?=$finalizados?></h3>
                    <span>Finalizados</span>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-info"><img src="<?=a_assets('img/atendendo.png')?>" width="90px" height = "88px"></i></span>
                <div class="dash-widget-info">
                    <h3><?=$atendendo?></h3>
                    <span>Atendendo</span>
                </div>                
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-info"><img src="<?=a_assets('img/atendimentos.png')?>" width="90px" height = "88px"></i></span>
                <div class="dash-widget-info">
                    <h3><?=($atendimentos)?></h3>
                    <span>Atendimentos</span>
                </div>                
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="dash-widget dash-widget5">
                <span class="dash-widget-icon bg-info"><img src="<?=a_assets('img/notificacoes.png')?>" width="90px" height = "88px"></i></span>
                <div class="dash-widget-info">
                    <h3><?=$notificacoesEnviadas?></h3>
                    <span>Notificações</span>
                </div>                
            </div>
        </div>

        <a href="https://vcard.ads.ltda/cliente" target="_blank" onclick="<?$this->clientesmodel->updateClicou($this->session->userdata('loggedincliente'), $clicou->clickVCard)?>">
            <div class="col-md-6 col-sm-6 col-lg-3">
                <div class="dash-widget dash-widget5">
                    <span class="dash-widget-icon bg-info"><img src="<?=a_assets('img/leads_vcard.png')?>" width="90px" height = "88px"></i></span>
                    <div class="dash-widget-info">
                        <h3><?=$clicou->clickVCard?></h3>
                        <span>Leads vCard</span>
                    </div>                
                </div>
            </div>
        </a>

    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>Interações Mensais</h4>
        </div>    
        <div class="col-md-12">
            <div class="card-box">
                <div id="bar-example"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
        <div class="col-md-12">
            <h4>Departamentos mais acessados</h4>
        </div>    
        <div class="col-md-12">
            <div class="card-box">
                <div id="bar-example2"></div>
            </div>
        </div>
        </div>
    
        <div class="col-md-6">
        <div class="col-md-12">
            <h4>Atendentes mais ativos</h4>
        </div>    
        <div class="col-md-12">
            <div class="card-box">
                <div id="bar-example3"></div>
            </div>
        </div>
        </div>
    </div>        
</div>


<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
<?
//montando grafico
foreach ($meses as $key => $value) {
    $bars .= "{
                y: '$value->Mes',
                a: $value->qtd
            },";
}
$bars = substr($bars, 0,-1);
//montando grafico
foreach ($depmaisacessado as $key => $value) {
    $bars2 .= "{
                y: '$value->departamento',
                a: $value->qtd
            },";
}
$bars2 = substr($bars2, 0,-1);

//montando grafico
foreach ($atendentesmaisativos as $key => $value) {
    $bars3 .= "{
                y: '$value->atendente',
                a: $value->qtd
            },";
}
$bars3 = substr($bars3, 0,-1);
?>
<script type="text/javascript">
if ($("#bar-example").length > 0) {
    var colors = [
        '#E94B3B',
        '#39C7AA',
        '#1C7EBB',
        '#F98E33',
        '#ad96da'
    ];
    Morris.Bar({
        lineColors: colors,
        element: 'bar-example',
        data: [<?=$bars?>],
        xkey: 'y',
        ykeys: ['a'],
        resize: true,
        labels: ['Interações']
    });
    
}

if ($("#bar-example2").length > 0) {
    var colors = [
        '#E94B3B',
        '#39C7AA',
        '#1C7EBB',
        '#F98E33',
        '#ad96da'
    ];
    Morris.Bar({
        lineColors: colors,
        element: 'bar-example2',
        data: [<?=$bars2?>],
        xkey: 'y',
        ykeys: ['a'],
        resize: true,
        labels: ['']
    });
    
}

if ($("#bar-example3").length > 0) {
    var colors = [
        '#E94B3B',
        '#39C7AA',
        '#1C7EBB',
        '#F98E33',
        '#ad96da'
    ];
    Morris.Bar({
        lineColors: colors,
        element: 'bar-example3',
        data: [<?=$bars3?>],
        xkey: 'y',
        ykeys: ['a'],
        resize: true,
        labels: ['']
    });
    
}
</script>