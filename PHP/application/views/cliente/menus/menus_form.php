<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Menu Empresa</h4>
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Nome do Contato = %nome%</span>             
            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Nome do Operador = %operador%</span>
            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Protocolo = %protocolo%</span>
            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Saudação = %DTN%</span>
            <span class="label label-success-border" style="padding: 5px;margin-bottom: 5px;">Departamento = %nomedepartamento%</span>
            <?
            if(count($menuslist)>0){ 
                foreach ($menuslist as $key => $value) {
                    if($menus[$key+1]<>''){
                ?>
                <div class="row">
                    <div class="col-md-12">
                            <label for="example-hf-nome"><?=$menus[$key+1]?>
                                <span class="text-danger">*</span>
                            </label>
                            <br clear="all">
                            <input type="hidden" name="id[]" value="<?=$value->id?>">
                            <input type="hidden" name="id_menu[]" value="<?=$value->id_menu?>">
                            <textarea rows="3" cols="5" class="form-control" name="resposta[]" placeholder="Descreva sua resposta"><?=$value->resposta?></textarea>
                   </div>
                </div>
                <br clear="all">
                <?    
                }
                }    
            }else{
                foreach ($menus as $key => $value) {
                ?>
                <div class="row">
                    <div class="col-md-12">
                            <label for="example-hf-nome"><?=$value?>
                                <span class="text-danger">*</span>
                            </label>
                            <br clear="all">
                            <input type="hidden" name="id[]" value="">
                            <input type="hidden" name="id_menu[]" value="<?=$key?>">
                            <textarea rows="3" cols="5" class="form-control" name="resposta[]" placeholder="Descreva sua resposta"><?=$respostas?></textarea>
                   </div>
                </div>
                <br clear="all">
                <?    
                }    
            }
            
            ?>
             
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Salvar">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
