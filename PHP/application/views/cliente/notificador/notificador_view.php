<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-8">
                        <h4 class="page-title">Notificador: <?=$mensagem->titulo?></h4>
                    </div>
                    <div class="col-xs-4">
                        <a href="<?=site_url($this->session->userdata('folder').$this->router->class)?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                        
                        <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/adicionar')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Adicionar Novo</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                                <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            
                                            <th>Nome</th>
                                            <th>Telefone</th>
                                            <th>Status</th>
                                            <th width="10%">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($list as $l) {
                                        ?>
                                        <tr>
                                            
                                            <td><?=$l->nome?></td>
                                            <td><?=$l->telefone?></td>
                                            <td><?=($l->status==1?'Enviado':'Não Enviado')?></td>
                                            <td class="text-center">

                                                
                                                <a href="javascript://" onclick="apagarmodal('<?=site_url('cliente/')?><?=$this->router->class?>','apagar','<?=$l->nome?>',<?=$l->id?>)" title="" class="btn btn-effect-ripple btn-sm btn-danger" style="overflow: hidden; position: relative;" data-original-title="Apagar"><i class="fa fa-times"></i></a>       
                                                                       
                                           
                                         </td><!--data-toggle="modal" data-target="#myModal"-->
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

