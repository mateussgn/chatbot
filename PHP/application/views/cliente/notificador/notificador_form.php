<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
if(isset($obj)){ 
    $id                    = $obj->id;
    $titulo                  = $obj->titulo;
    $texto                  = $obj->texto;
    $foto                  = $obj->foto;
    
    $btnsalvar = 'Atualizar';
}else{
    $id                    = '';
    $titulo                  = '';
    $texto                  = '';
    $foto                  = '';
    $btnsalvar = 'Salvar';

}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Notificador</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' enctype="multipart/form-data">

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                
                <div class="row">
                    <div class="col-md-12">
                        <label>Mensagem</label>
                        <select name="id_mensagem" class="form-control" >
                            <option value="">selecione</option>
                            <?
                            foreach ($mensagens as $key => $value) {
                            ?>
                                <option value="<?=$value->id?>"><?=$value->titulo?></option>
                            <?
                            }
                            ?>
                            
                        </select>
                        
                    </div>
                </div>
                <br clear="all">
                
                <div class="row">
                    <div class="col-md-6">
                        <label>Arquivo CSV</label>
                        <input type="file" id="example-titulo" name="file" class="form-control" placeholder="Ex.: Menu" value="">
                        <span><a href="<?=site_url('uploads/csv/envios.csv')?>">Modelo baixe aqui</a></span>
                    </div>
                    
                </div>


                <br clear="all">
               
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="Enviar">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
