<?php 
 $modulos = $this->biblioteca_func_franquia->configuracoesModulares_franquia();
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div id="page-content">
    <!-- Table Styles Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-10">
                <div class="header-section">
                    <h1>Clientes</h1>
                </div>
            </div>
            
            <div class="col-sm-2">
                <div class="header-section">
                    <!-- <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/adicionar')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Adicionar Novo</a>
 -->
                </div>
            </div>
        </div>
    </div>
    <!-- END Table Styles Header -->
    <?php show_alert(); ?> 
     
    <div class="block full">
        <div class="block-title">
            <h2>Listar</h2>
             
        </div>
        <div class="table-responsive">
            <table id="example-datatable" class="table table-condensed table-bordered table-vcenter">
                <thead>
                    <tr>
                        <!-- <th class="text-center" style="width: 50px;">ID</th> -->
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Telefone Alt.</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    foreach ($list as $l) {
                    ?>
                    <tr>
                        <!-- <td class="text-center"><?=$l->id?></td> -->
                        <td><?=$l->nome?></td>
                        <td><?=$l->email?></td>
                        <td><?=$l->telefone_preferencial?></td>
                        <td><?=$l->telefone_alternativo?></td>
                        <td class="text-center">

                            <a href="<?=site_url($this->session->userdata('folder').'eventos/adicionar/'.$l->id)?>" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-sm btn-primary" style="overflow: hidden; position: relative;" data-original-title="ir para Evento"><i class="fi fi-pdf"></i></a>
                       
                                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/editar/'.$l->id)?>" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-sm btn-success" style="overflow: hidden; position: relative;" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                       
                            <a href="javascript://" onclick="apagarmodal('<?=$this->router->class?>','apagar','<?=$l->nome?>',<?=$l->id?>)" title="" class="btn btn-effect-ripple btn-sm btn-danger" style="overflow: hidden; position: relative;" data-original-title="Apagar"><i class="fa fa-times"></i></a>                                
                       
                     </td><!--data-toggle="modal" data-target="#myModal"-->
                    </tr>
                    <?
                    }
                    ?>
                </tbody>
            </table>
        </div>                
    </div>                
</div>
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>

