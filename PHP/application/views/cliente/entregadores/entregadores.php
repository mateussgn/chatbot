<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
 ?>

<div class="content container-fluid">
                <?php show_alert(); ?>  
                
                <div class="row">
                    <div class="col-xs-10">
                        <h4 class="page-title">Entregadores</h4>
                    </div>
                    <div class="col-xs-2">
                        <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/adicionar')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i> Adicionar Novo</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                                <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            <th width="7%">ID</th>
                                            <th width="7%">Status</th>
                                            <th>Nome</th>
                                            <th>Telefone</th>
                                            <th>Nota 1</th>
                                            <th>Nota 2</th>
                                            <th>Nota 3</th>
                                            <th>Nota 4</th>
                                            <th>Nota 5</th>
                                            
                                            <th>Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($list as $l) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=$l->id?></td>
                                            <td class="text-center"><?=$l->status==1?'Online':'Offline'?></td>
                                            <td><?=$l->nome?></td>
                                            <td><?=$l->number?></td>
                                            <td><?=$l->nota1?></td>
                                            <td><?=$l->nota2?></td>
                                            <td><?=$l->nota3?></td>
                                            <td><?=$l->nota4?></td>
                                            <td><?=$l->nota5?></td>
                                            <td class="text-center">

                                                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/editar/'.$l->id)?>" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-sm btn-success" style="overflow: hidden; position: relative;" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                           
                                                <a href="javascript://" onclick="apagarmodal('<?=$this->router->class?>','apagar','<?=$l->nome?>',<?=$l->id?>)" title="" class="btn btn-effect-ripple btn-sm btn-danger" style="overflow: hidden; position: relative;" data-original-title="Apagar"><i class="fa fa-times"></i></a>                                
                                           
                                         </td><!--data-toggle="modal" data-target="#myModal"-->
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    
    
<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>