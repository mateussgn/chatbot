<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
if(isset($obj)){ 
    $id                    = $obj->id;
    $nome                  = $obj->nome;
    $nomeCompleto          = $obj->nomeCompleto;
    $telefone              = $obj->telefone;
    $email                 = $obj->email;
    $rua                   = $obj->rua;
    $bairro                = $obj->bairro;
    $cidade                = $obj->cidade;
    $estado                = $obj->estado;
    $complemento           = $obj->complemento;
    $pontoDeReferencia     = $obj->pontoDeReferencia;
    $numero                = $obj->numero;
    $cep                   = $obj->cep;
    $cpf                   = $obj->cpf;
    $id_departamento       = explode(',', $obj->id_departamento);
    $facebook              = $obj->facebook;
    $instagram             = $obj->instagram;
    $dias                  = explode(',', $obj->dias);
    $horainicioseg         = $obj->horainicioseg;
    $horainicioter         = $obj->horainicioter;
    $horainicioqua         = $obj->horainicioqua;
    $horainicioqui         = $obj->horainicioqui;
    $horainiciosex         = $obj->horainiciosex;
    $horainiciosab         = $obj->horainiciosab;
    $horainiciodom         = $obj->horainiciodom;
    $horafimseg            = $obj->horafimseg;
    $horafimter            = $obj->horafimter;
    $horafimqua            = $obj->horafimqua;
    $horafimqui            = $obj->horafimqui;
    $horafimsex            = $obj->horafimsex;
    $horafimsab            = $obj->horafimsab;
    $horafimdom            = $obj->horafimdom;
    $id_cliente            = $obj->id_cliente;
    $status                = $obj->status;
    $observacoes           = $obj->observacoes;

    $arrDataNasc           = explode("-", $obj->dataNascimento);
    $dataNasc              = $arrDataNasc[2]."/".$arrDataNasc[1]."/".$arrDataNasc[0];
    $dataNascimento        = $dataNasc;

    $btnsalvar = 'Salvar';
}else{
    $id                    = '';
    $nome                  = '';
    $nomeCompleto          = '';
    $telefone              = '';
    $email                 = '';
    $rua                   = '';
    $bairro                = '';
    $cidade                = '';
    $estado                = '';
    $complemento           = '';
    $pontoDeReferencia     = '';
    $numero                = '';
    $cep                   = '';
    $cpf                   = '';
    $id_departamento       = '';
    $facebook              = '';
    $instagram             = '';
    $dias                  = '';
    $horainicioseg         = '';
    $horainicioter         = '';
    $horainicioqua         = '';
    $horainicioqui         = '';
    $horainiciosex         = '';
    $horainiciosab         = '';
    $horainiciodom         = '';
    $horafimseg            = '';
    $horafimter            = '';
    $horafimqua            = '';
    $horafimqui            = '';
    $horafimsex            = '';
    $horafimsab            = '';
    $horafimdom            = '';
    $id_cliente            = '';
    $status                = '';
    $dataNascimento        = '';
    $observacoes           = '';

    $btnsalvar = 'Salvar';
}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Entregadores</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                     
            <h4 class="card-title">Dados Pessoais</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CPF
                        </label>
                        <input type="text" id="example-titulo" name="cpf" class="form-control cpf" placeholder="CPF" value="<?=$cpf?>">
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Primeiro Nome
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Nome" value="<?=$nome?>" required="required">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Nome Completo
                        </label>
                        <input type="text" id="val-username" name="nomeCompleto" class="form-control" placeholder="Nome Completo" value="<?=$nomeCompleto?>">
                </div>

            </div>
            <br clear="all">
            
            <h4 class="card-title">Dados de Localização</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CEP (12345-123)
                        </label>
                        <input type="text" id="cep" name="cep" class="form-control cep" placeholder="CEP" value="<?=$cep?>">
                    
                </div>


                <div class="col-md-6">
                        <label>Logradouro
                        </label>
                        <input type="text" id="rua" name="rua" class="form-control" placeholder="Logradouro" value="<?=$rua?>">
                </div>

                <div class="col-md-2">
                        <label>Número
                        </label>
                        <input type="text" id="numero" name="numero" class="form-control" placeholder="Número" value="<?=$numero?>">
                </div>
                

                <div class="col-md-2">
                        <label>Complemento</label>
                        <input type="text" id="" name="complemento" class="form-control" placeholder="Complemento" value="<?=$complemento?>">
                </div>

                <br clear="all"><br clear="all">

                <div class="col-md-2">
                        <label>Bairro
                        </label>
                        <input type="text" id="bairro" name="bairro" class="form-control" placeholder="Bairro" value="<?=$bairro?>">
                </div>


                <div class="col-md-2">
                        <label>Cidade
                        </label>
                        <input type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade" value="<?=$cidade?>">
                </div>

                <div class="col-md-2">
                        <label>Estado
                        </label>
                        <input type="text" id="estado" name="estado" class="form-control" placeholder="Estado" value="<?=$estado?>">
                </div>

                <div class="col-md-6">
                        <label>Ponto de Referencia
                        </label>
                        <input type="text" id="pontoDeReferencia" name="pontoDeReferencia" class="form-control" placeholder="Ponto De Referencia" value="<?=$pontoDeReferencia?>">
                </div>

            </div>
            <br clear="all">

            <h4 class="card-title">Dados de Contato/Acesso</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>Telefone
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="telefone" name="telefone" class="form-control" placeholder="Telefone" value="<?=$telefone?>" required="required">
                </div>

                <div class="col-md-4">
                        <label>Email
                        </label>
                        <input type="text" id="example-titulo" name="email" class="form-control" placeholder="Email" value="<?=$email?>">
                </div>

                <div class="col-md-3">
                        <label>Facebook
                        </label>
                        <input type="text" id="facebook" name="facebook" class="form-control" placeholder="Facebook" value="<?=$facebook?>">
                </div>

                <div class="col-md-3">
                        <label>Instagram
                        </label>
                        <input type="text" id="instagram" name="instagram" class="form-control" placeholder="Instagram" value="<?=$instagram?>">
                </div>

                <br clear="all"><br clear="all">

                <div class="col-md-12">
                        <label>Observacoes do Entregador
                        </label>
                        <input type="text" id="example-titulo" name="observacoes" class="form-control" placeholder="Observacoes" value="<?=$observacoes?>">
                </div>

            </div>
            <br clear="all">
            <h4 class="card-title">Acesso ao Atendimento</h4>
            <div class="row">
                <div class="col-md-5">
                    <label>Departamentos</label>
                    <br clear="all">
                    <!-- <select name="id_departamento[]" class="form-control" multiple="multiple"> -->
                        <?
                        foreach ($departamentos as $key => $value) {
                            if(in_array($value->id, $id_departamento)){
                                $sel = 'selected="selected"';
                            }else{
                                $sel = '';
                            }
                        ?>
                            <!-- <option <?=$sel?> value="<?=$value->id?>"><?=$value->titulo?></option> -->
                            <input type="checkbox" name="id_departamento[]" class="" value="<?=$value->id?>" <?=(in_array($value->id, $id_departamento)?'checked="checked"':'')?>> <?=$value->titulo?>
                        <?
                        }
                        ?>
                    <!-- </select> -->
                </div>
                
                <div class="col-md-7">
                        <label>Dias da Semana</label>
                        <br clear="all">
                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Seg" <?=(in_array('Seg', $dias)?'checked="checked"':'')?>> Segunda
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainicioseg" name="horainicioseg" class="form-control hora" placeholder="00:00" value="<?=$horainicioseg?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimseg" name="horafimseg" class="form-control hora" placeholder="00:00" value="<?=$horafimseg?>">
                        </div>
                        <br clear="all">
                        <br clear="all">

                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Ter" <?=(in_array('Ter', $dias)?'checked="checked"':'')?>> Terca
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainicioter" name="horainicioter" class="form-control hora" placeholder="00:00" value="<?=$horainicioter?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimter" name="horafimter" class="form-control hora" placeholder="00:00" value="<?=$horafimter?>">
                        </div>
                        <br clear="all">
                        <br clear="all">

                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Qua" <?=(in_array('Qua', $dias)?'checked="checked"':'')?>> Quarta
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainicioqua" name="horainicioqua" class="form-control hora" placeholder="00:00" value="<?=$horainicioqua?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimqua" name="horafimqua" class="form-control hora" placeholder="00:00" value="<?=$horafimqua?>">
                        </div>
                        <br clear="all">
                        <br clear="all">

                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Qui" <?=(in_array('Qui', $dias)?'checked="checked"':'')?>> Quinta
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainicioqui" name="horainicioqui" class="form-control hora" placeholder="00:00" value="<?=$horainicioqui?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimqui" name="horafimqui" class="form-control hora" placeholder="00:00" value="<?=$horafimqui?>">
                        </div>
                        <br clear="all">
                        <br clear="all">

                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Sex" <?=(in_array('Sex', $dias)?'checked="checked"':'')?>> Sexta
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainiciosex" name="horainiciosex" class="form-control hora" placeholder="00:00" value="<?=$horainiciosex?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimsex" name="horafimsex" class="form-control hora" placeholder="00:00" value="<?=$horafimsex?>">
                        </div>
                        <br clear="all">
                        <br clear="all">
                        
                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Sab" <?=(in_array('Sab', $dias)?'checked="checked"':'')?>> Sabado
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainiciosab" name="horainiciosab" class="form-control hora" placeholder="00:00" value="<?=$horainiciosab?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimsab" name="horafimsab" class="form-control hora" placeholder="00:00" value="<?=$horafimsab?>">
                        </div>
                        <br clear="all">
                        <br clear="all">
                        
                        <div class="col-md-3">
                            <input type="checkbox" name="dias[]" class="" value="Dom" <?=(in_array('Dom', $dias)?'checked="checked"':'')?>> Domingo
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horainiciodom" name="horainiciodom" class="form-control hora" placeholder="00:00" value="<?=$horainiciodom?>">
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="horafimdom" name="horafimdom" class="form-control hora" placeholder="00:00" value="<?=$horafimdom?>">
                        </div>
                </div>                
            </div>

            <br clear="all">

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
