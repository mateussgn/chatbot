<?php
$this->load->view($this->session->userdata('folder') . 'add-on/header', $modulos);
?>
<?php
if (isset($obj)) {
    $id                = $obj->id;
    $nome              = $obj->nome;
    $preco_credito     = $obj->preco_credito;
    $preco_debito      = $obj->preco_debito;
    $preco_dinheiro    = $obj->preco_dinheiro;
    $preco_vale        = $obj->preco_vale;
    
    $btnsalvar         = 'Atualizar';

} else {
    $id                = '';
    $nome              = '';
    $preco_credito     = '';
    $preco_debito      = '';
    $preco_dinheiro    = '';
    $preco_vale        = '';
    
    $btnsalvar         = 'Salvar';
}

?>
<div class="content container-fluid">
    <?php
show_alert();
?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Bairros</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?= site_url($this->session->userdata('folder') . $this->router->class . '') ?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php
echo site_url($this->session->userdata('folder') . $this->router->class . '/insert');
?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<?
echo $id;
?>">
                    
                     
            <h4 class="card-title">Dados Cadastrais</h4>
            <div class="row">
                <div class="col-md-5">
                        <label for="example-hf-nome">Bairro
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Nome" value="<?= $nome ?>" required="required">
                    
                </div>

                <div class="col-md-2">
                        <label for="example-hf-nome">Preco a dinheiro
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="preco_dinheiro" class="form-control" placeholder="Preco" value="<?= $preco_dinheiro ?>" required="required">
                        <label for="example-hf-nome">Preco no debito
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="preco_debito" class="form-control" placeholder="Preco" value="<?= $preco_debito ?>" required="required">
                        <label for="example-hf-nome">Preco no credito
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="preco_credito" class="form-control" placeholder="Preco" value="<?= $preco_credito ?>" required="required">
                        <label for="example-hf-nome">Preco vale
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="preco_vale" class="form-control" placeholder="Preco" value="<?= $preco_vale ?>" required="required">
                    
                </div>

            </div>
            <br clear="all">
            
            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?= $btnsalvar ?>">
            </div>
    </div>
</div>

</div>

<?php
$this->load->view($this->session->userdata('folder') . 'add-on/footer');
?>