<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
if(isset($obj)){ 
	$id                    = $obj->id;
    $nome                  = $obj->nome;
    $email                 = $obj->email;
    $login                  = $obj->login;

    $btnsalvar = 'Atualizar';
}else{
    $id                    = '';
    $nome                  = '';
    $email                 = '';
    $loign              = '';
    $btnsalvar = 'Salvar';

}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Usuário</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                     
            <div class="row">
                <div class="col-md-6">
                        <label for="example-hf-nome">Nome Completo
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Nome" value="<?=$nome?>" required="required">
                    
                </div>

                <div class="col-md-6">
                        <label for="example-hf-nome">Email
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="email" class="form-control" placeholder="E-mail" value="<?=$email?>" required="required">
                </div>

            </div>
            <br clear="all">
            <div class="row">
                <div class="col-md-6">
                        <label>Usuário
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="login" class="form-control" placeholder="Usuário" value="<?=$login?>">
                </div>
                <div class="col-md-6">
                        <label>Senha
                               <span class="text-danger">*</span>
                        </label>
                        <input type="password" id="example-titulo" name="senha" class="form-control" placeholder="Senha" value="">
                </div>

            </div>



            <br clear="all">
            
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
