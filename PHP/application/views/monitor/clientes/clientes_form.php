<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
if(isset($obj)){ 
	$id                    = $obj->id;
    $nome                  = $obj->nome;
    $email                 = $obj->email;
    $mae                 = $obj->mae;
    $endereco              = $obj->endereco;
    $cep                   = $obj->cep;
    $cidade                = $obj->cidade;
    $estado                = $obj->estado;
    $bairro                = $obj->bairro;
    $numero                = $obj->numero;
    $razao                  = $obj->razao;
    $usuario                  = $obj->usuario;
    $dialogflow                  = $obj->dialogflow;
    $frases                  = $obj->frases;

    $telefone = $obj->telefone;
    $complemento           = $obj->complemento;
    
    $rg_ie                 = $obj->rg_ie;
    $cnpj              = $obj->cnpj;
    $sexo                  = $obj->sexo;
    $arrDataNasc = explode("-", $obj->dataNascimento);
    $dataNasc    = $arrDataNasc[2]."/".$arrDataNasc[1]."/".$arrDataNasc[0];
    $dataNascimento        = $dataNasc;
    
   

    $btnsalvar = 'Atualizar';
}else{
    $id                    = '';
    $nome                  = '';
    $email                 = '';
    $mae                 = '';
    $endereco              = '';
    $cep                   = '';
    $cidade                = '';
    $estado                = '';
    $bairro                = '';
    $numero                = '';
    $razao                  = '';
    $usuario                  = '';
    $dialogflow                  = '';
    $frases                  = '';

    $telefone = '';
    $complemento           = '';
    
    $rg_ie                 = '';
    $cnpj              = '';
    $sexo                  = '';
    $dataNascimento        = '';
    
    $btnsalvar = 'Salvar';

}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Clientes</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                    
                     
            <h4 class="card-title">Dados Pessoais</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>Registro</label>
                        <input type="text" id="example-titulo" name="rg_ie" class="form-control" placeholder="RG ou IE" value="<?=$rg_ie?>">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Nome Completo
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Nome" value="<?=$nome?>" required="required">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Nome da Mãe
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="mae" class="form-control" placeholder="Mãe" value="<?=$mae?>" required="required">
                </div>

            </div>
            <br clear="all">
            <div class="row">
                <div class="col-md-3">
                        <label for="example-hf-dataN">Data de Nascimento
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-datepicker" name="dataNascimento" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data de Nascimento" value="<?=$dataNascimento?>" required="required">
                </div>

                 <div class="form-group col-md-2">
                    <div class="col-md-12">
                        <label>Sexo</label>
                        <select name="sexo" id="sexo" class="form-control">
                            <option value=""></option>
                            <option value="m" <?echo ($sexo == "m") ? "selected":""?>>Masculino</option>
                            <option value="f" <?echo ($sexo == "f") ? "selected":""?>>Feminino</option>
                        </select>    
                    </div>
                </div>
            </div>

            <h4 class="card-title">Dados de Localização</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CEP (12345-123)
                        </label>
                        <input type="text" id="cep" name="cep" class="form-control cep" placeholder="CEP" value="<?=$cep?>">
                    
                </div>


                <div class="col-md-6">
                        <label>Logradouro
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="rua" name="endereco" class="form-control" placeholder="Logradouro" value="<?=$endereco?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Número
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="numero" name="numero" class="form-control" placeholder="Número" value="<?=$numero?>" required="required">
                </div>
                

                <div class="col-md-2">
                        <label>Complemento</label>
                        <input type="text" id="" name="complemento" class="form-control" placeholder="Complemento" value="<?=$complemento?>">
                </div>

                <br clear="all"><br clear="all">

                <div class="col-md-2">
                        <label>Bairro
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="bairro" name="bairro" class="form-control" placeholder="Bairro" value="<?=$bairro?>" required="required">
                </div>


                <div class="col-md-2">
                        <label>Cidade
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade" value="<?=$cidade?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Estado
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="estado" name="estado" class="form-control" placeholder="Cidade" value="<?=$estado?>" required="required">
                </div>

            </div>
            <br clear="all">

            <h4 class="card-title">Dados Empresariais</h4>
            <div class="row">
                <div class="col-md-3">
                        <label>CNPJ
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="cnpj" class="form-control cnpj" placeholder="CNPJ" value="<?=$cnpj?>" required="required">
                </div>

                <div class="col-md-4">
                        <label for="example-hf-nome">Razão Social
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="razao" class="form-control" placeholder="Razão Social" value="<?=$razao?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Telefone
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="fonep" name="telefone" class="form-control fone" placeholder="Telefone" value="<?=$telefone?>" required="required">
                </div>

                <div class="col-md-3">
                        <label>Email
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="email" class="form-control" placeholder="Email" value="<?=$email?>" required="required">
                </div>
                <br clear="all"><br clear="all">
                <div class="col-md-3">
                        <label>Usuário
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="usuario" class="form-control" placeholder="Usuário" value="<?=$usuario?>">
                </div>
                <div class="col-md-3">
                        <label>Senha
                               <span class="text-danger">*</span>
                        </label>
                        <input type="password" id="example-titulo" name="senha" class="form-control" placeholder="Senha" value="">
                </div>

            </div>

            <br clear="all">

            <h4 class="card-title">Configuração</h4>
            <div class="row">
                <div class="col-md-9">
                        <label>DialogFlow (Token Developer)
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="dialogflow" class="form-control" placeholder="Dialogflow" value="<?=$dialogflow?>">
                </div>

               
                <div class="col-md-3">
                        <label>Frases de Treinamento</label>
                        <br clear="all">
                        <input type="checkbox" id="example-titulo" name="frases" class="" value="1" data-toggle="toggle" <?=($frases==1?'checked=checked':'')?>>

                </div>

            </div>
            <br clear="all">
            
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
