<?php 
    // $senha = sha1('admin');
    // echo $senha;
 ?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo head_title()?></title>

        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="robots" content="">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?=a_img('favicon.png')?>">
        <link rel="apple-touch-icon" href="<?=a_img('icon57.png')?>" sizes="57x57">
        <link rel="apple-touch-icon" href="<?=a_img('icon72.png')?>" sizes="72x72">
        <link rel="apple-touch-icon" href="<?=a_img('icon76.png')?>" sizes="76x76">
        <link rel="apple-touch-icon" href="<?=a_img('icon114.png')?>" sizes="114x114">
        <link rel="apple-touch-icon" href="<?=a_img('icon120.png')?>" sizes="120x120">
        <link rel="apple-touch-icon" href="<?=a_img('icon144.png')?>" sizes="144x144">
        <link rel="apple-touch-icon" href="<?=a_img('icon152.png')?>" sizes="152x152">
        <link rel="apple-touch-icon" href="<?=a_img('icon180.png')?>" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?=a_css('bootstrap.min.css')?>">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?=a_css('plugins.css')?>">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?=a_css('main.css')?>">

        <!-- Include a specific file here from themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?=a_css('themes.css')?>">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="<?=a_js('vendor/modernizr-2.8.3.min.js')?>"></script>
    </head>

<body style="background:#FFF;">
        <!-- Full Background -->
        <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
        <!-- <img src="<?=a_img('placeholders/layout/login2_full_bg.jpg')?>" alt="Full Background" class="full-bg animation-pulseSlow"> -->
        <!-- END Full Background -->

        <!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
                <img src="<?=$logomarca_franq?>" width='100%'>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
            <div class="block animation-fadeInQuick">
                <!-- Login Title -->
                <div class="block-title">
                    <h2>Recuperação de Senha</h2>
                </div>
                <!-- END Login Title -->

                <!-- Login Form -->
                <form id="form-login" action="<?php echo site_url('franquia/login/updatsenha')?>" method="post" class="form-horizontal">
                    <input type="hidden" name="id" value="<?=$dados->id?>">
                    <div class="form-group">
                        <label for="login-password" class="col-xs-12">Senha</label>
                        <div class="col-xs-12">
                            <input type="password" id="login-password" name="senha" class="form-control" placeholder="Senha..">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="login-password" class="col-xs-12">Repetir Senha</label>
                        <div class="col-xs-12">
                            <input type="password" id="login-password" name="senha" class="form-control" placeholder="Senha..">
                        </div>
                    </div>
                    <div class="form-group form-actions">
                        <div class="col-xs-8">
                             
                        </div>
                        <div class="col-xs-4 text-right">
                            <button type="submit" class="btn btn-effect-ripple btn-sm btn-success">Salvar</button>
                        </div>
                    </div>
                </form>
                <!-- END Login Form -->

               
            </div>
            <!-- END Login Block -->

            <!-- Footer -->
            <!-- <footer class="text-muted text-center animation-pullUp">
                <small><span id="year-copy"></span> &copy; <a href="http://goo.gl/RcsdAh" target="_blank">AppUI 2.6</a></small>
            </footer> -->
            <!-- END Footer -->



<!-- ############################### Modal ######################################### -->
<!-- ############################### Modal ######################################### -->
<div class="modal fade" id="esqueci-a-senha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        
    <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/#')?>" method="post" class="form-horizontal form-bordered" id='form-validation' enctype="multipart/form-data" target='blank'>
                
             
               <div class="form-group col-md-6">
                    <div class="col-md-12">
                        <label for="example-hf-email">Informe o E-mail Cadastrado
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="email" name="email_cad" class="form-control" placeholder="Informe e-mail cadastrado" value="">

                    </div>
                </div>


    </form>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    </div>
    </div>
  </div>
</div>


<!-- ############################### Modal ######################################### -->
<!-- ############################### Modal ######################################### -->
<!-- ############################### Modal ######################################### -->
        </div>
        <!-- END Login Container -->

        <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="<?=a_js('vendor/jquery-2.2.0.min.js')?>"></script>
        <script src="<?=a_js('vendor/bootstrap.min.js')?>"></script>
        <script src="<?=a_js('plugins.js')?>"></script>
        <script src="<?=a_js('app.js')?>"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="<?=a_js('pages/readyLogin.js')?>"></script>
        <script>$(function(){ ReadyLogin.init(); });</script>
    </body>
</html>

    
  