</div>
    <div class="sidebar-overlay" data-reff=""></div>
    <script type="text/javascript" src="<?=a_assets('js/jquery-3.2.1.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/jquery.slimscroll.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/select2.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/moment.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/bootstrap-datetimepicker.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/bootstrap-tagsinput.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/bootstrap-toggle.min.js')?>"></script>

    <script type="text/javascript" src="<?=a_assets('plugins/morris/morris.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('plugins/raphael/raphael-min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/jquery.dataTables.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/dataTables.bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/app.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/jquerymaskinput.js')?>"></script>
    <script type="text/javascript" src="<?=a_assets('js/scripts.js')?>"></script>
</body>

</html>