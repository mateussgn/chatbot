<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="<?=a_assets('img/favicon.png')?>">
    <title><?php echo head_title()?></title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/font-awesome.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/fullcalendar.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/dataTables.bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/select2.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/bootstrap-datetimepicker.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('plugins/morris/morris.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/style.css')?>">
    <link href="<?=a_assets('css/bootstrap-toggle.min.css')?>" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<!-- Modal confirmar -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span id="msn-titulo-form"></span></h4>
      </div>
      <div class="modal-body">
        <h3 id="msn-titulo-texto"></h3>
        <p id="msn-texto"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
        <button type="button" class="btn btn-primary btn-apagar-sim">Sim</button>
      </div>
    </div>
  </div>
</div>

    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="index.html" class="logo">
                    <img src="<?=a_assets('img/logo.jpg')?>" width="150" alt="">
                </a>
            </div>
            <div class="page-title-box pull-left">
                <!-- <h3>Atendimento Digital</h3> -->
            </div>
            <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
            <ul class="nav navbar-nav navbar-right user-menu pull-right">
                <!-- <li class="dropdown hidden-xs">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge bg-primary pull-right">3</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="media-list">
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">
                                                <img alt="John Doe" src="<?=a_assets('img/user.jpg')?>" class="img-responsive img-circle">
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
                                            <p class="noti-time"><span class="notification-time">4 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">V</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
                                            <p class="noti-time"><span class="notification-time">6 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">L</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
                                            <p class="noti-time"><span class="notification-time">8 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">G</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
                                            <p class="noti-time"><span class="notification-time">12 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">V</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
                                            <p class="noti-time"><span class="notification-time">2 days ago</span></p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="activities.html">View all Notifications</a>
                        </div>
                    </div>
                </li>
                <li class="dropdown hidden-xs">
                    <a href="javascript:;" id="open_msg_box" class="hasnotifications"><i class="fa fa-comment-o"></i> <span class="badge bg-primary pull-right">8</span></a>
                </li> -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle user-link" data-toggle="dropdown" title="Admin">
                        <span class="user-img"><img class="img-circle" src="<?=a_assets('img/user.jpg')?>" width="40" alt="Admin">
                            <span class="status online"></span></span>
                        <span><?=$this->session->userdata('loggednameusuario')?></span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=site_url($this->session->userdata('folder').'usuario/editar/'.$this->session->userdata('loggedin'))?>">Editar Perfil</a></li>
                        <li><a href="<?=site_url($this->session->userdata('folder').'login/logout')?>">Logout</a></li>
                    </ul>
                </li>
            </ul>
            <div class="dropdown mobile-user-menu pull-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?=site_url($this->session->userdata('folder').'usuario/editar/'.$this->session->userdata('loggedin'))?>">Editar Perfil</a></li>
                    <li><a href="<?=site_url($this->session->userdata('folder').'login/logout')?>">Logout</a></li>
                </ul>
            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="active">
                            <a href="<?=site_url($this->session->userdata('folder').'/')?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="<?=site_url($this->session->userdata('folder').'usuario')?>"><i class="fa fa-user" aria-hidden="true"></i> Usuários</a>
                        </li>
                        <li>
                            <a href="<?=site_url($this->session->userdata('folder').'clientes')?>"><i class="fa fa-users" aria-hidden="true"></i> Clientes</a>
                        </li>
                        <li>
                            <a href="<?=site_url($this->session->userdata('folder').'limpar')?>"><i class="fa fa-users" aria-hidden="true"></i> Limpar</a>
                        </li>
                        <!-- <li>
                            <a href="<?=site_url($this->session->userdata('folder').'suporte')?>"><i class="fa fa-ticket" aria-hidden="true"></i> Suporte</a>
                        </li> -->
                        <!-- <li>
                            <a href="<?=site_url($this->session->userdata('folder').'configuracao')?>"><i class="fa fa-cog" aria-hidden="true"></i> Configurações</a>
                        </li> -->
                        
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-wrapper">

        
