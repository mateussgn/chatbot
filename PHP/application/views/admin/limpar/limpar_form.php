<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>

<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Limpar</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right"> <i class="fa fa-plus"></i>   Listar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                    
                     
            <h4 class="card-title">Dados Cadastrais</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>Id Cliente</label>
                        <input type="text" id="example-titulo" name="rg_ie" class="form-control" placeholder="RG ou IE" value="<?=$id?>">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Cliente
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Cliente" value="<?=$nome?>" required="required">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Responsável
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="responsavel" class="form-control" placeholder="Responsável" value="<?=$responsavel?>">
                </div>

            </div>
            <br clear="all">
            <div class="row">
                <div class="col-md-2">
                        <label>Telefone
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="fonep" name="telefoneresp" class="form-control fone" placeholder="Telefone" value="<?=$telefoneresp?>" >
                </div>

                <div class="col-md-2">
                        <label for="example-hf-dataN">Data de Nascimento
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-datepicker" name="dataNascimento" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data de Nascimento" value="<?=$dataNascimento?>" >
                </div>

                <div class="col-md-3">
                        <label>Email
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="emailresp" class="form-control" placeholder="Email" value="<?=$emailresp?>" >
                </div>

                <div class="col-md-2">
                        <label for="example-hf-dataN">Data Ativação
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-datepicker" name="dataativacao" class="form-control input-datepicker data" data-date-format="dd/mm/yyyy" placeholder="Data de Ativação" value="<?=$dataativacao?>">
                </div>

                 <div class="form-group col-md-3">
                    <div class="col-md-12">
                        <label>Produto</label>
                        <select name="produto[]" id="produto" class="form-control" multiple="multiple">
                            <option value=""></option>
                            <option value="Whatsapp" <?=in_array("Whatsapp",$produto) ? "selected":""?>>Whatsapp</option>
                            <option value="messenger" <?=in_array("messenger",$produto) ? "selected":""?>>Messenger</option>
                        </select>    
                    </div>
                </div>
            </div>
            <br clear="all">
            <div class="row">
                <div class="col-md-3">
                        <label>Acesso Gestor (Qtd)</label>
                        <input type="text" id="example-titulo" name="acessogestor" class="form-control" placeholder="Acesso Gestor" value="<?=$acessogestor?>">
                    
                </div>
                <div class="col-md-3">
                        <label>Acesso Atendente (Qtd)</label>
                        <input type="text" id="example-titulo" name="acessoatendente" class="form-control" placeholder="Acesso Gestor" value="<?=$acessoatendente?>">
                    
                </div>
            </div>
            <br clear="all">
            <h4 class="card-title">Dados de Localização</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CEP (12345-123)
                        </label>
                        <input type="text" id="cep" name="cep" class="form-control cep" placeholder="CEP" value="<?=$cep?>">
                    
                </div>


                <div class="col-md-6">
                        <label>Logradouro
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="rua" name="endereco" class="form-control" placeholder="Logradouro" value="<?=$endereco?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Número
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="numero" name="numero" class="form-control" placeholder="Número" value="<?=$numero?>" required="required">
                </div>
                

                <div class="col-md-2">
                        <label>Complemento</label>
                        <input type="text" id="" name="complemento" class="form-control" placeholder="Complemento" value="<?=$complemento?>">
                </div>

                <br clear="all"><br clear="all">

                <div class="col-md-2">
                        <label>Bairro
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="bairro" name="bairro" class="form-control" placeholder="Bairro" value="<?=$bairro?>" required="required">
                </div>


                <div class="col-md-2">
                        <label>Cidade
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade" value="<?=$cidade?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Estado
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="estado" name="estado" class="form-control" placeholder="Cidade" value="<?=$estado?>" required="required">
                </div>

            </div>
            <br clear="all">

            <h4 class="card-title">Dados Empresariais</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CNPJ
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="cnpj" class="form-control cnpj" placeholder="CNPJ" value="<?=$cnpj?>" required="required">
                </div>

                <div class="col-md-2">
                        <label for="example-hf-nome">Razão Social
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="razao" class="form-control" placeholder="Razão Social" value="<?=$razao?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Telefone
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="fonep" name="telefone" class="form-control fone" placeholder="Telefone" value="<?=$telefone?>" required="required">
                </div>

                <div class="col-md-3">
                        <label>Email
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="email" class="form-control" placeholder="Email" value="<?=$email?>" required="required">
                </div>
                <div class="col-md-3">
                        <label>Messenger
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="messenger" class="form-control" placeholder="Messenger" value="<?=$messenger?>" >
                </div>
                <br clear="all"><br clear="all">
                <div class="col-md-3">
                        <label>Usuário
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="usuario" class="form-control" placeholder="Usuário" value="<?=$usuario?>">
                </div>
                <div class="col-md-3">
                        <label>Senha
                               <span class="text-danger">*</span>
                        </label>
                        <input type="password" id="example-titulo" name="senha" class="form-control" placeholder="Senha" value="">
                </div>

            </div>

            <br clear="all">

            <h4 class="card-title">Configuração</h4>
            <div class="row">
                <div class="col-md-6">
                        <label>Token Developer
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="dialogflow" class="form-control" placeholder="Dialogflow" value="<?=$dialogflow?>">
                </div>
                <div class="col-md-6">
                        <label>Token Client
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="dialogflowclient" class="form-control" placeholder="Dialogflow" value="<?=$dialogflowclient?>">
                </div>
                <br clear="all"><br clear="all">
               
                <div class="col-md-3">
                        <label>AI - Inteligencia artificial</label>
                        <br clear="all">
                        <input type="checkbox" id="example-titulo" name="frases" class="" value="1" data-toggle="toggle" <?=($frases==1?'checked=checked':'')?>>

                </div>
                <div class="col-md-3">
                        <label>Menu Personalizado</label>
                        <br clear="all">
                        <input type="checkbox" id="example-titulo" name="menupersonalizado" class="" value="1" data-toggle="toggle" <?=($menupersonalizado==1?'checked=checked':'')?>>

                </div>
                <div class="col-md-3">
                        <label>Notificador</label>
                        <br clear="all">
                        <input type="checkbox" id="example-titulo" name="notificador" class="" value="1" data-toggle="toggle" <?=($notificador==1?'checked=checked':'')?>>

                </div>

                <br clear="all"><br clear="all">
                <div class="form-group col-md-5">
                    <div class="col-md-12">
                        <label>QrCode</label>
                        <input type="text" id="example-titulo" name="ipserver" class="form-control" placeholder="http://0.0.0.0:0000/" value="<?=$ipserver?>">
                    </div>
                </div>
                

            </div>
            <br clear="all">
            
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
