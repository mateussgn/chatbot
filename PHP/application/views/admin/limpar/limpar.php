<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>

<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Limpar</h4>
            </div>
        </div>
        
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  

            <!--<a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/bairros') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Bairros</a>-->
            
            <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/fila') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>   Fila</a>

            <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/mensagens') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>   Mensagens</a>

            <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/dialogflow') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Interações Dialogflow</a>

            <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/envios') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Envios</a>

            <!--<a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/pedidos') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Pedidos</a>-->

            <!--<a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/entregadores') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Entregadores</a>-->

            <!--<a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/vendas') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Vendas</a>-->

            <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/chatlist') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Lista de Contatos</a>

            <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/agendacontatos') ?>" class="btn btn-effect-ripple btn-primary"> <i class="fa fa-plus"></i>  Agenda de Contatos</a>
            
          </div>
        </div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
