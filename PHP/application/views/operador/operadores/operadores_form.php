<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
if(isset($obj)){ 
	$id                    = $obj->id;
    $nome                  = $obj->nome;
    $nomecompleto                  = $obj->nomecompleto;
    $email                 = $obj->email;
    $mae                 = $obj->mae;
    $endereco              = $obj->endereco;
    $cep                   = $obj->cep;
    $cidade                = $obj->cidade;
    $estado                = $obj->estado;
    $bairro                = $obj->bairro;
    $numero                = $obj->numero;
    $razao                  = $obj->razao;
    $usuario                  = $obj->usuario;
    $telefone = $obj->telefone;
    $complemento           = $obj->complemento;
    $cpf              = $obj->cpf;
    $sexo                  = $obj->sexo;
    $arrDataNasc = explode("-", $obj->dataNascimento);
    $dataNasc    = $arrDataNasc[2]."/".$arrDataNasc[1]."/".$arrDataNasc[0];
    $dataNascimento        = $dataNasc;
    $dias           = explode(',', $obj->dias);
    $id_departamento           = explode(',', $obj->id_departamento);
    $horainicio                  = $obj->horainicio;
    $horafim                  = $obj->horafim;

    $btnsalvar = 'Salvar';
}else{
    $id                    = '';
    $nome                  = '';
    $email                 = '';
    $endereco              = '';
    $cep                   = '';
    $tipo                  = '';
    $cidade                = '';
    $estado                = '';
    $bairro                = '';
    $numero                = '';
    $telefone_preferencial = '';
    $telefone_alternativo  = '';
    $complemento           = '';
    $contato           = '';
    $cpf              = '';
    $usuario                  = '';
    $sexo                  = '';
    $dataNascimento        = '';
    $estadoCivil           = '';
    $btnsalvar = 'Salvar';

}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Perfil do Operador</h4>
            </div>
        </div>
        
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                     
            <h4 class="card-title">Dados Pessoais</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CPF
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="cpf" class="form-control cpf" placeholder="CPF" value="<?=$cpf?>" required="required">
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Primeiro Nome
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Nome" value="<?=$nome?>" required="required">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Nome Completo
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nomecompleto" class="form-control" placeholder="Nome Completo" value="<?=$nomecompleto?>" required="required">
                </div>

            </div>
            <br clear="all">
            

            <h4 class="card-title">Dados de Localização</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CEP (12345-123)
                        </label>
                        <input type="text" id="cep" name="cep" class="form-control cep" placeholder="CEP" value="<?=$cep?>">
                    
                </div>


                <div class="col-md-6">
                        <label>Logradouro
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="rua" name="endereco" class="form-control" placeholder="Logradouro" value="<?=$endereco?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Número
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="numero" name="numero" class="form-control" placeholder="Número" value="<?=$numero?>" required="required">
                </div>
                

                <div class="col-md-2">
                        <label>Complemento</label>
                        <input type="text" id="" name="complemento" class="form-control" placeholder="Complemento" value="<?=$complemento?>">
                </div>

                <br clear="all"><br clear="all">

                <div class="col-md-2">
                        <label>Bairro
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="bairro" name="bairro" class="form-control" placeholder="Bairro" value="<?=$bairro?>" required="required">
                </div>


                <div class="col-md-2">
                        <label>Cidade
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade" value="<?=$cidade?>" required="required">
                </div>

                <div class="col-md-2">
                        <label>Estado
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="estado" name="estado" class="form-control" placeholder="Cidade" value="<?=$estado?>" required="required">
                </div>

            </div>
            <br clear="all">

            <h4 class="card-title">Dados de Contato/Acesso</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>Telefone
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="fonep" name="telefone" class="form-control fone" placeholder="Telefone" value="<?=$telefone?>" required="required">
                </div>

                <div class="col-md-4">
                        <label>Email
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="email" class="form-control" placeholder="Email" value="<?=$email?>" required="required">
                </div>
                
                <div class="col-md-3">
                        <label>Usuário
                               <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="example-titulo" name="usuario" class="form-control" placeholder="Usuário" value="<?=$usuario?>">
                </div>
                <div class="col-md-3">
                        <label>Senha
                               <span class="text-danger">*</span>
                        </label>
                        <input type="password" id="example-titulo" name="senha" class="form-control" placeholder="Senha" value="">
                </div>

            </div>
            <br clear="all">
            
                    

            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
