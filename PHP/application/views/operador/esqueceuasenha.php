<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <title><?php echo head_title()?></title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=a_css('bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_css('font-awesome.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_css('style.css')?>">
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="main-wrapper">
        <div class="account-page">
            <div class="container">
                <h3 class="account-title">Esqueceu a Senha</h3>
                <div class="account-box">
                    <div class="account-wrapper">
                        <div class="account-logo">
                            <a href="<?=site_url()?>"><img src="<?=a_img('logo.jpg')?>" alt="Atendimento Digital" style="width: 100% !important;"></a>
                        </div>
                        <form action="<?=site_url('login/lembrar_a_senha')?>" method='post'>
                            <div class="form-group form-focus">
                                <label class="control-label">E-mail</label>
                                <input class="form-control floating" type="text">
                            </div>
                            
                            <div class="form-group text-center">
                                <button class="btn btn-primary btn-block account-btn" type="submit">Resetar a Senha</button>
                            </div>
                            <div class="text-center">
                                <a href="<?=site_url('login')?>">Voltar ao Login?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?=a_js('jquery-3.2.1.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_js('bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=a_js('app.js')?>"></script>
</body>

</html>
