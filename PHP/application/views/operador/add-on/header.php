<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="<?=a_assets('img/favicon.png')?>">
    <title><?php echo head_title()?></title>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/font-awesome.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/fullcalendar.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/dataTables.bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/select2.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/bootstrap-datetimepicker.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('plugins/morris/morris.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=a_assets('css/style.css')?>">
    <link href="<?=a_assets('css/bootstrap-toggle.min.css')?>" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
    #progressBar {
        background-color: #3E6FAD;
        width: 0px;
        height: 20px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        -o-border-radius: 5px;
        border-radius: 5px;
        -moz-transition: .25s ease-out;
        -webkit-transition: .25s ease-out;
        -o-transition: .25s ease-out;
        transition: .25s ease-out;
    }
    </style>
</head>

<body>

<!-- Modal confirmar -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span id="msn-titulo-form"></span></h4>
      </div>
      <div class="modal-body">
        <h3 id="msn-titulo-texto"></h3>
        <p id="msn-texto"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
        <button type="button" class="btn btn-primary btn-apagar-sim">Sim</button>
      </div>
    </div>
  </div>
</div>

    <div class="main-wrapper">
        <div class="header">
            <div class="header-left">
                <a href="<?=site_url('operador')?>" class="logo">
                    <img src="<?=a_assets('img/logo.jpg')?>" width="150" alt="">
                </a>
            </div>
            <div class="page-title-box pull-left">
                <!-- <h3>Atendimento Digital</h3> -->
            </div>
            <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>

            <ul class="nav navbar-nav navbar-right user-menu pull-right">
                <!-- <li class="dropdown hidden-xs">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i> <span class="badge bg-primary pull-right">3</span></a>
                    <div class="dropdown-menu notifications">
                        <div class="topnav-dropdown-header">
                            <span>Notifications</span>
                        </div>
                        <div class="drop-scroll">
                            <ul class="media-list">
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">
                                                <img alt="John Doe" src="<?=a_assets('img/user.jpg')?>" class="img-responsive img-circle">
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">John Doe</span> added new task <span class="noti-title">Patient appointment booking</span></p>
                                            <p class="noti-time"><span class="notification-time">4 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">V</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Tarah Shropshire</span> changed the task name <span class="noti-title">Appointment booking with payment gateway</span></p>
                                            <p class="noti-time"><span class="notification-time">6 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">L</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Misty Tison</span> added <span class="noti-title">Domenic Houston</span> and <span class="noti-title">Claire Mapes</span> to project <span class="noti-title">Doctor available module</span></p>
                                            <p class="noti-time"><span class="notification-time">8 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">G</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Rolland Webber</span> completed task <span class="noti-title">Patient and Doctor video conferencing</span></p>
                                            <p class="noti-time"><span class="notification-time">12 mins ago</span></p>
                                        </div>
                                    </a>
                                </li>
                                <li class="media notification-message">
                                    <a href="activities.html">
                                        <div class="media-left">
                                            <span class="avatar">V</span>
                                        </div>
                                        <div class="media-body">
                                            <p class="noti-details"><span class="noti-title">Bernardo Galaviz</span> added new task <span class="noti-title">Private chat module</span></p>
                                            <p class="noti-time"><span class="notification-time">2 days ago</span></p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="topnav-dropdown-footer">
                            <a href="activities.html">View all Notifications</a>
                        </div>
                    </div>
                </li>
                <li class="dropdown hidden-xs">
                    <a href="javascript:;" id="open_msg_box" class="hasnotifications"><i class="fa fa-comment-o"></i> <span class="badge bg-primary pull-right">8</span></a>
                </li> -->
                <li class="dropdown">
                    <a href="profile.html" class="dropdown-toggle user-link" data-toggle="dropdown" title="Admin">
                        <span class="user-img"><img class="img-circle" src="<?=a_assets('img/user.jpg')?>" width="40" alt="Admin">
                            <span class="status online"></span></span>
                        <span><?=$this->session->userdata('loggednameoperador')?></span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=site_url($this->session->userdata('folder').'operadores')?>">Editar Perfil</a></li>
                        <li><a href="<?=site_url($this->session->userdata('folder').'login/logout')?>">Logout</a></li>
                    </ul>
                </li>
            </ul>

            <div class="dropdown mobile-user-menu pull-right">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                <ul class="dropdown-menu pull-right">
                    <li><a href="<?=site_url($this->session->userdata('folder').'operadores')?>">Editar Perfil</a></li>
                    <li><a href="<?=site_url($this->session->userdata('folder').'login/logout')?>">Logout</a></li>
                </ul>

            </div>
        </div>
        <div class="sidebar" id="sidebar">
            <div class="sidebar-inner slimscroll">
                <div id="sidebar-menu" class="sidebar-menu">
                    <ul>
                        <li class="active">
                            <a href="<?=site_url($this->session->userdata('folder').'/')?>"><i class="fa fa-dashboard"></i> Painel</a>
                        </li>
                        <!--<li>
                            <div class="search-box" style="padding: 10px">
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control" placeholder="Pesquisa" required="" id="buscarcontato">
                                    <span class="input-group-btn">
                                        <button class="btn" type="button" onclick="buscarcontatos()">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </li>-->
                        <!-- <li>
                            <a href="<?=site_url($this->session->userdata('folder').'contatos')?>"><i class="fa fa-users" aria-hidden="true"></i> Contatos</a>
                        </li> -->
                        <div id="filaesquerda">
                        <!--<li class="menu-title">Entregadores</li>
                        <?
                        foreach ($entregadores as $key => $value) {
                        ?>
                            <li>
                                <a href="javascript://" onclick="openchat('<?=str_replace('@c.us', '', $value->number)?>','<?=$value->id?>')">
                                    <span class="chat-avatar-sm user-img">
                                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                                        <span class="status online"></span>
                                    </span>
                                    <?=$value->nome?>
                                    <br>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->telefone)?></span>
                                </a>
                            </li>
                        <?  
                        }
                        ?>-->

                        <li class="menu-title">Aguardando</li>
                        <?
                        foreach ($aguardando as $key => $value) {
                        ?>
                            <li>
                                <a href="javascript://" onclick="atenderchat('<?=$value->id?>','<?=str_replace('@c.us', '', $value->receive)?>')">
                                    <span class="chat-avatar-sm user-img">
                                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                                        <span class="status online"></span>
                                    </span>
                                    <?=$value->nome?><!--<?=str_replace('@c.us', '', $value->receive)?>-->
                                    <br>
                                    <?
                                        $data = $this->messagesmodel->getLastMessage($value->receive);
                                    ?>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 35px;">Pedido: <?=$value->protocolo?></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 145px; "><b><?=Date('d/m/Y',strtotime($data->datahora))?></b></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 170px; "><b><?=Date('H:i',strtotime($data->datahora))?></b></span>
                                </a>
                            </li>
                        <?  
                        }
                        ?>

                        <li class="menu-title">Em Atendimento</li>
                        <?
                        foreach ($ematendimento as $key => $value) {
                        ?>
                            <li>
                                <a href="javascript://" onclick="openchat('<?=str_replace('@c.us', '', $value->receive)?>','<?=$value->id?>')">
                                    <span class="chat-avatar-sm user-img">
                                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                                        <span class="status online"></span>
                                    </span>
                                    <?=$value->nome?><!--<?=str_replace('@c.us', '', $value->receive)?>-->
                                    <br>
                                    <?
                                        $data = $this->messagesmodel->getLastMessage($value->receive);
                                    ?>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 35px;">Pedido: <?=$value->protocolo?></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 145px; "><b><?=Date('d/m/Y',strtotime($data->datahora))?></b></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 170px; "><b><?=Date('H:i',strtotime($data->datahora))?></b></span>
                                </a>
                            </li>
                        <?  
                        }
                        ?>

                        <li class="menu-title">Finalizados</li>
                        <?
                        foreach ($finalizados as $key => $value) {
                        ?>
                            <li>
                                <a href="javascript://" onclick="openchat('<?=str_replace('@c.us', '', $value->receive)?>','<?=$value->id?>')">
                                    <span class="chat-avatar-sm user-img">
                                        <img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                                        <span class="status online"></span>
                                    </span>
                                    <?=$value->nome?><!--<?=str_replace('@c.us', '', $value->receive)?>-->
                                    <br>
                                    <?
                                        $data = $this->messagesmodel->getLastMessage($value->receive);
                                    ?>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 35px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 35px;">Pedido: <?=$value->protocolo?></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -26px;margin-left: 145px; "><b><?=Date('d/m/Y',strtotime($data->datahora))?></b></span>
                                    <span style="font-size: 10px;position: absolute;margin-top: -14px;margin-left: 170px; "><b><?=Date('H:i',strtotime($data->datahora))?></b></span>
                                </a>
                            </li>
                        <?  
                        }
                        ?>
                        </div>
                        
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-wrapper">

            <a id="mobile_btn_interacoes" class="mobile_btn_interacoes pull-left" href="#interacao"><i class="fa fa-comment" aria-hidden="true"></i></a>

        
