<?php  
$this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>



            <div class="chat-main-row">
                <div class="chat-main-wrapper">
                    <div class="col-xs-9 message-view task-view">
                        <div class="chat-window">
                            <div class="fixed-header">
                                <div class="navbar">
                                    <div class="user-details">
                                        <!-- <div class="pull-left user-img m-r-10">
                                            <a href="profile.html" title="Mike Litorus"><img src="<?=a_img('user.jpg')?>" alt="" class="w-40 img-circle"><span class="status online"></span></a>
                                        </div> -->
                                        <div class="user-info pull-left">
                                            <a href="profile.html" title="Mike Litorus"><span class="font-bold">Mike Litorus</span> 
                                                <!-- <i class="typing-text">Typing...</i> -->
                                            </a>
                                            <span class="last-seen">Visto pela última vez hoje 7:50 AM</span>
                                        </div>
                                    </div>
                                    <ul class="nav navbar-nav pull-right custom-menu">
                                        <!-- <li>
                                            <a href="#chat_sidebar" class="task-chat profile-rightbar pull-right"><i class="fa fa-user" aria-hidden="true"></i></a>
                                        </li> -->
                                        <li class="dropdown">
                                            <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="javascript:void(0)">Encaminhar</a></li>
                                                <li><a href="javascript:void(0)">Finalizar</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <!-- <div class="search-box pull-right">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" placeholder="Search" required="">
                                            <span class="input-group-btn">
                                                    <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                                </span>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="chat-contents">
                                <div class="chat-content-wrap">
                                    <div class="chat-wrap-inner">
                                        <div class="chat-box">
                                            <div class="chats">
                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>Hello. What can I do for you?</p>
                                                                <span class="chat-time">8:30 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat-line">
                                                    <span class="chat-date">October 8th, 2015</span>
                                                </div>

                                                <div class="chat chat-left">
                                                    <div class="chat-avatar">
                                                        <a href="profile.html" class="avatar">
                                                            <img alt="John Doe" src="<?=a_img('user.jpg')?>" class="img-responsive img-circle">
                                                        </a>
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>I'm just looking around.</p>
                                                                <p>Will you tell me something about yourself? </p>
                                                                <span class="chat-time">8:35 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>OK, my name is Limingqiang. I like singing, playing basketballand so on.</p>
                                                                <span class="chat-time">8:42 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="chat chat-left">
                                                    <div class="chat-avatar">
                                                        <a href="profile.html" class="avatar">
                                                            <img alt="John Doe" src="<?=a_img('user.jpg')?>" class="img-responsive img-circle">
                                                        </a>
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>You wait for notice.</p>
                                                                <span class="chat-time">8:30 am</span>
                                                            </div>
                                                        </div>
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>Consectetuorem ipsum dolor sit?</p>
                                                                <span class="chat-time">8:50 am</span>
                                                            </div>
                                                        </div>
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>OK?</p>
                                                                <span class="chat-time">8:55 am</span>
                                                            </div>
                                                        </div>
                                                        <div class="chat-bubble">
                                                            <div class="chat-content img-content">
                                                                <div class="chat-img-group clearfix">
                                                                    <p>Uploaded 3 Images</p>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <span class="chat-time">9:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>OK!</p>
                                                                <span class="chat-time">9:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat chat-left">
                                                    <div class="chat-avatar">
                                                        <a href="profile.html" class="avatar">
                                                            <img alt="John Doe" src="<?=a_img('user.jpg')?>" class="img-responsive img-circle">
                                                        </a>
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>Uploaded 3 files</p>
                                                                <ul class="attach-list">
                                                                    <li><i class="fa fa-file"></i> <a href="#">example.avi</a></li>
                                                                    <li><i class="fa fa-file"></i> <a href="#">activity.psd</a></li>
                                                                    <li><i class="fa fa-file"></i> <a href="#">example.psd</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>Consectetuorem ipsum dolor sit?</p>
                                                                <span class="chat-time">8:50 am</span>
                                                            </div>
                                                        </div>
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>OK?</p>
                                                                <span class="chat-time">8:55 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content img-content">
                                                                <div class="chat-img-group clearfix">
                                                                    <p>Uploaded 6 Images</p>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                    <a class="chat-img-attach" href="#">
                                                                        <img width="182" height="137" alt="" src="<?=a_img('placeholder.jpg')?>">
                                                                        <div class="chat-placeholder">
                                                                            <div class="chat-img-name">placeholder.jpg</div>
                                                                            <div class="chat-file-desc">842 KB</div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <span class="chat-time">9:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat chat-left">
                                                    <div class="chat-avatar">
                                                        <a href="profile.html" class="avatar">
                                                            <img alt="John Doe" src="<?=a_img('user.jpg')?>" class="img-responsive img-circle">
                                                        </a>
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <ul class="attach-list">
                                                                    <li class="pdf-file"><i class="fa fa-file-pdf-o"></i> <a href="#">Document_2016.pdf</a></li>
                                                                </ul>
                                                                <span class="chat-time">9:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat chat-right">
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <ul class="attach-list">
                                                                    <li class="pdf-file"><i class="fa fa-file-pdf-o"></i> <a href="#">Document_2016.pdf</a></li>
                                                                </ul>
                                                                <span class="chat-time">9:00 am</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chat chat-left">
                                                    <div class="chat-avatar">
                                                        <a href="profile.html" class="avatar">
                                                            <img alt="John Doe" src="<?=a_img('user.jpg')?>" class="img-responsive img-circle">
                                                        </a>
                                                    </div>
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>Typing ...</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-footer">
                                <div class="message-bar">
                                    <div class="message-inner">
                                        <a class="link attach-icon" href="#" data-toggle="modal" data-target="#drag_files"><img src="<?=a_img('attachment.png')?>" alt=""></a>
                                        <div class="message-area">
                                            <div class="input-group">
                                                <textarea class="form-control" placeholder="Type message..."></textarea>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-primary" type="button"><i class="fa fa-send"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 message-view chat-profile-view chat-sidebar" id="chat_sidebar">
                        <div class="chat-window video-window">
                            <div class="tab-content chat-contents">
                            <div class="sidebar-inner slimscroll">
                                <div class="sidebar-menu">
                                    <ul>
                                        <li class="menu-title">Filas de Atendimento <a href="#" class="add-user-icon" data-toggle="modal" data-target="#add_chat_user"><i class="fa fa-plus"></i></a></li>
                                        <li>
                                            <a href="chat.html"><span class="chat-avatar-sm user-img"><img src="<?=a_img('user.jpg')?>" alt="" class="img-circle"><span class="status online"></span></span> John Doe <span class="badge bg-danger pull-right">1</span></a>
                                        </li>
                                        <li>
                                            <a href="chat.html"><span class="chat-avatar-sm user-img"><img src="<?=a_img('user.jpg')?>" alt="" class="img-circle"><span class="status offline"></span></span> Richard Miles <span class="badge bg-danger pull-right">18</span></a>
                                        </li>
                                        <li>
                                            <a href="chat.html"><span class="chat-avatar-sm user-img"><img src="<?=a_img('user.jpg')?>" alt="" class="img-circle"><span class="status away"></span></span> John Smith</a>
                                        </li>
                                        <li class="active">
                                            <a href="chat.html"><span class="chat-avatar-sm user-img"><img src="<?=a_img('user.jpg')?>" alt="" class="img-circle"><span class="status online"></span></span> Mike Litorus <span class="badge bg-danger pull-right">108</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="drag_files" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Drag and drop files upload</h3>
                        </div>
                        <div class="modal-body p-t-0">
                            <form id="js-upload-form">
                                <div class="upload-drop-zone" id="drop-zone">
                                    <i class="fa fa-cloud-upload fa-2x"></i> <span class="upload-text">Just drag and drop files here</span>
                                </div>
                                <h4>Uploading</h4>
                                <ul class="upload-list">
                                    <li class="file-list">
                                        <div class="upload-wrap">
                                            <div class="file-name">
                                                <i class="fa fa-photo"></i> photo.png
                                            </div>
                                            <div class="file-size">1.07 gb</div>
                                            <button type="button" class="file-close">
                                                <i class="fa fa-close"></i>
                                            </button>
                                        </div>
                                        <div class="progress progress-xs progress-striped">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 65%"></div>
                                        </div>
                                        <div class="upload-process">37% done</div>
                                    </li>
                                    <li class="file-list">
                                        <div class="upload-wrap">
                                            <div class="file-name">
                                                <i class="fa fa-file"></i> task.doc
                                            </div>
                                            <div class="file-size">5.8 kb</div>
                                            <button type="button" class="file-close">
                                                <i class="fa fa-close"></i>
                                            </button>
                                        </div>
                                        <div class="progress progress-xs progress-striped">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 65%"></div>
                                        </div>
                                        <div class="upload-process">37% done</div>
                                    </li>
                                    <li class="file-list">
                                        <div class="upload-wrap">
                                            <div class="file-name">
                                                <i class="fa fa-photo"></i> dashboard.png
                                            </div>
                                            <div class="file-size">2.1 mb</div>
                                            <button type="button" class="file-close">
                                                <i class="fa fa-close"></i>
                                            </button>
                                        </div>
                                        <div class="progress progress-xs progress-striped">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 65%"></div>
                                        </div>
                                        <div class="upload-process">Completed</div>
                                    </li>
                                </ul>
                            </form>
                            <div class="m-t-30 text-center">
                                <button class="btn btn-primary btn-lg">Add to upload</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="add_group" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Create a group</h3>
                        </div>
                        <div class="modal-body">
                            <p>Groups are where your team communicates. They’re best when organized around a topic — #leads, for example.</p>
                            <form>
                                <div class="form-group">
                                    <label>Group Name <span class="text-danger">*</span></label>
                                    <input class="form-control" required="" type="text">
                                </div>
                                <div class="form-group">
                                    <label>Send invites to: <span class="text-muted-light">(optional)</span></label>
                                    <input class="form-control" required="" type="text">
                                </div>
                                <div class="m-t-50 text-center">
                                    <button class="btn btn-primary btn-lg">Create Group</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="add_chat_user" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Create Chat Group</h3>
                        </div>
                        <div class="modal-body">
                            <div class="input-group m-b-30">
                                <input placeholder="Search to start a chat" class="form-control search-input input-lg" id="btn-input" type="text">
                                <span class="input-group-btn">
                                        <button class="btn btn-primary btn-lg">Search</button>
                                    </span>
                            </div>
                            <div>
                                <h5>Recent Conversations</h5>
                                <ul class="media-list media-list-linked chat-user-list">
                                    <li class="media">
                                        <a href="#" class="media-link">
                                            <div class="media-left"><span class="avatar">J</span></div>
                                            <div class="media-body media-middle text-nowrap">
                                                <div class="user-name">Jeffery Lalor</div>
                                                <span class="designation">Team Leader</span>
                                            </div>
                                            <div class="media-right media-middle text-nowrap">
                                                <div class="online-date">1 day ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#" class="media-link">
                                            <div class="media-left"><span class="avatar">B</span></div>
                                            <div class="media-body media-middle text-nowrap">
                                                <div class="user-name">Bernardo Galaviz</div>
                                                <span class="designation">Web Developer</span>
                                            </div>
                                            <div class="media-right media-middle text-nowrap">
                                                <div class="online-date">3 days ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#" class="media-link">
                                            <div class="media-left">
                                                <span class="avatar">
                                                        <img src="<?=a_img('user.jpg')?>" alt="John Doe">
                                                    </span>
                                            </div>
                                            <div class="media-body media-middle text-nowrap">
                                                <div class="user-name">John Doe</div>
                                                <span class="designation">Web Designer</span>
                                            </div>
                                            <div class="media-right media-middle text-nowrap">
                                                <div class="online-date">7 months ago</div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="m-t-50 text-center">
                                <button class="btn btn-primary btn-lg">Create Group</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="share_files" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Share File</h3>
                        </div>
                        <div class="modal-body">
                            <div class="files-share-list">
                                <div class="files-cont">
                                    <div class="file-type">
                                        <span class="files-icon"><i class="fa fa-file-pdf-o"></i></span>
                                    </div>
                                    <div class="files-info">
                                        <span class="file-name text-ellipsis">AHA Selfcare Mobile Application Test-Cases.xls</span>
                                        <span class="file-author"><a href="#">Bernardo Galaviz</a></span> <span class="file-date">May 31st at 6:53 PM</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Share With</label>
                                <input class="form-control" type="text">
                            </div>
                            <div class="m-t-50 text-center">
                                <button class="btn btn-primary btn-lg">Share</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
