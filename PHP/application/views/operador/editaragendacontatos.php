<?php 
 $this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>
<?php
$this->load->model('bairrosmodel');

if(isset($obj)){ 
	$id                    = $obj->id;
    $id_cliente            = $obj->id_cliente;
    $id_contato            = $obj->id_contato;
    $nome                  = $obj->nome;
    $nomeCompleto          = $obj->nomeCompleto;
    $email                 = $obj->email;
    $facebook              = $obj->facebook;
    $instagram             = $obj->instagram;
    $rua                   = $obj->rua;
    $cep                   = $obj->cep;
    $cidade                = $obj->cidade;
    $estado                = $obj->estado;
    $bairro                = $obj->bairro;
    $numero                = $obj->numero;
    $number                = $obj->number;
    $complemento           = $obj->complemento;
    $pontoDeReferencia     = $obj->pontoDeReferencia;
    $cpf                   = $obj->cpf;
    $observacoes           = $obj->observacoes;

    $btnsalvar = 'Salvar';
}else{
    $id                    = '';
    $id_cliente            = '';
    $id_contato            = '';
    $nome                  = '';
    $nomecompleto          = '';
    $email                 = '';
    $facebook              = '';
    $instagram             = '';
    $rua                   = '';
    $cep                   = '';
    $cidade                = '';
    $estado                = '';
    $bairro                = '';
    $numero                = '';
    $number                = '';
    $complemento           = '';
    $pontoDeReferencia     = '';
    $cpf                   = '';
    $observacoes           = '';

    $btnsalvar = 'Salvar';

}

?>
<div class="content container-fluid">
    <?php show_alert(); ?> 
    <div class="row">
        <div class="col-xs-10">
            <div class="header-section">
                <h4 class="page-title">Editar Contato</h4>
            </div>
        </div>
        <div class="col-xs-2">
            <div class="header-section">
                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'')?>" class="btn btn-effect-ripple btn-primary pull-right">Voltar</a>
                
            </div>
        </div>
    </div>

 
    <div class="row">
        
        <div class="col-md-12">
          <div class="card-box">  
          
        <form action="<?php echo site_url($this->session->userdata('folder').$this->router->class.'/insert')?>" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >

        
        <!-- ########################################################################### -->
        <input type="hidden" id="id" name="id" value="<? echo $id?>">
                     
            <h4 class="card-title">Dados Pessoais</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CPF
                        </label>
                        <input type="text" id="example-titulo" name="cpf" class="form-control cpf" placeholder="CPF" value="<?=$cpf?>">
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Primeiro Nome
                            <span class="text-danger">*</span>
                        </label>
                        <input type="text" id="val-username" name="nome" class="form-control" placeholder="Nome" value="<?=$nome?>" required="required">
                    
                </div>

                <div class="col-md-5">
                        <label for="example-hf-nome">Nome Completo
                        </label>
                        <input type="text" id="val-username" name="nomeCompleto" class="form-control" placeholder="Nome Completo" value="<?=$nomeCompleto?>">
                </div>

            </div>
            <br clear="all">
            
            <h4 class="card-title">Dados de Localização</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>CEP (12345-123)
                        </label>
                        <input type="text" id="cep" name="cep" class="form-control cep" placeholder="CEP" value="<?=$cep?>">
                    
                </div>


                <div class="col-md-6">
                        <label>Logradouro
                        </label>
                        <input type="text" id="rua" name="rua" class="form-control" placeholder="Logradouro" value="<?=$rua?>">
                </div>

                <div class="col-md-2">
                        <label>Número
                        </label>
                        <input type="text" id="numero" name="numero" class="form-control" placeholder="Número" value="<?=$numero?>">
                </div>
                

                <div class="col-md-2">
                        <label>Complemento</label>
                        <input type="text" id="complemento" name="complemento" class="form-control" placeholder="Complemento" value="<?=$complemento?>">
                </div>                

                <br clear="all"><br clear="all">

                <div class="col-md-2">
                        <label>Bairro
                        </label>
                        <br clear="all">
                        <input type="text" id="bairro" name="bairro" class="form-control" placeholder="Bairro" value="<?=$bairro?>">
                </div>


                <div class="col-md-2">
                        <label>Cidade
                        </label>
                        <input type="text" id="cidade" name="cidade" class="form-control" placeholder="Cidade" value="<?=$cidade?>">
                </div>

                <div class="col-md-2">
                        <label>Estado
                        </label>
                        <input type="text" id="estado" name="estado" class="form-control" placeholder="Estado" value="<?=$estado?>">
                </div>

                <div class="col-md-6">
                        <label>Ponto de Referencia
                        </label>
                        <input type="text" id="pontoDeReferencia" name="pontoDeReferencia" class="form-control" placeholder="Ponto De Referencia" value="<?=$pontoDeReferencia?>">
                </div>

            </div>
            <br clear="all">

            <h4 class="card-title">Dados de Contato/Acesso</h4>
            <div class="row">
                <div class="col-md-2">
                        <label>Telefone
                        </label>
                        <label>
                               <span class="text-danger"><?=$number?></span>
                        </label>
                        <!--<input type="text" id="number" name="number" class="form-control" placeholder="Telefone" value="<?=$number?>" required="required">-->
                </div>

                <div class="col-md-4">
                        <label>Email
                        </label>
                        <input type="text" id="example-titulo" name="email" class="form-control" placeholder="Email" value="<?=$email?>">
                </div>

                <div class="col-md-3">
                        <label>Facebook
                        </label>
                        <input type="text" id="facebook" name="facebook" class="form-control" placeholder="Facebook" value="<?=$facebook?>">
                </div>

                <div class="col-md-3">
                        <label>Instagram
                        </label>
                        <input type="text" id="instagram" name="instagram" class="form-control" placeholder="Instagram" value="<?=$instagram?>">
                </div>

                <br clear="all"><br clear="all">

                <div class="col-md-12">
                        <label>Observacoes do Contato
                        </label>
                       <!--<textarea rows="4" cols="12" type="text" id="example-titulo" name="observacoes" class="form-control" placeholder="Observacoes" value="<?=$observacoes?>"></textarea>-->
                        <input type="text" id="example-titulo" name="observacoes" class="form-control" placeholder="Observacoes" value="<?=$observacoes?>">
                </div>

            </div>
            
            <br clear="all">
            
            <div class="text-right">
                <input type="submit" class="btn btn-effect-ripple btn-primary" value="<?=$btnsalvar?>">
            </div>

                    

                </form>
                <!-- END Horizontal Form Content -->
            
    </div>
</div>

</div>

<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
