<?php  
$this->load->view($this->session->userdata('folder').'add-on/header' , $modulos);
?>



            <div class="chat-main-row">
                <div class="chat-main-wrapper">
                    <div class="col-xs-9 message-view task-view" id="chat-window">
                        <div class="chat-window">
                            <div class="fixed-header">
                                <div class="ebar">
                                    <div class="user-details">
                                        <!-- <div class="pull-left user-img m-r-10">
                                            <a href="profile.html" title="Mike Litorus"><img src="<?=a_img('user.jpg')?>" alt="" class="w-40 img-circle"><span class="status online"></span></a>
                                        </div> -->
                                        <div class="user-info pull-left">
                                            <!-- <a href="profile.html" title="Mike Litorus"> -->
                                                <span class="font-bold">Painel de Controle</span> 
                                                <!-- <i class="typing-text">Typing...</i> -->
                                            <!-- </a> -->
                                            <!-- <span class="last-seen">Visto pela última vez hoje 7:50 AM</span> -->
                                        </div>
                                    </div>
                                    <!-- <div class="search-box pull-right">
                                        <div class="input-group input-group-sm">
                                            <input type="text" class="form-control" placeholder="Search" required="">
                                            <span class="input-group-btn">
                                                    <button class="btn" type="button"><i class="fa fa-search"></i></button>
                                                </span>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="chat-contents">
                                <div class="chat-content-wrap">
                                    <div class="chat-wrap-inner">
                                        <div class="chat-box">
                                            <div class="chats">
                                                <div class="chat chat-left">
                                                    <div class="chat-body">
                                                        <div class="chat-bubble">
                                                            <div class="chat-content">
                                                                <p>Selecione alguém na fila!</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="chat-footer">
                                <div class="message-bar">
                                    <div class="message-inner">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    if($this->session->userdata('modulointeracoes') == 1){
                    ?>
                    <div class="col-xs-3 message-view chat-profile-view chat-sidebar" id="chat_sidebar" rel=''>
                        <div class="chat-window video-window">
                            <div class="tab-content chat-contents">
                            <div class="sidebar-inner slimscroll">

                                <div class="search-box" style="padding: 10px">
                                    <div class="input-group input-group-sm">
                                        <input type="text" class="form-control" placeholder="Pesquisa" required="" id="buscarcontato">
                                        <span class="input-group-btn">
                                                <button class="btn" type="button" onclick="buscarcontatos()"><i class="fa fa-search"></i></button>
                                            </span>
                                    </div>
                                </div>

                                
                                <div class="sidebar-menu" id="fila">
                                    <ul class="filas">
                                        <li class="menu-title">
                                            <th>Lista de Interação</th>
                                            <th>
                                                <a href="javascript://" onclick="agendacontatos()">Editar Contatos</a>
                                            </th>
                                        </li>
                                        <?
                                        foreach ($interacoes as $key => $value) {
                                        ?>
                                            <li>
                                                <a href="javascript://" onclick="openchat('<?=str_replace('@s.whatsapp.net', '', $value->receive)?>')">
                                                    <span class="chat-avatar-sm user-img"><img src="<?=a_img('user.jpg')?>" alt="" class="img-circle">
                                                        <span class="status online"></span>
                                                    </span> 
                                                    <?
                                                    if(trim($value->name)==''){
                                                        echo str_replace('@c.us', '', $value->receive);
                                                    }else{
                                                        echo $value->name;
                                                    }
                                                    ?>
                                                    <span class="badge bg-success pull-right">Visualizar</span>
                                                </a>
                                                <span style="font-size: 10px;position: absolute;margin-top: -13px;margin-left: 54px;"><?=str_replace('@c.us', '', $value->receive)?></span>
                                            </li>
                                        <?  
                                        }
                                        ?>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                        </div>
                    </div>
                    <?
                    }
                    ?>
                </div>
            </div>

            
            <div id="drag_files" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Envio de Arquivos</h3>
                        </div>
                        <div class="modal-body p-t-0">
                            <form id="js-upload-form">
                                <div id="filename"></div>
                                <div id="progress"></div>
                                <div id="progressBar"></div>
                                <br clear="all">
                                <input type="file" name="file" accept="image/jpg,application/pdf">

                            </form>
                            <!-- <div class="m-t-30 text-center">
                                <button class="btn btn-primary btn-lg">Add to upload</button>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>

            <div id="encaminharmodal" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Encaminhar Atendimento</h3>
                        </div>
                        <div class="modal-body p-t-0">
                            <form action="#" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >
                                <input type="hidden" id="idfilaencaminhar" name="idfila" value="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Departamento</label>
                                        <select name="id_departamento" id="id_departamento_enc" class="form-control">
                                            <option value="">Selecione</option>
                                            <?
                                            foreach ($departamentos as $key => $value) {
                                                ?>
                                                <option value="<?=$value->id?>"><?=$value->titulo?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    
                                    </div>
                                </div>

                                <br clear="all">

                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Atendente</label>
                                        <select name="id_atendente_enc" id="id_atendente_enc" class="form-control">
                                            <option value="">Selecione</option>
                                            <?
                                            foreach ($operadores as $key => $value) {
                                                ?>
                                                <option value="<?=$value->nome?>"><?=$value->nome?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    
                                    </div>
                                </div>
                                <br clear="all">
                                
                                    
                                <div class="text-right">

                                    <input type="button" class="btn btn-effect-ripple btn-primary" value="Encaminhar" onclick="encaminharchatsend()"></button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!--<div id="entregadormodal" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Escolher Entregador</h3>
                        </div>
                        <div class="modal-body p-t-0">
                            <form action="#" method="post" class="form-horizontal form-bordered form-validation" id='form-validation' >
                                <input type="hidden" id="identregadorencaminhar" name="identregador" value="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Entregador</label>
                                        <select name="id_entregador" id="id_entregador_enc" class="form-control">
                                            <option value="">Selecione</option>
                                            <?
                                            foreach ($entregadores as $key => $value) {
                                                ?>
                                                <option value="<?=$value->id?>"><?=$value->nome?></option>
                                                <?
                                            }
                                            ?>
                                        </select>
                                    
                                    </div>
                                </div>

                                <br clear="all">                                
                                    
                                <div class="text-right">

                                    <input type="button" class="btn btn-effect-ripple btn-primary" value="Encaminhar" onclick="entregadorchatsend()"></button>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>-->

            <div id="agendacontatosmodal" class="modal custom-modal fade center-modal" role="dialog">
                <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title">Contatos</h3>
                        </div>
                        <div class="modal-body p-t-0">
                            <div class="row">
                    <div class="col-lg-12">
                        <div class="card-box">
                            <div class="card-block">
                                <table class="display datatable table table-stripped">
                                    <thead>
                                        <tr>
                                            <th width="7%">ID</th>
                                            <th width="13%">Telefone</th>
                                            <th>Nome</th>
                                            <th>Bairro</th>
                                            <th width="10%">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?
                                        foreach ($agendadecontatos as $l) {
                                        ?>
                                        <tr>
                                            <td class="text-center"><?=$l->id?></td>
                                            <td><?=$l->number?></td>
                                            <td><?=$l->nome?></td>
                                            <td><?=$l->bairro?></td>
                                            <td class="text-center">

                                                <a href="<?=site_url($this->session->userdata('folder').$this->router->class.'/editaragendacontatos/'.$l->number)?>" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-sm btn-success" style="overflow: hidden; position: relative;" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                           
                                                <!--<a href="javascript://" onclick="apagarmodal('<?=$this->router->class?>','apagar','<?=$l->nome?>',<?=$l->id?>)" title="" class="btn btn-effect-ripple btn-sm btn-danger" style="overflow: hidden; position: relative;" data-original-title="Apagar"><i class="fa fa-times"></i></a>-->                                
                                           
                                         </td><!--data-toggle="modal" data-target="#myModal"-->
                                        </tr>
                                        <?
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        </div>
                    </div>
                </div>
            </div>

                   

<div id="sound">
    <audio id="player"> 
          <source src="<?=a_assets('sounds/notify.mp3')?>" type="audio/mp3" >
    </audio>
</div>

<div id="toast"></div>
<div id="toast2"></div>


<?php $this->load->view($this->session->userdata('folder').'add-on/footer'); ?>
<script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>

<script type="text/javascript" src="<?=a_assets('js/simpleUpload.min.js')?>"></script>

<script type="text/javascript">

function openchat(receive,idfila){
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/openchat/'+receive+'/'+idfila, /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          //data: 'receive=' + receive, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                $('#chat-window').html(data);

                lastid();

                var height = 0;
                $('.chat-wrap-inner .chat-box').each(function(i, value){
                    height += parseInt($(this).height());
                });

                height += '';

                $('.chat-wrap-inner').animate({scrollTop: height});
                

                setInterval(function(){
                    //lendo mensagens
                    loadmsg();
                    
                }, 2000);

            }     
     });

    
}

function loadmsg(){
    var msg = $('#msg').val();
    var jid = $('#jid').val();
    var id_atendente = $('#id_atendente').val();
    var number = $('#number').val();
    var last_id = $('#last_id').val();


    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/loadmsg/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'msg='+msg+'&jid='+jid+'&id_atendente='+id_atendente+'&number='+number+'&last_id='+last_id, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                if(data != ''){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                    //pegando ultimo id e adicionando ao campo
                }

            }     
     });
    lastid();
}


function atenderchat(id,receive){
    
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/atenderchat/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'id='+id+'&receive='+receive, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                openchat(receive,id);
                loadfilaesquerda();

                //apos atender escrevendo mensagem de atendimento do menu
                setTimeout(function(){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                }, 3000);
                
            }     
     });
}

function finalizarchat(id){
    
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/finalizarchat/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'id='+id, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                loadfilaesquerda();
                //loadmsg();

                //apos atender escrevendo mensagem de atendimento do menu
                setTimeout(function(){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                }, 3000);
            }     
     });
}

function encaminharchat(id){
    
    $('#encaminharmodal').modal();

    $('#idfilaencaminhar').val(id);
    
}

/*function entregadorchat(id){
    
    $('#entregadormodal').modal();

    $('#identregadorencaminhar').val(id);
    
}*/

function agendacontatos(){

    $('#agendacontatosmodal').modal();

}

function encaminharchatsend(){
    var idfila = $('#idfilaencaminhar').val();
    var id_departamento = $('#id_departamento_enc').val();
    var id_atendente = $('#id_atendente_enc').val();

    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/encaminharchat/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'idfila='+idfila+'&id_departamento='+id_departamento+'&id_atendente='+id_atendente, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                loadfilaesquerda();
                //loadmsg();

                //apos atender escrevendo mensagem de atendimento do menu
                setTimeout(function(){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                }, 3000);
            }     
     });

    $('#encaminharmodal').modal('hide');


}

/*function entregadorchatsend(){
    var idfila = $('#identregadorencaminhar').val();
    var id_entregador = $('#id_entregador_enc').val();

    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/entregadorchat/', /* URL que será chamada */ 
          /*type : 'GET', /* Tipo da requisição */ 
          /*data: 'idfila='+idfila+'&id_entregador='+id_entregador, /* dado que será enviado via POST */
          /*dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            /*success: function(data){
                loadfilaesquerda();
                //loadmsg();

                //apos atender escrevendo mensagem de atendimento do menu
                setTimeout(function(){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                }, 3000);
            }     
     });

    $('#entregadormodal').modal('hide');


}*/

function criaratendimento(receive,id_atendente){
    
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/criaratendimento/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'id_atendente='+id_atendente+'&receive='+receive, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                loadfilaesquerda();
                //loadmsg();

                //apos atender escrevendo mensagem de atendimento do menu
                setTimeout(function(){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                }, 3000);
            }     
     });

    $('#encaminharmodal').modal('hide');


}

function reabrirchat(id){
    
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/reabrirchat/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'id='+id, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                loadfilaesquerda();

                //apos atender escrevendo mensagem de atendimento do menu
                setTimeout(function(){
                    $(data).appendTo('.chats');
                    var height = 0;
                    $('.chat-wrap-inner .chat-box').each(function(i, value){
                        height += parseInt($(this).height());
                    });

                    height += '';

                    $('.chat-wrap-inner').animate({scrollTop: height});
                }, 3000);
            }     
     });
}


function loadfila(){
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/loadfila', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: '', /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                $('#fila').html(data);
            }     
     });
}

function playNotification(){
    audio = document.getElementById('player');
    audio.play();
}

function loadfilaalert(){
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/loadfilaalert', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: '', /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                //alert(data);
                //$('#player').play();
                if(data!=0){
                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-bottom-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "10000",
                      "hideDuration": "10000",
                      "timeOut": "5000",
                      "extendedTimeOut": "5000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    };
                    playNotification();
                    toastr.info(data);
                }
            }     
     });
}

function loadmsgalert(){
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/loadmsgalert', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: '', /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                //alert(data);
                //$('#player').play();
                if(data!=0){
                    toastr.options = {
                      "closeButton": true,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-bottom-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "10000",
                      "hideDuration": "10000",
                      "timeOut": "5000",
                      "extendedTimeOut": "5000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                    };
                    playNotification();
                    toastr.success(data);
                }
            }     
     });
}

function loadfilaesquerda(){
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/loadfilaesquerda', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: '', /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                $('#filaesquerda').html(data);
            }     
     });
}

function lastid(){
    var msg = $('#msg').val();
    var jid = $('#jid').val();
    var id_atendente = $('#id_atendente').val();
    var number = $('#number').val();
    var last_id = $('#last_id').val();


    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/lastid/', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'msg='+msg+'&jid='+jid+'&id_atendente='+id_atendente+'&number='+number+'&last_id='+last_id, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                $('#last_id').val(data);
            }     
     });
}

function sendchat(){
    var msg = $('#msg').val();
    var jid = $('#jid').val();
    var id_atendente = $('#id_atendente').val();
    var number = $('#number').val();
    var arquivo = $('#arquivo').val();

    $('#arquivo').val('');
    $('#msg').val('');

    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/insertchat/', /* URL que será chamada */ 
          type : 'POST', /* Tipo da requisição */ 
          data: 'msg='+msg+'&jid='+jid+'&id_atendente='+id_atendente+'&number='+number+'&arquivo='+arquivo, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                //comentado pois estava duplicando
                // $(data).appendTo('.chats');
                // var height = 0;
                // $('.chat-wrap-inner .chat-box').each(function(i, value){
                //     height += parseInt($(this).height());
                // });

                // height += '';

                // $('.chat-wrap-inner').animate({scrollTop: height});

            }     
     });
}

function buscar(){
    var txt = $('#busca').val(); 
    //$('.chats').find(txt).css('color','red');     
    $(".chats *").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(txt) > -1);
    });
}

function buscarcontatos(){
    var txt = $('#buscarcontato').val(); 
    $.ajax({
    url : '<?=site_url().$this->session->userdata('folder')?>dashboard/loadfila', /* URL que será chamada */ 
          type : 'GET', /* Tipo da requisição */ 
          data: 'txt='+txt, /* dado que será enviado via POST */
          dataType: 'html', /* Tipo de transmissão */
            /* dado que será enviado via POST */
            success: function(data){
                $('#fila').html(data);
            }     
     });
}


$('input[type=file]').change(function(){

    $(this).simpleUpload("<?=site_url().$this->session->userdata('folder')?>dashboard/upload", {

        start: function(file){
            //upload started
            $('#filename').html(file.name);
            $('#progress').html("");
            $('#progressBar').width(0);
        },

        progress: function(progress){
            //received progress
            $('#progress').html("Progress: " + Math.round(progress) + "%");
            $('#progressBar').width(progress + "%");
        },

        success: function(data){
            //alert(data);
            //upload successful
            //$('#progress').html("Sucesso!<br>Data: " + JSON.stringify(data));
            $('#filename').html(data);
            $('#arquivo').val(data);
            $('#progress').html("Sucesso!");
        },

        error: function(error){
            //upload failed
            $('#progress').html("Failure!<br>" + error.name + ": " + error.message);
        }

    });

});

$( document ).ready(function() {
    setInterval(function(){
        //lendo mensagens
        loadfila();
        loadfilaesquerda();
    }, 30000);

    setInterval(function(){
        //lendo mensagens
        loadfilaalert();
        loadmsgalert();
    }, 10000);

});

</script>
