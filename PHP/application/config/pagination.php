<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//CONFIG DEFAULT PAGINATION
$config['per_page'] = 8;
//$config['num_links'] = 5;

$config['first_tag_open'] = '<div class="pagination pagination-large pagination-centered"><ul>';
$config['first_tag_close'] = '</ul></div>';
$config['last_link'] = 'Anterior';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_link'] = 'Proximo';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_link'] = 'Anterior';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="page"><a href="#" title="">';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';