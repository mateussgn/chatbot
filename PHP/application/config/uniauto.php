<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['cep_origem'] = '12701220';

/*
|--------------------------------------------------------------------------
| Formas de envio
|--------------------------------------------------------------------------
| 
| 41106: PAC sem contrato
| 40010: SEDEX sem contrato
| 40045: SEDEX a Cobrar, sem contrato
| 40126: SEDEX a Cobrar, com contrato
| 40215: SEDEX 10, sem contrato
| 40290: SEDEX Hoje, sem contrato
| 40096: SEDEX com contrato
| 40436: SEDEX com contrato
| 40444: SEDEX com contrato
| 81019: e-SEDEX, com contrato
| 41068: PAC com contrato
| 40568: SEDEX com contrato
| 40606: SEDEX com contrato
| 81868: (Grupo 1) e-SEDEX, com contrato
| 81833: (Grupo 2) e-SEDEX, com contrato
| 81850: (Grupo 3) e-SEDEX, com contrato
| 
*/
$config['envio'] = array
(
    40010 => 'SEDEX',
    41106 => 'PAC'
);

$config['emailspadroes'] = 'contato@lembrelembre.com.br';


$config['pagamento'] = array(
    'credito' => array(
        'visa', 
        'mastercard', 
        'elo', 
        'diners', 
        'amex'
    ),
    'opcao_boleto' => array(
        'boleto'
    ),
    'debito' => array(
        'bb',
        'itau',
        'bradesco'
    )
);
