import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MyRunnable extends Thread {

	private WebDriver driver;
//	private String URL;

	public MyRunnable(WebDriver driver, String URL) {

		this.driver = driver;
//		this.URL = URL;

		start();

	}

	public void run() {

		while (true) {
			try {

				System.out.println("::: Pegando mensagens :::");

				this.get_message();
				this.getResposta();

				TimeUnit.SECONDS.sleep(5);
			} catch (IOException | InterruptedException e) {

				e.printStackTrace();
			} catch (NullPointerException npe) {
				npe.printStackTrace();
			}
		}

	}

	public void getResposta() throws IOException, InterruptedException {

		String urls = "http://192.99.208.96/rest/restapi/last";

		System.out.println(urls);

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(urls);
		request.addHeader("content-type", "text/html");
		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		int status = response.getStatusLine().getStatusCode();
		String ahu = EntityUtils.toString(entity, "UTF-8");

		System.out.println("\n" + status);
		System.out.println("\n" + ahu);

		if (status == 200) {

			JsonArray arr = new JsonParser().parse(ahu).getAsJsonArray();

			for (int i = 0; i < arr.size(); i++) {

				JsonObject rec = (JsonObject) arr.get(i);
				String id = rec.get("id").getAsString();
				String number = rec.get("jid").toString();
				String resp = rec.get("message").toString();
				String anexo = rec.get("image").toString();
				String cap = "";

				// String filename = rec.get("filename").toString();
				String filename = "";
				String fname = rec.get("media_name").getAsString();

				System.out.println("\n" + id);
				System.out.println("\n" + number);
				System.out.println("\n" + resp);

				if (!fname.isEmpty()) {

					// this.send_data(number, anexo, filename, cap);
					this.send_message_image(number, anexo, fname, resp);
					this.update_post(id);
				} else {

					String aaas = this.send_message(number, resp);
					if (aaas != null) {
						this.update_post(id);
					}

				}

			}
		}

	}

	public void send_data(String idd, String imgBase64, String filename, String cap)
			throws IOException, InterruptedException {

		String caption = "\"\"";

		System.out.println("\n" + imgBase64);
		System.out.println("\n" + filename);
		System.out.println("\n" + idd);

		((JavascriptExecutor) driver).executeScript(
				"window.WAPI.sendImage(" + imgBase64 + ", " + idd + ", " + filename + ", " + caption + ");");

	}

	public void update_post(String q) throws IOException {

		String u = "http://192.99.208.96/rest/restapi/update/" + q;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPut request = new HttpPut(u);
		request.addHeader("content-type", "application/json");
		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");

		System.out.println("\n" + "::: retorno ::: ");
		System.out.println("\n" + responseString);

	}

	public String send_message(String idd, String msg) throws IOException, InterruptedException {

		String cmd = "window.WAPI.sendMessageToID(" + idd + ", " + msg + ")";
		String ahu = "ok";

		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript(cmd);
		}

		return ahu;

	}

	public String send_message_image(String idd, String image, String name, String msg)
			throws IOException, InterruptedException {

		String cmd = "window.WAPI.sendImage(" + image + ", " + idd + "," + name + "," + msg + ")";

		String ahu = "ok";

		if (driver instanceof JavascriptExecutor) {
			((JavascriptExecutor) driver).executeScript(cmd);
		}

		return ahu;

	}

	public String get_message() throws IOException, InterruptedException {

		String cmd = "return window.WAPI.getBufferedNewMessages()";
		String ahu = null;

		if (driver instanceof JavascriptExecutor) {

			ahu = ((JavascriptExecutor) driver).executeScript(cmd).toString();
		}

		if (!ahu.equals("[]")) {

			JsonObject jsonObject = new JsonParser().parse(ahu).getAsJsonObject();
			JsonArray arr = jsonObject.getAsJsonArray("messages");
			for (int i = 0; i < arr.size(); i++) {

				JsonObject rec = (JsonObject) arr.get(i);
				String id = rec.get("number").toString();
				String tipo = rec.get("tipo").getAsString();
				String resp;
				if (tipo.equals("chat")) {
					String r = arr.get(i).toString();
					resp = this.send_post(r);

				} else {
					String url = rec.get("URL").getAsString();
					String chave = rec.get("mediaKey").getAsString();
					String resps = this.img_decoder(url, chave, tipo);
					rec.remove("message");
					rec.addProperty("message", resps.trim());
					String r = rec.toString();
					System.out.println("::: RRRR ::: " + r);
					resp = this.send_post(r);

				}

				resp = "\"" + resp + "\"";

				System.out.println("\n" + "::: retorno ::: "
				                 + "\n" + id
				                 + "\n" + " - Resp:"
				                 + "\n" + resp);

				if (resp != null && !resp.trim().isEmpty()) {
					this.send_message(id, resp);
				}

				/*
				 * if(mimetype != "indefinido") { this.update_message_image(idwhats); }
				 */

				// System.out.println(resp);

			}
		}

		return ahu;

	}

	public String img_decoder(String URL, String Chave, String tipo) throws IOException {

		Process p = Runtime.getRuntime()
				.exec("python3 /home/ultra00005/eclipse-workspace/robot/imgem.py " + URL + " " + Chave + " " + tipo);
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		StringBuilder builder = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append(System.getProperty("line.separator"));
		}
		String result = builder.toString();

		System.out.println("\n::: imagebase64 ::: "
		                 + "\n" + result);
		return result;
	}

	public String send_post(String q) throws IOException {

		String nurl = "http://192.99.208.96/rest/restapi/insertmessage";
		// String nurl = "http://localhost/meusprojetos/ichat/postRest.php";

		System.out.println("\n" + q);

		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(nurl);
		StringEntity params = new StringEntity(q, "UTF-8");

		request.addHeader("content-type", "application/json");
		request.setEntity(params);

		HttpResponse response = httpClient.execute(request);
		HttpEntity entity = response.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");
		return responseString;

	}

}