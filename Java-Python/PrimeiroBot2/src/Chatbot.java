import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.nanohttpd.protocols.http.IHTTPSession;
import org.nanohttpd.protocols.http.NanoHTTPD;
import org.nanohttpd.protocols.http.response.Response;
import org.nanohttpd.util.ServerRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Chatbot extends NanoHTTPD {

	private WebDriver driver;
	public Runnable mYr;

	private String URL = "/comments";
	private String mee = "";
	private String ttle = "";

	private String dir_drive = "/usr/bin/chromedriver";
	private String dir_gMess = "/home/ultra00005/eclipse-workspace/robot/configs/chat2.js";
	private String dir_qr = "/home/ultra00005/eclipse-workspace/robot/configs/screenshot.png";
	private String mee_dir = "/home/ultra00005/eclipse-workspace/robot/configs/MyApp.Properties";
	private String profile = "/home/ultra00005/eclipse-workspace/robot/configs/Default/";

	// //Windows dir::
	// private String absolutePath =
	// getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
	// private String dir_drive = absolutePath + "\\configs\\chromedriver.exe";
	// private String dir_gMess = absolutePath + "\\configs\\temp_config_novo.js";
	// private String dir_qrc = absolutePath + "\\configs\\screenshot.png";
	// private String mee_dir = absolutePath + "\\configs\\MyApp.Properties";
	// private String profile = absolutePath +
	// "\\configs\\configs\\profile\\Default\\";
	private static int getRandomNumberInRange(int min, int max) {
		int numporta;
		if(min>=max) {
			throw new IllegalArgumentException("max must be grater than min");			
		}
		
		Random r = new Random();
		numporta = r.nextInt((max - min)+1) + min;
		System.out.println("::::::::::::::::::::::::::::::::::"
				       + "\n::: Porta para o painel = " + numporta + " :::\n"
				       	 + "::::::::::::::::::::::::::::::::::");
		return numporta;
	}
	
	static int porta = getRandomNumberInRange(4000,7999);
	
	public Chatbot() throws InterruptedException, IOException {
		super(porta);

		if (startSelenium()) {

			Properties AppProps = new Properties();

			Path PropertyFile = Paths.get(mee_dir);
			try {

				Reader PropReader = Files.newBufferedReader(PropertyFile);
				AppProps.load(PropReader);
				mee = AppProps.getProperty("mee");
				URL = AppProps.getProperty("URL");
				PropReader.close();
			} catch (IOException Ex) {
				System.out.println("\n" + "IO Exception :"
			                     + "\n" + Ex.getMessage());
			}

		}

	}

	public Response serve(IHTTPSession session) {

		String uri = session.getUri();
		String mss = null;

//		System.out.println( );

		// sairDaConta()
		switch (uri) {
		case "/getqr":
			try {
				mss = "data:image/png;base64, " + takeQR();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		case "/status":
			try {
				mss = isLoggedIn();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case "/reload":
			try {
				this.reload();
				mss = "reload";
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case "/forcereload":
			try {
				this.Forcereload();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case "/restartSelenium":
			try {
				this.RestartSelenium();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		default:
			mss = "sem qr";

		}

		return Response.newFixedLengthResponse(mss);
	}

	@SuppressWarnings("deprecation")
	public boolean RestartSelenium() throws InterruptedException, IOException {
		driver.close();
		ChromeOptions chromeOptions = new ChromeOptions();
		System.setProperty("webdriver.chrome.driver", dir_drive);
		chromeOptions.addArguments("user-data-dir=" + profile);
		driver = new ChromeDriver(chromeOptions);
		driver.get("http://web.whatsapp.com");

		TimeUnit.SECONDS.sleep(10);
		String filePath = dir_gMess;
		String fileContents = readFileAsString(filePath);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(fileContents);

		((Thread) mYr).stop();
		mYr = new MyRunnable(driver, URL);

		return true;
	}

	@SuppressWarnings("deprecation")
	public void Forcereload() throws IOException, InterruptedException {
		mYr = new MyRunnable(driver, URL);
	}

	public void reload() throws IOException, InterruptedException {

		boolean thread = ((Thread) this.mYr).isAlive();

		if (!thread) {
			this.mYr = new MyRunnable(driver, URL);
		}

	}

	public String isLoggedIn() throws IOException, InterruptedException {

		String cmd = "return window.WAPI.isLoggedIn()";
		String ahu = null;

		if (driver instanceof JavascriptExecutor) {

			ahu = ((JavascriptExecutor) driver).executeScript(cmd).toString();

		}

		return ahu;
	}

	public boolean startSelenium() throws InterruptedException, IOException {
		ChromeOptions chromeOptions = new ChromeOptions();
		System.setProperty("webdriver.chrome.driver", dir_drive);
		chromeOptions.addArguments("user-data-dir=" + profile);
		driver = new ChromeDriver(chromeOptions);
		driver.get("http://web.whatsapp.com");

		TimeUnit.SECONDS.sleep(10);
		String filePath = dir_gMess;
		String fileContents = readFileAsString(filePath);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(fileContents);

		TimeUnit.SECONDS.sleep(2);
		this.mYr = new MyRunnable(driver, URL);

		return true;
	}

	private String readFileAsString(String filePath) throws IOException {
		StringBuffer fileData = new StringBuffer();
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
		}
		reader.close();
		return fileData.toString();
	}

	public String getQr() throws IOException {
		WebElement qr = driver.findElement(By.cssSelector("._2EZ_m"));
		this.reloadQRCode(qr);
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);

		Point point = qr.getLocation();
		int eleWidth = qr.getSize().getWidth();
		int eleHeight = qr.getSize().getHeight();

		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);

		String path = dir_qr;
		File screenshotLocation = new File(path);
		FileUtils.copyFile(screenshot, screenshotLocation);
		return path;

	}

	public String page_alert() {
		String jarDir = driver.findElement(By.className("U0cj3")).getText();
		return jarDir;

	}

	public void sairDaConta() {
		WebElement exit = driver.findElement(By.xpath("//*[@id=\"side\"]/header/div[2]/div/span/div[3]/div/span"));
		exit.findElement(By.className("_3lSL5 _2dGjP"));
		exit.click();
	}

	public void reloadQRCode(WebElement qr) {
		qr.click();
	}

	public String takeQR() throws IOException {

		String dir_qrc = getQr();
		File file = new File(dir_qr);
		try {
			dir_qrc = encodeFileToBase64Binary(file);
		} catch (IOException e) {

			e.printStackTrace();
		}

		return dir_qrc;
	}

	@SuppressWarnings("unused")
	private String encodeFileToBase64Binary(File file) throws IOException {

		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.getEncoder().encode(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}

	private byte[] loadFile(File file) throws IOException {
		byte[] bytes;
		try (InputStream is = new FileInputStream(file)) {
			long length = file.length();
			if (length > Integer.MAX_VALUE) {
				throw new IOException("File to large " + file.getName());
			}
			bytes = new byte[(int) length];
			int offset = 0;
			int numRead = 0;
			while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
				offset += numRead;
			}
			if (offset < bytes.length) {
				throw new IOException("Could not completely read file " + file.getName());
			}
		}
		return bytes;
	}

	public static void main(String[] args) {

		ServerRunner.run(Chatbot.class);

	}

}
