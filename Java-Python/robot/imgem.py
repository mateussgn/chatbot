import binascii
import logging
from json import dumps, loads
import os, sys, json
import shutil
import tempfile
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from axolotl.kdf.hkdfv3 import HKDFv3
from axolotl.util.byteutil import ByteUtil
from base64 import b64decode, b64encode
import requests
from io import BytesIO

#pip install requests
#pip install six>=1.10.0
#pip install python-axolotl
#pip install cryptography



crypt_keys = {'document': '576861747341707020446f63756d656e74204b657973',
                  'image'   : '576861747341707020496d616765204b657973',
                  'video'   : '576861747341707020566964656f204b657973',
                  'ptt'     : '576861747341707020417564696f204b657973',
                  'audio'   : '576861747341707020417564696f204b657973'}

				  


def download(url):
  r = requests.get(url)
  return r.content  
				  

def download_media(URL, CHAVE, TIPO):
  file_data = download(URL)
  media_key = b64decode(CHAVE)
  derivative = HKDFv3().deriveSecrets(media_key, binascii.unhexlify(crypt_keys[TIPO]), 112)
  parts = ByteUtil.split(derivative, 16, 32)
  iv = parts[0]
  cipher_key = parts[1]
  e_file = file_data[:-10]
  cr_obj = Cipher(algorithms.AES(cipher_key), modes.CBC(iv), backend=default_backend())
  decryptor = cr_obj.decryptor()
  stream = BytesIO(decryptor.update(e_file) + decryptor.finalize())
  return b64encode(stream.getvalue()).decode()



url= sys.argv[1] 
chave= sys.argv[2] 
tipo= sys.argv[3] 

print (download_media(url, chave, tipo))